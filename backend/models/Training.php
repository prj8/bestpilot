<?php

namespace backend\models;

use Yii;
use \backend\models\base\Training as BaseTraining;

/**
 * This is the model class for table "training".
 */
class Training extends BaseTraining
{

    public $_start_date;
    public $_start_text;
    public $_countryName;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return
	    [
            [['name', 'school_id'], 'required'],
            [['school_id', 'license_id', 'training_type_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'active'], 'integer'],
            [['address_theory', 'address_practice', 'remarks', 'information', 'theory_details', 'elearning_details'], 'string'],
            [['theory_type', 'created_at', 'updated_at', 'deleted_at', 'lang', 'address_theory_id', 'address_practice_id', 'countryName'], 'safe'],
            [['theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'price'], 'number'],
            [['name', 'theory_type'], 'string', 'max' => 255],
            [['currency_id'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }

    public function behaviors()
    {
        return array_replace_recursive(parent::behaviors(), [
            \cornernote\linkall\LinkAllBehavior::className(),
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(\backend\models\Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartDates()
    {
        return $this->hasMany(\common\models\base\TrainingStartDates::className(), ['training_id' => 'id'])->orderBy(['training_start_dates.start_date'=>'ASC']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryName()
    {

        return $this->_countryName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setCountryName($param)
    {
        $this->_countryName = $param;

    }

    public function getStartDate()
    {
        return $this->_start_date;
    }

    public function setStartDate($param)
    {
        $this->_start_date = $param;
    }

    public function getStartText()
    {
        return $this->_start_text;
    }

    public function setStartText($param)
    {
        $this->_start_text = $param;
    }
	
}
