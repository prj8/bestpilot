<?php
namespace backend\models;

use yii\base\Security;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \common\models\User
{

    const STATUS_ACTIVE_ADMIN = 1;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE_ADMIN],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE_ADMIN, self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE_ADMIN]);
    }

    public function sendAdminInvitation(){
        return \Yii::$app->mailer->compose(
                ['html' => 'admin-invitation-html', 'text' => 'admin-invitation-text'],
                ['token' => $this->confirmation_token]
            )
            ->setTo($this->email)
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setSubject(\Yii::t('backend/school/user', 'BestPilot.com - You have invitatnion to manage school.'))
            ->setReplyTo(\Yii::$app->params['supportEmail'])
            ->send();
    }

    /**
     * Removes confirmation token
     */
    public function removeConfirmationToken()
    {
        $this->confirmation_token = null;
    }

}
