<?php

namespace backend\models;

use \backend\models\base\Currency as BaseCurrency;

/**
 * This is the model class for table "currency".
 */
class Currency extends BaseCurrency
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'required'],
            [['active', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'symbol'], 'safe'],
            [['id'], 'string', 'max' => 4],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
