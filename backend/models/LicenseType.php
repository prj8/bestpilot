<?php

namespace backend\models;

use Yii;
use \backend\models\base\LicenseType as BaseLicenseType;

/**
 * This is the model class for table "license_type".
 */
class LicenseType extends BaseLicenseType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['license'], 'required'],
            [['lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'order'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['license'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['description', 'text', 'requirements'], 'string'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
