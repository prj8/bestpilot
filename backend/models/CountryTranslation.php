<?php

namespace backend\models;

use \backend\models\base\CountryTranslation as BaseCountryTranslation;

/**
 * This is the model class for table "countries_translation".
 */
class CountryTranslation extends BaseCountryTranslation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id'], 'integer'],
            [['country'], 'string', 'max' => 212],
            [['country_id'], 'string', 'max' => 4]
        ]);
    }
	
}
