<?php
namespace backend\models;


use backend\models\User;
use yii\base\Model;
use Yii;

class UserRegistration extends Model
{
    public $username;
    public $password;
    public $password_repeat;

    private $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            ['password', 'required', 'message'=>Yii::t('school/admin-registration', "Field {attribute} can not be empty")],
            ['password', 'string', 'min' => 6, 'message' => Yii::t('school/admin-registration', 'Field {attribute} can not be longer than 6 chars')],
            ['password_repeat', 'required', 'message'=>Yii::t('school/admin-registration', "Field {attribute} can not be empty")],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>Yii::t('school/admin-registration', "Passwords don't match")],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username'   => Yii::t('school/admin-registration', 'Username'),
            'password'   => Yii::t('school/admin-registration', 'Password'),
            'password_repeat'   => Yii::t('school/admin-registration', 'Repeat password'),
        ];
    }

    public function save() {

    }
}
