<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[Aircraft]].
 *
 * @see Aircraft
 */
class AircraftQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[aircraft.deleted]]=0');
        if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false)) {
            $this->join('INNER JOIN', 'user_to_school uts', 'uts.school_id = aircraft.school_id')->andWhere(['AND', ['uts.user_id'=>\Yii::$app->user->id]]);
        }
        return $this;
    }

    /**
     * @inheritdoc
     * @return Aircraft[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Aircraft|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}