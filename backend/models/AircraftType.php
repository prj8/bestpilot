<?php

namespace backend\models;

use \backend\models\base\AircraftType as BaseAircraftType;

/**
 * This is the model class for table "aircraft_type".
 */
class AircraftType extends BaseAircraftType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['active', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'multiengine'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at', 'photo'], 'safe'],
            [['name', 'range', 'speed', 'multiengine'], 'required'],
            [['name', 'range', 'speed'], 'string', 'max' => 128],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }




    public function afterFind()
    {
        parent::afterFind();
        $this->name = trim($this->name);
    }
	
}
