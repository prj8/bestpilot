<?php

namespace backend\models;

use \backend\models\base\Country as BaseCountry;

/**
 * This is the model class for table "countries".
 */
class Country extends BaseCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'country'], 'required'],
            [['id'], 'string', 'max' => 3],
            [['currency_id'], 'string', 'max' => 4],
            [['country'], 'string', 'max' => 212],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }

    public function getTtitle(){
        if(!$country = \Yii::$app->cache->get(__CLASS__.'/country'.$this -> id.\Yii::$app->language)){
            $country = $this -> hasTranslation() ? $this -> translate() -> country : $this -> country;
            \Yii::$app->cache->set(__CLASS__.'/country'.$this -> id.\Yii::$app->language, $country, 3600*24);
        }

        return $country;
    }
	
}
