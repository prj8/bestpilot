<?php

namespace backend\models;

use \backend\models\base\School as BaseSchool;

/**
 * This is the model class for table "school".
 */
class School extends BaseSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'city', 'country_id'], 'required'],
            [['coordinates', 'remarks'], 'string'],
            [['has_en', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['logo', 'created_at', 'updated_at', 'deleted_at', 'country_id'], 'safe'],
            [['name', 'street', 'city', 'www', 'email', 'phone', 'contact_person'], 'string', 'max' => 255],
            [['street_no', 'post_code'], 'string', 'max' => 12],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
