<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[AircraftType]].
 *
 * @see AircraftType
 */
class AircraftTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return AircraftType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AircraftType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}