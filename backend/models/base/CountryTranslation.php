<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "countries_translation".
 *
 * @property integer $id
 * @property string $id_country
 * @property integer $country
 *
 * @property \backend\models\Countries $country
 */
class CountryTranslation extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['country'], 'string', 'max' => 212],
            [['country_id'], 'string', 'max' => 4]
        ];
    }

    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries_translation';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/country', 'ID'),
            'id_country' => Yii::t('backend/country', 'Id Country'),
            'country' => Yii::t('backend/country', 'Country'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(\backend\models\Countries::className(), ['id' => 'country_id']);
    }

}
