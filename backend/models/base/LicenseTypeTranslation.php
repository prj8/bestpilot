<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "license_type_translation".
 *
 * @property integer $id
 * @property integer $license_type_id
 * @property string $description
 * @property string $text
 *
 * @property \backend\models\Countries $country
 */
class LicenseTypeTranslation extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'license_type_id'], 'integer'],
            [['description', 'text', 'requirements', 'language'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'license_type_translation';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/licenseType', 'ID'),
            'license_type_id' => Yii::t('backend/licenseType', 'License Type ID'),
            'description' => Yii::t('backend/licenseType', 'Description'),
            'text' => Yii::t('backend/licenseType', 'Long text'),
            'requirements' => Yii::t('backend/licenseType', 'Requirements'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLicenseType()
    {
        return $this->hasOne(\backend\models\LicenseType::className(), ['id' => 'license_type_id']);
    }

}
