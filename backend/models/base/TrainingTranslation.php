<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "license_type_translation".
 *
 * @property integer $id
 * @property integer $license_type_id
 * @property string $description
 * @property string $text
 *
 * @property \backend\models\Countries $country
 */
class TrainingTranslation extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'training_id'], 'integer'],
            [['information'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'training_translation';
    }

    /**
     *
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock
     *
     */
    public function optimisticLock() {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/training', 'ID'),
            'training_id' => Yii::t('backend/training', 'Training ID'),
            'information' => Yii::t('backend/training', 'Information'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(\backend\models\Training::className(), ['id' => 'training_id']);
    }

}
