<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_rates".
 *
 * @property integer $id
 * @property integer $rate
 * @property integer $school_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 */
class SchoolRates extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate', 'school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['user_id', 'created_at', 'updated_at', 'deleted_at', 'published'], 'safe'],
            [['email', 'text', 'name'], 'safe'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_rates';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/school-rates', 'ID'),
            'rate' => Yii::t('backend/school-rates', 'Rate'),
            'school_id' => Yii::t('backend/school-rates', 'School ID'),
            'lock' => Yii::t('backend/school-rates', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\backend\models\School::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTraining()
    {
        return $this->hasOne(\backend\models\Training::className(), ['id' => 'training_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\SchoolRatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\SchoolRatesQuery(get_called_class());
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleted = 1;
            $this->save();
            return false;
        } else {
            return false;
        }
    }
}
