<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "user_to_school".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $school_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 * @property \backend\models\User $user
 */
class UserToSchool extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_to_school';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/school', 'ID'),
            'user_id' => Yii::t('backend/school', 'User ID'),
            'school_id' => Yii::t('backend/school', 'School ID'),
            'lock' => Yii::t('backend/school', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\backend\models\School::className(), ['id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\backend\models\User::className(), ['id' => 'user_id']);
    }

    public static function validateUserToSchool($school_id, $user_id)
    {
        $tmp = self::find()->where(['AND', ['school_id'=>$school_id, 'user_id'=>$user_id]])->one();
        return ($tmp !== null);
    }

    public static function validateUserToTraining($training_id, $user_id)
    {
        $tmp = self::find()->join('INNER JOIN', 'training t', 'user_to_school.school_id = t.school_id AND t.id = '.$training_id)->where(['AND', ['user_to_school.user_id'=>$user_id]])->one();
        return ($tmp !== null);
    }

    public static function validateUserToAircraft($aircraft_id, $user_id)
    {
        $tmp = self::find()->join('INNER JOIN', 'aircraft a', 'user_to_school.school_id = a.school_id AND a.id = '.$aircraft_id)->where(['AND', ['user_to_school.user_id'=>$user_id]])->one();
        return ($tmp !== null);
    }

    public static function validateUserToSimulator($simulator_id, $user_id)
    {
        $tmp = self::find()->join('INNER JOIN', 'simulator s', 'user_to_school.school_id = s.school_id AND s.id = '.$simulator_id)->where(['AND', ['user_to_school.user_id'=>$user_id]])->one();
        return ($tmp !== null);
    }

    public static function validateUserToSchoolRate($school_rate_id, $user_id)
    {
        $tmp = self::find()->join('INNER JOIN', 'school_rates sr', 'user_to_school.school_id = sr.school_id AND sr.id = '.$school_rate_id)->where(['AND', ['user_to_school.user_id'=>$user_id]])->one();
        return ($tmp !== null);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\UserToSchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\UserToSchoolQuery(get_called_class());
    }
}
