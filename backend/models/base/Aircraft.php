<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aircraft".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $type
 * @property string $register_no
 * @property integer $multi_engine
 * @property string $aircraft_year
 * @property string $engine_year
 * @property string $glass_cockpit
 * @property integer $gps
 * @property integer $radio
 * @property integer $ils
 * @property integer $night
 * @property integer $ifr
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 * @property \backend\models\AircraftToTraining[] $aircraftToTrainings
 */
class Aircraft extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id',  'gps', 'radio', 'ils', 'night', 'ifr', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['type_id', 'school_id'], 'required'],
            [['remarks', 'image'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'type_id'], 'safe'],
            [['glass_cockpit'], 'string', 'max' => 255],
            [['register_no'], 'string', 'max' => 16],
            [['aircraft_year', 'engine_year'], 'string', 'max' => 64],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aircraft';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/aircraft', 'ID'),
            'school_id' => Yii::t('backend/aircraft', 'School ID'),
            'type' => Yii::t('backend/aircraft', 'Type'),
            'register_no' => Yii::t('backend/aircraft', 'Register No'),
            'multi_engine' => Yii::t('backend/aircraft', 'Multi Engine'),
            'aircraft_year' => Yii::t('backend/aircraft', 'Aircraft Year'),
            'engine_year' => Yii::t('backend/aircraft', 'Engine Year'),
            'glass_cockpit' => Yii::t('backend/aircraft', 'Glass Cockpit'),
            'gps' => Yii::t('backend/aircraft', 'Gps'),
            'radio' => Yii::t('backend/aircraft', 'Radio'),
            'ils' => Yii::t('backend/aircraft', 'Ils'),
            'night' => Yii::t('backend/aircraft', 'Night'),
            'ifr' => Yii::t('backend/aircraft', 'Ifr'),
            'remarks' => Yii::t('backend/aircraft', 'Remarks'),
            'lock' => Yii::t('backend/aircraft', 'Lock'),
            'image' => Yii::t('backend/aircraft', 'Image'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\backend\models\School::className(), ['id' => 'school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftType()
    {
        return $this->hasOne(\backend\models\AircraftType::className(), ['id' => 'type_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftTypeName()
    {
        return $this->aircraftType->name;
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftToTrainings()
    {
        return $this->hasMany(\backend\models\AircraftToTraining::className(), ['aircraft_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\AircraftQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\AircraftQuery(get_called_class());
    }



    public function photoResize($width=0, $height=null){
        $filename = Yii::getAlias('@upload') . '/aircraft/' . $this->image;
        if(is_file($filename)){
            return \Yii::$app->thumbnail->url($filename, ['thumbnail' => [
                'width' => $width,
                'height' => $height,
                'mode' => \sadovojav\image\Thumbnail::THUMBNAIL_OUTBOUND,
            ]]);
        }else
            return $this->aircraftType->photoResize($width, $height);
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleted = 1;
            $this->save();
            return false;
        } else {
            return false;
        }
    }
}
