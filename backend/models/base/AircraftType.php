<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aircraft_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $range
 * @property string $speed
 * @property string $photo
 * @property integer $active
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\Aircraft[] $aircrafts
 */
class AircraftType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['active', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'multiengine'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'range', 'speed', 'multiengine'], 'required'],
            [['name', 'range', 'speed', 'photo'], 'string', 'max' => 128],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aircraft_type';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/aircraft', 'ID'),
            'name' => Yii::t('backend/aircraft', 'Name'),
            'range' => Yii::t('backend/aircraft', 'Range'),
            'speed' => Yii::t('backend/aircraft', 'Speed'),
            'photo' => Yii::t('backend/aircraft', 'Photo'),
            'active' => Yii::t('backend/aircraft', 'Active'),
            'lock' => Yii::t('backend/aircraft', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircrafts()
    {
        return $this->hasMany(\backend\models\Aircraft::className(), ['type_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\AircraftTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\AircraftTypeQuery(get_called_class());
    }

    public function photoResize($width=0, $height=null){
        $filename = Yii::getAlias('@upload') . '/aircraft-types/' . $this->photo;
        if(file_exists($filename)){
            return \Yii::$app->thumbnail->url($filename, ['thumbnail' => [
                'width' => $width,
                'height' => $height,
                'mode' => \sadovojav\image\Thumbnail::THUMBNAIL_OUTBOUND,
            ]]);
        }else
            return null;
    }
}
