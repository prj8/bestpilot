<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the base model class for table "school".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 * @property string $street
 * @property string $street_no
 * @property string $city
 * @property string $post_code
 * @property string $coordinates
 * @property string $www
 * @property string $email
 * @property string $phone
 * @property string $contact_person
 * @property integer $has_en
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\Aircraft[] $aircrafts
 * @property \backend\models\Simulator[] $simulators
 * @property \backend\models\Training[] $trainings
 * @property \backend\models\UserToSchool[] $userToSchools
 */
class School extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city', 'country_id'], 'required'],
            [['coordinates', 'remarks'], 'string'],
            [['has_en', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['logo', 'created_at', 'updated_at', 'deleted_at', 'country_id'], 'safe'],
            [['name', 'street', 'city', 'www', 'email', 'phone', 'contact_person'], 'string', 'max' => 255],
            [['street_no', 'post_code'], 'string', 'max' => 12],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/school', 'ID'),
            'name' => Yii::t('backend/school', 'Name'),
            'logo' => Yii::t('backend/school', 'Logo'),
            'street' => Yii::t('backend/school', 'Street'),
            'street_no' => Yii::t('backend/school', 'Street No'),
            'city' => Yii::t('backend/school', 'City'),
            'post_code' => Yii::t('backend/school', 'Post Code'),
            'coordinates' => Yii::t('backend/school', 'Coordinates'),
            'www' => Yii::t('backend/school', 'Www'),
            'email' => Yii::t('backend/school', 'Email'),
            'phone' => Yii::t('backend/school', 'Phone'),
            'contact_person' => Yii::t('backend/school', 'Contact Person'),
            'has_en' => Yii::t('backend/school', 'Has En'),
            'remarks' => Yii::t('backend/school', 'Remarks'),
            'lock' => Yii::t('backend/school', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircrafts()
    {
        return $this->hasMany(\backend\models\Aircraft::className(), ['school_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\backend\models\Address::className(), ['school_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimulators()
    {
        return $this->hasMany(\backend\models\Simulator::className(), ['school_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(\backend\models\Training::className(), ['school_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserToSchools()
    {
        return $this->hasMany(\backend\models\UserToSchool::className(), ['school_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(\backend\models\Country::className(), ['id' => 'country_id']);
    }


    /**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\SchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\SchoolQuery(get_called_class());
    }

    public function addUser($user_id)
    {
        if (!UserToSchool::find()->where(['AND', ['=', 'user_id', $user_id], ['=', 'school_id', $this->id]])->one()){
            $uts = new UserToSchool();
            $uts->school_id = $this->id;
            $uts->user_id = $user_id;
            $uts->save();
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deleted = 1;
            $this->save();
            return false;
        } else {
            return false;
        }
    }
}
