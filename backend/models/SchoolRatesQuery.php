<?php

namespace backend\models;

/**
 * This is the ActiveQuery class for [[SchoolRates]].
 *
 * @see SchoolRates
 */
class SchoolRatesQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere('[[school_rates.deleted]]=0');
        if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false)) {
            $this->join('INNER JOIN', 'user_to_school uts', 'uts.school_id = school_rates.school_id')->andWhere(['AND', ['uts.user_id'=>\Yii::$app->user->id]]);
        }
        return $this;
    }

    /**
     * @inheritdoc
     * @return SchoolRates[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolRates|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}