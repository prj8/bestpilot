<?php

namespace backend\models;

use \backend\models\base\TrainingStartDates as BaseTrainingStartDates;

/**
 * This is the model class for table "training_start_dates".
 */
class TrainingStartDates extends BaseTrainingStartDates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['training_id', 'start_text', 'start_date'], 'required'],
            [['training_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['start_date', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['start_text'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
