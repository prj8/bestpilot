<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
//use backend\models\Training;

/**
 * backend\models\TrainingSearch represents the model behind the search form about `backend\models\Training`.
 */
 class TrainingSearch extends Training
{


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'license_id', 'training_type_id', 'theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'active'], 'integer'],
            [['name', 'address_theory', 'theory_type', 'address_practice', 'remarks', 'created_at', 'updated_at', 'deleted_at', 'countryName', 'startText', 'startDate'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Training::find()->joinWith('startDates')->joinWith('school');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'license_id' => $this->license_id,
            'training_type_id' => $this->training_type_id,
            'theory_hours' => $this->theory_hours,
            'elearning_hours' => $this->elearning_hours,
            'aircraft_hours' => $this->aircraft_hours,
            'simulator_hours' => $this->simulator_hours,
            'price' => $this->price,
            'lock' => $this->lock,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'created' => $this->created,
            'modified' => $this->modified,
            'deleted' => $this->deleted,
            'school.deleted' => 0,
        ]);

        //$query->andFilterWhere(['start_date' => $this->start_date,]);

        if ($this->startDate!=NULL && $this->startDate!="") {
            $dates = explode(' ',$this->startDate);
            $date_from = $dates[0];
            $date_to = $dates[count($dates)-1];
            $query->andFilterWhere(['between', 'training_start_dates.start_date', $date_from, $date_to]);

        }


        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address_theory', $this->address_theory])
            ->andFilterWhere(['like', 'theory_type', $this->theory_type])
            ->andFilterWhere(['like', 'address_practice', $this->address_practice])
            ->andFilterWhere(['like', 'training_start_dates.start_text', $this->startText])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'school.country_id', $this->countryName]);

        $query->active();
        return $dataProvider;
    }
}
