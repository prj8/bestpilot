<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class KingAppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/jquery-ui.css',
        'css/jquery.gritter.css',
        'css/font-awesome.min.css',
        'css/style.css',
        'css/widgets.css',
    ];
    public $js = [
        
        'js/bootstrap.min.js',
        'js/bootstrap-datetimepicker.min.js',
        'js/jquery.gritter.min.js',
        'js/respond.min.js',
        'js/html5shiv.js',
        'js/custom.js',
        'js/jquery-ui.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
