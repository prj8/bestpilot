<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\popover\PopoverX;

$school_admin = \webvimark\modules\UserManagement\models\User::hasRole('school-admin', false);

$this->title = Yii::t('backend/school-rates', 'School Rates');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/school-rates', 'School Rate');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="col-lg-12 col-md-12 school-rates-index">


    <p>
        <?= Html::a(Yii::t('backend/school-rates', 'Create School Rates'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php

    if ($school_admin) {
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute'=>'rate',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'width'=>'70px',
            ],
            [
                'attribute' => 'school_id',
                'label' => Yii::t('backend/school-rates', 'School'),
                'value' => function($model){
                    return $model->school->name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid--school_id']
            ],
            [
                'attribute'=>'name',
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true
            ],
            'email:email',
            [
                'attribute' => 'user_id',
                'label' => Yii::t('backend/school-rates', 'User logged'),
                'value' => function($model){
                    return $model->user_id != null ? $model->user->email : 'not set';
                },
            ],
            [
                'attribute'=>'text',
                'hAlign'=>'left',
                'vAlign'=>'top',
                'width'=>'30%',
                'pageSummary'=>true
            ],
        ];
    } else {
        $gridColumn = [
            ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'rate',
                'editableOptions'=>[
                    'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                    'data' => [0 => '0', 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'],
                ],
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true,
                'width'=>'70px',
            ],
            [
                'attribute' => 'school_id',
                'label' => Yii::t('backend/school-rates', 'School'),
                'value' => function($model){
                    return $model->school->name;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid--school_id']
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'name',
                'editableOptions'=>[
                    'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                ],
                'hAlign'=>'right',
                'vAlign'=>'middle',
                'pageSummary'=>true
            ],
            'email:email',
            [
                'attribute' => 'user_id',
                'label' => Yii::t('backend/school-rates', 'User'),
                'value' => function($model){
                    return $model->user_id != null ? $model->user->email : Yii::t('backend', 'not set');
                },
            ],
            [
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'text',
                'editableOptions'=>[
                    'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                ],
                'hAlign'=>'left',
                'vAlign'=>'top',
                'width'=>'30%',
                'pageSummary'=>true
            ],
        ];
    }
    $gridColumn = array_merge($gridColumn, [
        ['attribute' => 'lock', 'visible' => false],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'published',
            'editableOptions'=>[
                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                'header'=>'Published',
                'placement'=>PopoverX::ALIGN_LEFT,
            ],
            'value'=>function($data){ return ($data->published==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
            'hAlign'=>'right',
            'vAlign'=>'middle',
            'width'=>'50px',
            'pageSummary'=>true,
            'filterType' => GridView::FILTER_SELECT2,
            'filter' => [0=>Yii::t('backend', 'No'), 1=>Yii::t('backend', 'Yes')],
            'filterWidgetOptions' => [
                'pluginOptions' => ['allowClear' => true],
            ],
            'filterInputOptions' => ['placeholder' => 'Published', 'id' => 'grid-school-rates-search-published']
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view'=>function($url,$model,$key){
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['school-rates/view', 'id'=>$model->id], ['title'=>Yii::t('backend/school-rates', 'View')]);
                },
                'update'=>function($url,$model,$key){
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['school-rates/update', 'id'=>$model->id], ['title'=>Yii::t('backend/school-rates', 'Update')]);
                },
                'delete'=>function($url,$model,$key){
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['school-rates/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/school-rates', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/school-rates', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/school-rates', 'Delete')]);
                },
            ],
        ],
    ]);
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school']],
        'resizableColumns'=>true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu

        'toolbar' => [
            '{toggleData}',
        ],
    ]); ?>

</div>

<?php
/*
$this->registerJs("

		$('.kv-editable').click(function(e){
			$('.kv-editable-popover.in').not('#'+$(this).attr('id').replace('-targ', '')+'-popover .kv-editable-popover.in').removeClass('in').hide();
			return true;
		});
	");
*/
?>
