<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;


/* @var $this yii\web\View */
/* @var $model backend\models\base\SchoolRates */

$this->title = Yii::t('schoolrates', 'Create School Rates');
$this->params['breadcrumbs'][] = ['label' => Yii::t('schoolrates', 'School Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-rates-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
