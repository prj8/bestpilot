<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;

/* @var $this yii\web\View */
/* @var $model backend\models\base\SchoolRates */

$this->title = Yii::t('backend/school-rates', 'Update {modelClass}: ', [
    'modelClass' => 'School Rates',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/school-rates', 'School Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/school-rates', 'Update');
?>
<div class="school-rates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
