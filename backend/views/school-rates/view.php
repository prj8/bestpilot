<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\base\SchoolRates */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/school-rates', 'School Rates'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/school-rates', 'School Rate');
?>
<div class="school-rates-view">

    <div class="row">
        <div class="col-sm-9">

        </div>
        <div class="col-sm-3 text-right" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend/school-rates', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend/school-rates', 'Change visibility'), ['publish', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend/school-rates', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend/school-rates', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'rate',
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend/school-rates', 'School'),
        ],
        [
            'attribute' => 'training.name',
            'label' => Yii::t('backend/school-rates', 'Training'),
        ],
        'name',
        'email:email',
        [
            'attribute' => 'user.email',
            'label' => Yii::t('backend/school-rates', 'User'),
        ],
        'text:ntext',
        ['attribute' => 'lock', 'visible' => false],
        'published',
        [
            'attribute' => 'published',
            'value' => function($model){ return $model->published==1 ? Yii::t('backend', 'Yes') : Yii::t('backend', 'No');}
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
</div>
