<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model backend\models\base\SchoolRates */
/* @var $form yii\widgets\ActiveForm */

$school_admin = \webvimark\modules\UserManagement\models\User::hasRole('school-admin', false);

?>

<div class="school-rates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'rate')->dropDownList([0 => '0', 1 => '1', 2 => '2', 3 => '3', 4 => '4', 5 => '5'], ['disabled'=>$school_admin]); ?>

    <?php

        echo $form->field($model, 'school_id')->dropDownList(\yii\helpers\ArrayHelper::map(\backend\models\base\School::find()->active()->orderBy('id')->asArray()->all(), 'id', 'name'), ['id'=>'cat1-id', 'disabled'=>$school_admin]);


        echo $form->field($model, 'training_id')->widget(DepDrop::classname(), [
            'type'=>DepDrop::TYPE_SELECT2,
            'data'=>\yii\helpers\ArrayHelper::map(\backend\models\base\Training::find()->where(['=', 'training.school_id', $model->school_id])->active()->orderBy('id')->all(), 'id', function($ar, $dv){return $ar->licenseType->license.' ('.$ar->price.'zł)';}),
            'options'=>['id'=>'subcat1-id', 'disabled'=>$school_admin],
            'select2Options'=>['pluginOptions'=>['allowClear'=>true]],
            'pluginOptions'=>[
                'depends'=>['cat1-id'],
                'placeholder'=>'Select ...',
                'url'=>Url::to(['/school-rates/subcat1'])
            ]
        ]);

    ?>


    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name', 'disabled'=>$school_admin]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email', 'disabled'=>$school_admin]) ?>

    <?php if (!$school_admin) echo $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'email'),
        'options' => ['placeholder' => Yii::t('backend/school-rates', 'Choose User')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6, 'disabled'=>$school_admin]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'published')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/school-rates', 'Create') : Yii::t('backend/school-rates', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
