<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend\training', 'License Type');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\training', 'View License Types');

$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="col-lg-12 col-md-12 license-type-index">

    

    <p>
        <?= Html::a(Yii::t('backend\training', 'Create License Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn', 'visible' => false],
        ['attribute' => 'id', 'visible' => false],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'license',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'pageSummary'=>true,
			'headerOptions'=>['hAlign'=>'left','vAlign'=>'bottom']
		],
        [
            'class'=>'kartik\grid\EditableColumn',
            'attribute'=>'order',
            'editableOptions'=>[
                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
            ],
            'width'=>'10%',
            'pageSummary'=>true
        ],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
			'visibleButtons' => ['view'=>false]
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-license-type']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{toggleData}',
        ],
    ]); ?>

</div>


<?php

$this->registerJs("

		$('.kv-editable').click(function(e){
			$('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
			return true;
		});
	");

?>