<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LicenseType */

$this->title = Yii::t('backend\training', 'Create License Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'License Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
//$this->params['page_subheader'] = Yii::t('backend\training', 'View License Type details');
?>
<div class="license-type-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
