<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LicenseType */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="license-type-form">
	<div class="row">
		<div class="col-md-9 col-lg-9 col-sm-9">
    <?php $form = ActiveForm::begin([
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
			'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
    ],
]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'license')->textInput(['maxlength' => true, 'placeholder' => 'License']) ?>

	<?php
		echo $form->field($model, 'description')->widget(\yii\redactor\widgets\Redactor::className());
		echo $form->field($model, 'text')->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/licenseType', 'Page text'));
		echo $form->field($model, 'requirements')->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/licenseType', 'Requirements'));
		$Languages = \yii\helpers\ArrayHelper::map(\lajax\translatemanager\models\Language::find()->where(['=', 'status', '1'])->asArray()->all(), 'language_id', 'language');

		foreach ($Languages as $key => $value) {
			if ('pl' != $key) {
				echo $form->field($model->translate($key), "[$key]description")->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/licenseType', 'Description') . ' ' . $value);
				echo $form->field($model->translate($key), "[$key]text")->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/licenseType', 'Page text') . ' ' . $value);
				echo $form->field($model->translate($key), "[$key]requirements")->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/licenseType', 'Requirements') . ' ' . $value);
		}
		}
	?>


    <?= $form->field($model, 'order')->textInput(['maxlength' => false, 'placeholder' => 'Order']) ?>


    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    
    <div class="form-group">
		<div class="col-md-2">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('backend\training', 'Create') : Yii::t('backend\training', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
    </div>

    <?php ActiveForm::end(); ?>
		</div>
	</div>

</div>
