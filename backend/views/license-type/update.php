<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\LicenseType */

$this->title = Yii::t('backend\training', 'Update {modelClass}: ', [
    'modelClass' => 'License Type',
]) . ' ' . $model->license;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'License Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->license, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend\training', 'Update');
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\training', 'Edit License Type details');
?>
<div class="license-type-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
