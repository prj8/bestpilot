<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\School */
/* @var $form yii\widgets\ActiveForm */


\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Users',
        'relID' => 'users',
        'value' => \yii\helpers\Json::encode($model->userToSchools),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

?>

<div class="school-form">
    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data', 'class' => 'form-horizontal'], // important
        'fieldConfig' => [
            'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
            'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
        ],
    ]); ?>
    <div class="row">
        <div class="col-md-9">


            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

            <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?php
            $forms = [
                [
                    'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/school', 'Users')),
                    'content' => $this->render('_formUsers', [
                        'row' => \yii\helpers\ArrayHelper::toArray($model->userToSchools)
                    ]),
                ],
            ];

            if ($model->id != 0)
                echo kartik\tabs\TabsX::widget([
                    'items' => $forms,
                    'position' => kartik\tabs\TabsX::POS_ABOVE,
                    'encodeLabels' => false,
                    'pluginOptions' => [
                        'bordered' => true,
                        'sideways' => true,
                        'enableCache' => false,
                    ],
                ]);
            ?>




        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/school', 'Create') : Yii::t('backend/school', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>


        </div>
    </div>


    <?php ActiveForm::end(); ?>
</div>
