<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\SchoolSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-school-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => true, 'placeholder' => 'Street']) ?>

    <?= $form->field($model, 'street_no')->textInput(['maxlength' => true, 'placeholder' => 'Street No']) ?>

    <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => 'City']) ?>

    <?php /* echo $form->field($model, 'post_code')->textInput(['maxlength' => true, 'placeholder' => 'Post Code']) */ ?>

    <?php /* echo $form->field($model, 'coordinates')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'www')->textInput(['maxlength' => true, 'placeholder' => 'Www']) */ ?>

    <?php /* echo $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) */ ?>

    <?php /* echo $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Phone']) */ ?>

    <?php /* echo $form->field($model, 'contact_person')->textInput(['maxlength' => true, 'placeholder' => 'Contact Person']) */ ?>

    <?php /* echo $form->field($model, 'has_en')->textInput(['placeholder' => 'Has En']) */ ?>

    <?php /* echo $form->field($model, 'remarks')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend\school', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend\school', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
