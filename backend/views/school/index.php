<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend/school', 'School');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/school', 'View schools');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="school-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend/school', 'Create School'), ['create'], ['class' => 'btn btn-success']) ?>
        <!--<?= Html::a(Yii::t('backend/school', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>-->
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
		['attribute' => 'id', 'visible' => false],
		[
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'name',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'300px',
			'pageSummary'=>true
		],
		[
			'attribute' => 'country_id',
			'label' => Yii::t('backend\training', 'Country'),
			'value' => function($model){
				return $model->country->country;
			},
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Country::find()->orderBy('country','ASC')->asArray()->all(), 'id', 'country'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'Country', 'id' => 'grid-training-search-country_id']
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'city',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'100px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'www',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'email',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'phone',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'contact_person',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'has_en',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
				'header'=>'In english'
			],
			'value'=>function($data){ return ($data->has_en==1 ? Yii::t('app', 'Yes'):Yii::t('app', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'50px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'remarks',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'100px',
			'pageSummary'=>true
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => [
				'class' => 'action-column-min',
				'style'=>'width:7%'
			],
			'template' => '{view} {update} {users} {delete}',
			'buttons' => [
				'view'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['school/view', 'id'=>$model->id], ['title'=>Yii::t('backend/school', 'View')]);
				},
				'update'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['school/update', 'id'=>$model->id], ['title'=>Yii::t('backend/school', 'Update')]);
				},
				'users'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-user"></span>', ['school/users', 'id'=>$model->id], ['title'=>Yii::t('backend/school', 'Users')]);
				},
				'delete'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['school/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/school', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/school', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/school', 'Delete')]);
				},
			],
		],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school']],
		'resizableColumns'=>true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        
        'toolbar' => [
            '{toggleData}',
        ],
    ]); ?>

</div>


<?php
/*
$this->registerJs("

		$('.kv-editable').click(function(e){
			$('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
			return true;
		});
	");
*/
?>