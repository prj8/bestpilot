<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\School */

$this->title = Yii::t('backend/school', 'Update {modelClass}: ', [
    'modelClass' => 'School',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/school', 'School'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/school', 'Update');
?>
<div class="school-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>




    <div class="row">
        <div class="col-md-12">
            <div class="widget worange">
                <?php
                $layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		<div class="widget-foot">
			{footer}
			<div class="clearfix"></div>
		</div>
		</div>
		<!-- Widget ends -->
		
HTML;
                ?>


                <?php
                if($providerAircraft->totalCount){
                    $gridColumnAircraft = [
                        ['attribute' => 'id', 'visible' => false],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
                                'options'=>[
                                    'data'=> \yii\helpers\ArrayHelper::map(\backend\models\AircraftType::find()->asArray()->all(), 'id', 'name')
                                ]
                            ],
                            'attribute' => 'type_id',
                            'label' => Yii::t('backend/aircraft', 'Aircraft Type'),
                            'value' => function($model){
                                return $model->aircraftType->name;
                            },
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'register_no',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'10%',
                            'pageSummary'=>true
                        ],
                        [
                            'label'=>Yii::t('backend/aircraft', 'Multiengine'),
                            'value'=>function($data){ return ($data->aircraftType->multiengine==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                            'filterType' => GridView::FILTER_CHECKBOX,

                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'aircraft_year',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'engine_year',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'glass_cockpit',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'10%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'gps',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                            ],
                            'value'=>function($data){ return ($data->gps==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'radio',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                            ],
                            'value'=>function($data){ return ($data->radio==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'ils',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                            ],
                            'value'=>function($data){ return ($data->ils==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'night',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                            ],
                            'value'=>function($data){ return ($data->night==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'ifr',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
                            ],
                            'value'=>function($data){ return ($data->ifr==1 ? Yii::t('backend', 'Yes'):Yii::t('backend', 'No')); },
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'width'=>'5%',
                            'pageSummary'=>true,
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'remarks',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'hAlign'=>'right',
                            'vAlign'=>'middle',
                            'pageSummary'=>true
                        ],
                        ['attribute' => 'lock', 'visible' => false],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'options'=>['class'=>'action-column-max'],
                            'buttons' => [
                                'view'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['aircraft/view', 'id'=>$model->id], ['title'=>Yii::t('backend/aircraft', 'View')]);
                                },
                                'update'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['aircraft/update', 'id'=>$model->id], ['title'=>Yii::t('backend/aircraft', 'Update')]);
                                },
                                'delete'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['aircraft/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/aircraft', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/aircraft', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/aircraft', 'Delete')]);
                                },
                            ],
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerAircraft,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aircraft']],
                        'layout' => $layout,
                        'columns' => $gridColumnAircraft,
                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/aircraft', 'Aircraft')),
                            '{footer}' => Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Aircraft To School'), ['aircraft/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-warning kv-batch-create'])
                        ],
                    ]);
                } else {
                    echo Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Aircraft To School'), ['aircraft/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-warning kv-batch-create']);
                }
                ?>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="widget wred">

                <?php
                if($providerTraining->totalCount){
                    $gridColumnTraining = [
                        ['attribute' => 'id', 'visible' => false],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'name',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'10%',
                            'pageSummary'=>true,
                            'headerOptions'=>['hAlign'=>'left','vAlign'=>'bottom']
                        ],
                        [
                            'attribute' => 'license_id',
                            'label' => Yii::t('backend/training', 'License'),
                            'value' => function($model){
                                return $model->licenseType->license;
                            },
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
                                'options'=>[
                                    'data'=> \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->asArray()->all(), 'id', 'type')
                                ]
                            ],
                            'attribute' => 'training_type_id',
                            'label' => Yii::t('backend/training', 'Training Type'),
                            'value' => function($model){
                                return $model->trainingType != null ? $model->trainingType->type : '(not set)';
                            },
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'theory_hours',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'elearning_hours',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'theory_type',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'10%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'aircraft_hours',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'simulator_hours',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],

                        [
                            'class'=>'kartik\grid\EditableColumn',
                            'attribute'=>'price',
                            'editableOptions'=>[
                                'inputType'=>\kartik\editable\Editable::INPUT_TEXT
                            ],
                            'width'=>'5%',
                            'pageSummary'=>true
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'options'=>['class'=>'action-column-max'],
                            'buttons' => [
                                'view'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['training/view', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'View')]);
                                },
                                'update'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['training/update', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Update')]);
                                },
                                'delete'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['training/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/training', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/training', 'Delete')]);
                                },
                            ],
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerTraining,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-training']],
                        'layout' => $layout,
                        'columns' => $gridColumnTraining,
                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/school', 'Training')),
                            '{footer}' => Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Training To School'), ['training/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-danger kv-batch-create'])
                        ],
                    ]);
                } else {
                    echo Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Training To School'), ['training/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-danger kv-batch-create']);
                }
                ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget wlightblue">

                <?php
                if($providerSimulator->totalCount){
                    $gridColumnSimulator = [
                        ['attribute' => 'id', 'visible' => false],
                        'type',
                        'class',
                        'aircraft_types:ntext',
                        'remarks:ntext',
                        ['attribute' => 'lock', 'visible' => false],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [
                                'view'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['simulator/view', 'id'=>$model->id], ['title'=>Yii::t('backend/simulator', 'View')]);
                                },
                                'update'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['simulator/update', 'id'=>$model->id], ['title'=>Yii::t('backend/simulator', 'Update')]);
                                },
                                'delete'=>function($url,$model,$key){
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['simulator/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/simulator', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/simulator', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/simulator', 'Delete')]);
                                },
                            ],
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerSimulator,
                        'pjax' => true,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-simulator']],
                        'layout' => $layout,
                        'columns' => $gridColumnSimulator,
                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/simulator', 'Simulator')),
                            '{footer}' => Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Simulator To School'), ['simulator/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-info kv-batch-create'])
                        ],
                    ]);
                } else {

                    echo Html::a('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Simulator To School'), ['simulator/create', 'school_id'=>$model->id], ['type' => 'button', 'class' => 'btn btn-info kv-batch-create']);
                }
                ?>
            </div>
        </div>
    </div>

</div>
