<div class="form-group" id="add-aircraft">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Aircraft',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'aircraftType.name' => ['type' => TabularForm::INPUT_TEXT],
        'register_no' => ['type' => TabularForm::INPUT_TEXT],
        'multi_engine' => ['type' => TabularForm::INPUT_TEXT],
        'aircraft_year' => ['type' => TabularForm::INPUT_TEXT],
        'engine_year' => ['type' => TabularForm::INPUT_TEXT],
        'glass_cockpit' => ['type' => TabularForm::INPUT_TEXT],
        'gps' => ['type' => TabularForm::INPUT_DROPDOWN_LIST, 'items'=>['0'=>Yii::t('backend', 'No'), '1'=>Yii::t('backend', 'Yes')]],
        'radio' => ['type' => TabularForm::INPUT_DROPDOWN_LIST, 'items'=>['0'=>Yii::t('backend', 'No'), '1'=>Yii::t('backend', 'Yes')]],
        'ils' => ['type' => TabularForm::INPUT_DROPDOWN_LIST, 'items'=>['0'=>Yii::t('backend', 'No'), '1'=>Yii::t('backend', 'Yes')]],
        'night' => ['type' => TabularForm::INPUT_DROPDOWN_LIST, 'items'=>['0'=>Yii::t('backend', 'No'), '1'=>Yii::t('backend', 'Yes')]],
        'ifr' => ['type' => TabularForm::INPUT_DROPDOWN_LIST, 'items'=>['0'=>Yii::t('backend', 'No'), '1'=>Yii::t('backend', 'Yes')]],
        'remarks' => ['type' => TabularForm::INPUT_TEXTAREA],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/school', 'Delete'), 'onClick' => 'delRowAircraft(' . $key . '); return false;', 'id' => 'aircraft-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Aircraft'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAircraft()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

