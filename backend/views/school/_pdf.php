<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\School */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\school', 'School'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend\school', 'School').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'street',
        'street_no',
        'city',
        'post_code',
        'coordinates:ntext',
        'www',
        'email:email',
        'phone',
        'contact_person',
        'has_en',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAircraft->totalCount){
    $gridColumnAircraft = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'type',
        'register_no',
        'multi_engine',
        'aircraft_year',
        'engine_year',
        'glass_cockpit',
        'gps',
        'radio',
        'ils',
        'night',
        'ifr',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAircraft,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend\school', 'Aircraft')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAircraft
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerTraining->totalCount){
    $gridColumnTraining = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
                [
                'attribute' => 'license.id',
                'label' => Yii::t('backend\school', 'License')
            ],
        [
                'attribute' => 'trainingType.id',
                'label' => Yii::t('backend\school', 'Training Type')
            ],
        'address_theory:ntext',
        'theory_hours',
        'elearning_hours',
        'theory_type',
        'address_practice:ntext',
        'aircraft_hours',
        'simulator_hours',
        'start_text',
        'start_date',
        'price',
        'price_theory_group',
        'price_theory_individually',
        'price_pracitce_cheap',
        'price_pracitce_expensive',
        'price_additional',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerTraining,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend\school', 'Training')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnTraining
    ]);
}
?>
    </div>
</div>
