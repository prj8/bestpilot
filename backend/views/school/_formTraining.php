<div class="form-group" id="add-training">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Training',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'name' => ['type' => TabularForm::INPUT_TEXT],
        'license_id' => [
            'label' => 'License types',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\LicenseType::find()->orderBy('id')->asArray()->all(), 'id', 'license'),
                'options' => ['placeholder' => Yii::t('backend/school', 'Choose License types')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'training_type_id' => [
            'label' => 'Training types',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->orderBy('id')->asArray()->all(), 'id', 'type'),
                'options' => ['placeholder' => Yii::t('backend/school', 'Choose Training types')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'address_theory' => ['type' => TabularForm::INPUT_TEXTAREA],
        'theory_hours' => ['type' => TabularForm::INPUT_TEXT],
        'elearning_hours' => ['type' => TabularForm::INPUT_TEXT],
        'theory_type' => ['type' => TabularForm::INPUT_TEXT],
        'address_practice' => ['type' => TabularForm::INPUT_TEXTAREA],
        'aircraft_hours' => ['type' => TabularForm::INPUT_TEXT],
        'simulator_hours' => ['type' => TabularForm::INPUT_TEXT],

        'price' => ['type' => TabularForm::INPUT_TEXT],

        'remarks' => ['type' => TabularForm::INPUT_TEXTAREA],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/school', 'Delete'), 'onClick' => 'delRowTraining(' . $key . '); return false;', 'id' => 'training-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Training'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowTraining()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

