<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\School */

$this->title = Yii::t('backend/school', 'Create School');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/school', 'School'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/school', 'Add new school');

?>
<div class="school-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
