
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);



echo TabularForm::widget([
    'id'=>'add-addresses',
    'dataProvider' => $dataProvider,
    'formName' => 'Addresses',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'id' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => ['type' => TabularForm::INPUT_HIDDEN],
        'country_id' => [
            'label' => 'Country',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Country::find()->orderBy('country')->asArray()->all(), 'id', 'country'),
                'options' => ['placeholder' => Yii::t('backend/school', 'Choose country')],
            ],
            'columnOptions' => ['width' => '20%']
        ],
        'city' => [
            'label' => 'City',
            'type' => TabularForm::INPUT_TEXT,
            'columnOptions' => ['width' => '20%']
        ],
        'zip-code' => [
            'label' => 'Zip code',
            'type' => TabularForm::INPUT_TEXT,
            'columnOptions' => ['width' => '10%']
        ],
        'address' => [
            'label' => 'Address',
            'type' => TabularForm::INPUT_TEXT,
            'columnOptions' => ['width' => '20%']
        ],
        'coordinates' => [
            'label' => 'Coordinates',
            'type' => TabularForm::INPUT_TEXT,
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/school', 'Delete'), 'onClick' => 'delRowAddresses(' . $key . '); return false;', 'id' => 'addresses-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Adddress'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAddresses()']),
        ]
    ]
]);


?>

