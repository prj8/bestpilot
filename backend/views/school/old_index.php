<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SchoolSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend\school', 'School');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\school', 'View schools');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="school-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend\school', 'Create School'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend\school', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
		['attribute' => 'id', 'visible' => false],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'name',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'300px',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'street',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'street_no',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'50px',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'city',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'100px',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'post_code',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'70px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'www',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'email',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'phone',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'contact_person',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'200px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'has_en',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
				'header'=>'In english'
			],
			'value'=>function($data){ return ($data->has_en==1 ? Yii::t('app', 'Yes'):Yii::t('app', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'50px',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'remarks',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'100px',
			'pageSummary'=>true
		],
        [
            'class' => 'yii\grid\ActionColumn',
        ],
    ]; 
	
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-school']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
