
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);



echo TabularForm::widget([
    'id'=>'add-users',
    'dataProvider' => $dataProvider,
    'formName' => 'Users',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'id' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => ['type' => TabularForm::INPUT_HIDDEN],
        'user_id' => [
            'label' => 'User',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('username')->asArray()->all(), 'id', function($ar, $dv){return $ar['username'].'('.$ar['email'].')';}),
                'options' => ['placeholder' => Yii::t('backend/school', 'Choose user')],
            ],
            'columnOptions' => ['width' => '20%']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/school', 'Delete'), 'onClick' => 'delRowUsers(' . $key . '); return false;', 'id' => 'users-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/school', 'Add Users'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowUsers()']),
        ]
    ]
]);


?>

