<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\ActiveForm;
use webvimark\modules\UserManagement\components\UserConfig;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\School */

$this->title = Yii::t('backend/school', 'Update {modelClass}: ', [
        'modelClass' => 'School',
    ]) . ' ' . $model->name;

$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/school', 'School'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/school', 'Update');

$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/school', 'Add User to school');

?>
<div class="school-update">



    <div class="row">
        <div class="col-md-9">
            <?php
            $gridColumn = [
                ['attribute' => 'id', 'visible' => false],
                [
                    'label' => 'Username',
                    'value' => function($model){
                        return $model->user->username;
                    },
                    'hAlign'=>'right',
                    'vAlign'=>'middle',
                    'width'=>'300px',
                    'pageSummary'=>true
                ],
                [
                    'label' => 'Email',
                    'value' => function($model){
                        return $model->user->email;
                    },
                    'hAlign'=>'right',
                    'vAlign'=>'middle',
                    'width'=>'300px',
                    'pageSummary'=>true
                ],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'headerOptions' => [
                        'class' => 'action-column-min',
                        'style'=>'width:7%'
                    ],
                    'template' => '{delete}',
                    'buttons' => [
                        'delete'=>function($url,$model,$key){
                            if (Yii::$app->user->id == $model->user_id) return '';
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['school/user-delete', 'id'=>$model->id], ['title'=>Yii::t('backend/school', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/school', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/school', 'Delete')]);
                        },
                    ],
                ],
            ];
            ?>
            <?php
            $layout = <<< HTML
                    <!-- Widget head -->
                    <div class="widget-head">
                      <div class="pull-left">{heading}</div>
                      <div class="widget-icons pull-right">
                        <a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
                        <a class="wclose" href="#"><i class="fa fa-times"></i></a>
                      </div>
                      <div class="pull-right">{summary}</div>
                      <div class="clearfix"></div>
                    </div>             
                    <!-- Widget content -->
                    <div class="widget-content">
                    {items}
                    {pager}
                    <div class="widget-foot">
                        {footer}
                        <div class="clearfix"></div>
                    </div>
                    </div>
                    <!-- Widget ends -->
                    
HTML;
            ?>
            <div class="widget worange">
            <?= GridView::widget([
                'dataProvider' => $providerUsers,
                'columns' => $gridColumn,
                'pjax' => true,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-users']],
                'layout' => $layout,
                'replaceTags' => [
                    '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/school', 'School Admins')),
                    '{footer}' => ''
                ],
            ]); ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-9">

                <?= Html::beginForm(['school/users', 'id'=>Yii::$app->request->get('id')], 'post', ['data-pjax' => '', 'class' => 'form-horizontal']); ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label text-right">Adres email</label>
                        <div class='col-sm-8'>
                            <?= Html::input('text', 'email', Yii::$app->request->post('email'), ['class' => 'form-control']) ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label text-right">&nbsp;</label>
                        <div class='col-sm-8'>
                            <?= Html::submitButton('Wyślij zaproszenie', ['class' => 'btn btn-lg btn-primary', 'name' => 'hash-button']) ?>
                        </div>
                    </div>

                <?= Html::endForm() ?>

            <?php

            /*
                if (!\webvimark\modules\UserManagement\models\User::hasRole('superadmin'))
                {
                    //Yii::$app->u

                } else {
                    echo $form->field($userModel, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
                        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('username')->asArray()->all(), 'id', function($ar, $dv){return $ar['username'].'('.$ar['email'].')';}),
                        'options' => ['placeholder' => Yii::t('backend\training', 'Choose User')],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                }

               */

            ?>





        </div>
    </div>


</div>
