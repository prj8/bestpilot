<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\School */
/* @var $form yii\widgets\ActiveForm */


\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
	'viewParams' => [
		'class' => 'Addresses',
		'relID' => 'addresses',
		'value' => \yii\helpers\Json::encode($model->addresses),
		'isNewRecord' => ($model->isNewRecord) ? 1 : 0
	]
]);

?>

<div class="school-form">
	<?php $form = ActiveForm::begin([
		'options'=>['enctype'=>'multipart/form-data', 'class' => 'form-horizontal'], // important
		'fieldConfig' => [
			'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
			'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
			/*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
			'errorOptions' => ['class'=>'error'],
			'hintOptions' => [],
		],
	]); ?>
<div class="row">
		<div class="col-md-9">


    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>
	
    <?= $form->field($model, 'logo')->widget(FileInput::classname(), [
						'options' => ['accept' => 'image/*', 'multiple' => false],
						'pluginOptions'=>[
								'showUpload' => false,
								'showRemove' => false,
								'allowedFileExtensions'=>['jpg','gif','png'],
								'initialPreview'=> $model->logo != NULL ? ['<img style="max-width:200px;" src="http://bestpilot.prj8.pl/upload/logos/' .$model->logo.'" />'] : [],
								'overwriteInitial'=>true,
								'initialPreviewConfig' => [['url'=>Url::to(['school/delete-logo', 'id'=>$model->id]), 'extra'=>['id'=>$model->id]]]
								],
							]); 
	?>

    <?= $form->field($model, 'street')->textInput(['maxlength' => true, 'placeholder' => 'Street']) ?>

    <?= $form->field($model, 'street_no')->textInput(['maxlength' => true, 'placeholder' => 'Street No']) ?> 

    <?= $form->field($model, 'city')->textInput(['maxlength' => true, 'placeholder' => 'City']) ?>

	<?= $form->field($model, 'country_id')->widget(\kartik\widgets\Select2::classname(), [
		'data' => \yii\helpers\ArrayHelper::map(\backend\models\Country::find()->orderBy('id')->asArray()->all(), 'id', 'country'),
		'options' => ['placeholder' => Yii::t('backend\training', 'Choose Country')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]); ?>

    <?= $form->field($model, 'post_code')->textInput(['maxlength' => true, 'placeholder' => 'Post Code']) ?>

    <?= $form->field($model, 'coordinates')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'www')->textInput(['maxlength' => true, 'placeholder' => 'Www']) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Phone']) ?>

    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true, 'placeholder' => 'Contact Person']) ?>

    <?= $form->field($model, 'has_en')->dropDownList(['0'=>Yii::t('backend\school', 'No'), '1'=>Yii::t('backend\school', 'Yes')]) ?>

	<?= $form->field($model, 'active', ['template' => "<div class='col-sm-2'>{label}</div>\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>"])->checkbox(['label' => 'active', ]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<?php
			$forms = [
				[
					'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend\school', 'Addresses')),
					'content' => $this->render('_formAddress', [
						'row' => \yii\helpers\ArrayHelper::toArray($model->addresses)
					]),
				],
			];

			if ($model->id != 0)
				echo kartik\tabs\TabsX::widget([
					'items' => $forms,
					'position' => kartik\tabs\TabsX::POS_ABOVE,
					'encodeLabels' => false,
					'pluginOptions' => [
						'bordered' => true,
						'sideways' => true,
						'enableCache' => false,
					],
				]);
			?>




		</div>
	</div>

	<div class="row">
		<div class="col-md-12">

			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('backend\school', 'Create') : Yii::t('backend\school', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>


		</div>
	</div>


	<?php ActiveForm::end(); ?>
</div>
