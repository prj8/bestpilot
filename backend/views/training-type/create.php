<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\TrainingType */

$this->title = Yii::t('backend\training', 'Create Training Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'Training Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
//$this->params['page_subheader'] = Yii::t('backend\training', 'Create new Training Type');
?>
<div class="training-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
