<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingType */

$this->title = Yii::t('backend\training', 'Update {modelClass}: ', [
    'modelClass' => 'Training Type',
]) . ' ' . $model->type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'Training Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->type, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend\training', 'Update');
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\training', 'Edit Training Type details');
?>
<div class="training-type-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
