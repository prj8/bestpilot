<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingType */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="training-type-form">
	<div class="row">
		<div class="col-md-9 col-lg-9 col-sm-9">

    <?php $form = ActiveForm::begin([
		'options' => ['class' => 'form-horizontal'],
		'fieldConfig' => [
			'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
			'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
    ],
]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'placeholder' => 'Type']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
		<div class="col-md-2">
			<?= Html::submitButton($model->isNewRecord ? Yii::t('backend\training', 'Create') : Yii::t('backend\training', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		</div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
