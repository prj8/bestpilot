<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend\training', 'Training Type');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\training', 'View Training Types');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="training-type-index">


    <p>
        <?= Html::a(Yii::t('backend\training', 'Create Training Type'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php 
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn', 'visible' => false],
        ['attribute' => 'id', 'visible' => false],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'type',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'pageSummary'=>true,
			'headerOptions'=>['hAlign'=>'left','vAlign'=>'bottom']
		],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
			'visibleButtons' => ['view'=>false]
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-training-type']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_PDF => false
                ]
            ]) ,
        ],
    ]); ?>

</div>


<?php

$this->registerJs("

		$('.kv-editable').click(function(e){
			$('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
			return true;
		});
	");

?>