<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\model\base\Country */

$this->title = $model->country;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app\country', 'Country'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = 'Aircraft details view';
?>
<div class="country-view">

    <div class="row">
        <div class="col-sm-9">
        </div>
        <div class="col-sm-3 text-right" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('app\country', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app\country', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app\country', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id'],
        'country',
        'currency_id',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
        </div>
    </div>


</div>
