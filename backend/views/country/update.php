<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\backend\model\base\Country */

$this->title = Yii::t('app\country', 'Update {modelClass}: ', [
    'modelClass' => 'Country',
]) . ' ' . $model->country;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app\country', 'Country'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->country, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app\country', 'Update');

$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\aircraft', 'Edit Aircraft');
?>
<div class="country-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
