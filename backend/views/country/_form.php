<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


?>

<div class="country-form">

    <?php $form = ActiveForm::begin([
        'options'=>['class' => 'form-horizontal'], // important
        'fieldConfig' => [
            'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
            'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
        ],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id')->textInput(['maxlength' => true, 'placeholder' => 'ID']); ?>

    <?php
        echo $form->field($model, 'country')->textInput(['maxlength' => true, 'placeholder' => 'Country ']);
        $Languages = \yii\helpers\ArrayHelper::map(\lajax\translatemanager\models\Language::find()->where(['=', 'status', '1'])->asArray()->all(), 'language_id', 'language');
        foreach ($Languages as $key => $value) {
            if ('pl' != $key)
                echo $form->field($model->translate($key), "[$key]country")->textInput(['maxlength' => true, 'placeholder' => 'Country'])->label(Yii::t('backend/country', 'Country').' '.$value);
        }
    ?>

    <?= $form->field($model, 'currency_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\Currency::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('backend/country', 'Choose Currency')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/country', 'Create') : Yii::t('backend/country', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
