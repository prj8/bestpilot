<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\KingAppAsset;
//use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use webvimark\modules\UserManagement\components\GhostMenu;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\components\UserConfig;

KingAppAsset::register($this);

$controllerPath = '';
$moduleId = '';
if(Yii::$app->controller->module->id) {
	$controllerPath ='/'.Yii::$app->controller->module->id;
	$moduleId = Yii::$app->controller->module->id;
}
$controllerPath .= '/'.Yii::$app->controller->id;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
			<div class="container">
				<!-- Menu button for smallar screens -->
				<div class="navbar-header">
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<?= Html::a('Best<span class="bold">Pilot</span>', ['/'], ['class' => 'navbar-brand']) ?>
				</div>
				<!-- Site name for smallar screens -->
				<!-- Navigation starts -->
				<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">     
					<!-- Links -->
					<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">            
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<!-- <img src="/img/user.jpg" alt="" class="nav-user-pic img-responsive" /> --> <?= Yii::$app->user->username ?> <b class="caret"></b>
							</a>
							<!-- Dropdown menu -->
							<ul class="dropdown-menu">
							<li><?= Html::a('<i class="fa fa-user"></i> Profile', ['/user-management/user/update', 'id'=>Yii::$app->user->id], ['class' => '']) ?></li>
							<li><?= Html::a('<i class="fa fa-cogs"></i> Settings</a>', ['/user-management/auth/change-own-password'], ['class' => '']) ?><a href="#"></li>
							<li><?= Html::a('<i class="fa fa-power-off"></i> Logout', ['/user-management/auth/logout'], ['class' => '']) ?></li>
							</ul>
						</li>
					</ul>
					


					
					<!-- Notifications -->
					<!-- cuted out -->
					<!-- -->
					
				</nav>
			</div>
		</div>
		
		
		<!-- Main content starts -->
		<div class="content">
			<!-- Sidebar -->
			<div class="sidebar">
				<div class="sidebar-dropdown"><a href="#">Navigation</a></div>
				<div class="sidebar-inner" style="padding-top:53px;">
					<!-- Search form -->
					<!--
					<div class="sidebar-widget">
						<form >
							<input type="text" class="form-control" placeholder="Search">
						</form>
					</div>
					-->
					<!--- Sidebar navigation -->
					<!-- If the main navigation has sub navigation, then add the class "has_submenu" to "li" of main navigation. -->
					
					<!-- Use the class nred, ngreen, nblue, nlightblue, nviolet or norange to add background color. You need to use this in <li> tag. -->
					
					<?php
						echo GhostMenu::widget([
							'encodeLabels'=>false,
							'activateParents'=>true,
							'activateItems'=>true,
							'options'=>['class'=>'navi'],
							'items' => [
								[
									'label' => '<i class="fa fa-desktop"></i> Dashboard',
									'url' => ['/site/index'],
									'options'=>['class'=>'ngreen'],
								],
								[
									'label' => 'Training',
									'url' => ['/training/index'],
									'options'=>['class'=>'nred'],
								],
								[
									'label' => 'Schools',
									'url' => ['/school/index'],
									'options'=>['class'=>'nblue'],
								],
                                [
                                    'label' => 'School rates',
                                    'url' => ['/school-rates/index'],
                                    'options'=>['class'=>'nred'],
                                ],
								[
									'label' => 'Aircraft',
									'url' => ['/aircraft/index'],
									'options'=>['class'=>'norange'],
								],
								[
									'label' => 'Aircraft Types',
									'url' => ['/aircraft-type/index'],
									'options'=>['class'=>'nlightblue'],
								],
								[
									'label' => 'Simulators',
									'url' => ['/simulator/index'],
									'options'=>['class'=>'ngreen'],
								],
								[
									'label' => 'Licence Types',
									'url' => ['/license-type/index'],
									'options'=>['class'=>'ngreen'],
								],
								[
									'label' => 'Training Types',
									'url' => ['/training-type/index'],
									'options'=>['class'=>'ngreen'],
								],
								[
									'label' => 'Countries',
									'url' => ['/country/index'],
									'options'=>['class'=>'nred'],
								],
								[
									'label' => '<a href="#">Authorization</a>',
									'options'=>['class'=>'has_submenu nlightblue'],
									'items'=>UserManagementModule::menuItems()
								],
								[
									'label' => Yii::t('left-menu', 'Language'),
									'options'=>['class'=>'has_submenu nred'],
									'url' => ['/translatemanager/language'],
									'icon' => 'fa fa-language',
									'visible' => Yii::$app->user->can('/translatemanager/*'),
									'items' => [
										[
											'label' => Yii::t('left-menu', 'List of languages'),
											'url' => ['/translatemanager/language/list'],
											'active' => $controllerPath === '/translatemanager/language/list'
										],
										[
											'label' => Yii::t('left-menu', 'Create'),
											'url' => ['/translatemanager/language/create'],
											'active' => $controllerPath === '/translatemanager/language/create'
										],
										['label' => Yii::t('left-menu', 'Scan'), 'url' => ['/translatemanager/language/scan']],
										['label' => Yii::t('left-menu', 'Optimize'), 'url' => ['/translatemanager/language/optimizer']]
									]
								],
								/*
								[
									'label' => '<a href="#">Frontend routes</a>',
									'options'=>['class'=>'has_submenu nred'],
									'items'=>[
										['label'=>'Login', 'url'=>['/user-management/auth/login']],
										['label'=>'Logout', 'url'=>['/user-management/auth/logout']],
										['label'=>'Registration', 'url'=>['/user-management/auth/registration']],
										['label'=>'Change own password', 'url'=>['/user-management/auth/change-own-password']],
										['label'=>'Password recovery', 'url'=>['/user-management/auth/password-recovery']],
										['label'=>'E-mail confirmation', 'url'=>['/user-management/auth/confirm-email']],
									],
								],*/
							],
						]);
						?>
					<!--/ Sidebar navigation -->

					<!-- Date -->
					<div class="sidebar-widget">
					
					<!--	KALENDARZ  -->
					<!--	
						<div id="todaydate"></div>
					-->
					</div>
				</div>
			</div>
			<!-- Sidebar ends -->

			<!-- Main bar -->
			<div class="mainbar">
				<!-- Page heading -->
				<div class="page-head">
					<!-- Page heading -->
					<h2 class="pull-left"><?= isset($this->params['page_header'])? $this->params['page_header']:'' ?>
					  <!-- page meta -->
					  <span class="page-meta"><?= isset($this->params['page_subheader'])? $this->params['page_subheader']:'' ?></span>
					</h2>
					<!-- Breadcrumb -->
					<div class="bread-crumb pull-right">
					 <?= Breadcrumbs::widget([
						'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
						 'homeLink' => ['label'=>Yii::t('admin/home', 'Home'), 'url'=>['site/index']],
					]) ?>
					</div>
					<div class="clearfix"></div>
				</div><!--/ Page heading ends -->

				<!-- Matter -->
				<div class="matter">
					<div class="container-fluid">
					  <div class="row-fluid">
							<?= Alert::widget() ?>
							<!-- Content -->
							<?= $content ?>
					  </div>
					</div>
				</div><!--/ Matter ends -->
			</div><!--/ Mainbar ends -->	    	
			<div class="clearfix"></div>
		</div><!--/ Content ends -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
