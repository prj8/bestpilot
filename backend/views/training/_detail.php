<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

?>
<div class="training-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->name) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend\training', 'School'),
        ],
        [
            'attribute' => 'license.id',
            'label' => Yii::t('backend\training', 'License'),
        ],
        [
            'attribute' => 'trainingType.id',
            'label' => Yii::t('backend\training', 'Training Type'),
        ],
        'address_theory:ntext',
        'theory_hours',
        'theory_details:ntext',
        'elearning_hours',
        'elearning_details:ntext',
        'theory_type',
        'address_practice:ntext',
        'aircraft_hours',
        'simulator_hours',
        'start_text',
        'start_date',
        'price',
        'price_theory_group',
        'price_theory_individually',
        'price_pracitce_cheap',
        'price_pracitce_expensive',
        'price_additional',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>