<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Training */

$this->title = Yii::t('backend/training', 'Create Training');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/training', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
//$this->params['page_subheader'] = Yii::t('backend/training', 'Create Training');
?>
<div class="training-create">

  

    <?= $this->render('_form', [
        'model' => $model,
        'school_name' => $school_name,
    ]) ?>

</div>
