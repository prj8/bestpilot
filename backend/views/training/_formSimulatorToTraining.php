<?php
/**
 * Created by PhpStorm.
 * User: Karol
 * Date: 2016-11-03
 * Time: 13:52
 */
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);


echo TabularForm::widget([
    'id'=>'add-simulator',
    'dataProvider' => $dataProvider,
    'formName' => 'Simulator',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'id' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => [
            'label' => 'Simulator',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Simulator::find()->where(['school_id'=>$school_id])->orderBy('id')->asArray()->all(), 'id', function($ar, $dv){return $ar['type'];}),
                'options' => ['placeholder' => Yii::t('backend/training', 'Choose Simulator')],
            ],
            'columnOptions' => ['width' => '50%']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/training', 'Delete'), 'onClick' => 'delRowSimulator(' . $key . '); return false;', 'id' => 'simulator-to-training-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Simulator To Training'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowSimulator()']).Html::hiddenInput('school_id', $school_id),
        ]
    ]
]);


?>

<?php
/**
 * Created by PhpStorm.
 * User: Karol
 * Date: 2016-11-03
 * Time: 14:02
 */