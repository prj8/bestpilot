
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);


echo TabularForm::widget([
    'id'=>'add-aircraft',
    'dataProvider' => $dataProvider,
    'formName' => 'Aircraft',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'id' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => [
            'label' => 'Aircraft',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Aircraft::find()->where(['school_id'=>$school_id])->orderBy('id')->all(), 'id', function($ar, $dv){return isset($ar['aircraftTypeName']) ? $ar['aircraftTypeName'].' ('.$ar['register_no'].')' : $ar['id'];}),
                'options' => ['placeholder' => Yii::t('backend/training', 'Choose Aircraft')],
            ],
            'columnOptions' => ['width' => '50%']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/training', 'Delete'), 'onClick' => 'delRowAircraft(' . $key . '); return false;', 'id' => 'aircraft-to-training-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Aircraft To Training'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAircraft()']).Html::hiddenInput('school_id', $school_id),
        ]
    ]
]);


?>

