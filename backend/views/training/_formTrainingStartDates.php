
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;


$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);



echo TabularForm::widget([
    'id'=>'add-start-dates',
    'dataProvider' => $dataProvider,
    'formName' => 'StartDates',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'id' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        'id' => ['type' => TabularForm::INPUT_HIDDEN],
        'start_text' => [
            'label' => 'Start Date Text',
            'type' => TabularForm::INPUT_TEXT,
            'columnOptions' => ['width' => '40%']
        ],
        'start_date' => [
            'label' => 'Start Date Text',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\datecontrol\DateControl::classname(),
            'options' => [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => Yii::t('backend/training', 'Choose Start Date'),
                        'autoclose' => true,
                    ],
                ],
            ],
            'columnOptions' => ['width' => '50%']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/training', 'Delete'), 'onClick' => 'delRowStartDates(' . $key . '); return false;', 'id' => 'start-date-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/training', 'Add Start Date'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowStartDates()']),
        ]
    ]
]);


?>

