<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-training-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend\training', 'Choose School')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'license_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\LicenseType::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('backend\training', 'Choose License types')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'training_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('backend\training', 'Choose Training types')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php /* echo $form->field($model, 'address_theory')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'theory_hours')->textInput(['placeholder' => 'Theory Hours']) */ ?>

    <?php /* echo $form->field($model, 'elearning_hours')->textInput(['placeholder' => 'Elearning Hours']) */ ?>

    <?php /* echo $form->field($model, 'theory_type')->textInput(['maxlength' => true, 'placeholder' => 'Theory Type']) */ ?>

    <?php /* echo $form->field($model, 'address_practice')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'aircraft_hours')->textInput(['placeholder' => 'Aircraft Hours']) */ ?>

    <?php /* echo $form->field($model, 'simulator_hours')->textInput(['placeholder' => 'Simulator Hours']) */ ?>

    <?php /* echo $form->field($model, 'start_text')->textInput(['maxlength' => true, 'placeholder' => 'Start Text']) */ ?>

    <?php /* echo $form->field($model, 'start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATETIME,
        'saveFormat' => 'php:Y-m-d H:i:s',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('backend\training', 'Choose Start Date'),
                'autoclose' => true,
            ]
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'price')->textInput(['placeholder' => 'Price']) */ ?>

    <?php /* echo $form->field($model, 'price_theory_group')->textInput(['placeholder' => 'Price Theory Group']) */ ?>

    <?php /* echo $form->field($model, 'price_theory_individually')->textInput(['placeholder' => 'Price Theory Individually']) */ ?>

    <?php /* echo $form->field($model, 'price_pracitce_cheap')->textInput(['placeholder' => 'Price Pracitce Cheap']) */ ?>

    <?php /* echo $form->field($model, 'price_pracitce_expensive')->textInput(['placeholder' => 'Price Pracitce Expensive']) */ ?>

    <?php /* echo $form->field($model, 'price_additional')->textInput(['placeholder' => 'Price Additional']) */ ?>

    <?php /* echo $form->field($model, 'remarks')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend\training', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend\training', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
