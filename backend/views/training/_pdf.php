<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

/*$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
*/
?>
<div class="training-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend\training', 'Training').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'school.name',
                'label' => Yii::t('backend\training', 'School')
            ],
        [
                'attribute' => 'licenseType.license',
                'label' => Yii::t('backend\training', 'License')
            ],
        [
                'attribute' => 'trainingType.type',
                'label' => Yii::t('backend\training', 'Training Type')
            ],
        'address_theory:ntext',
        'theory_hours',
        'elearning_hours',
        'theory_type',
        'address_practice:ntext',
        'aircraft_hours',
        'simulator_hours',
        'start_text',
        'start_date',
        'price',
        'price_theory_group',
        'price_theory_individually',
        'price_pracitce_cheap',
        'price_pracitce_expensive',
        'price_additional',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAircraftToTraining->totalCount){
    $gridColumnAircraftToTraining = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'aircraft.id',
                'label' => Yii::t('backend\training', 'Aircraft')
            ],
            ];
    echo Gridview::widget([
        'dataProvider' => $providerAircraftToTraining,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend\training', 'Aircraft To Training')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAircraftToTraining
    ]);
}
?>
    </div>
</div>
