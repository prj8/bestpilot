<?php

use yii\helpers\Html;
use \kartik\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Training */
/* @var $providerAircraftToTraining backend\models\Aircraft */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'Aircraft',
        'relID' => 'aircraft',
        'value' => \yii\helpers\Json::encode($model->aircraft),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
        'class' => 'Simulator',
        'relID' => 'simulator',
        'value' => \yii\helpers\Json::encode($model->simulator),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
	'viewParams' => [
		'class' => 'StartDates',
		'relID' => 'start-dates',
		'value' => \yii\helpers\Json::encode($model->startDates),
		'isNewRecord' => ($model->isNewRecord) ? 1 : 0
	]
]);

?>


<?php $form = ActiveForm::begin([
	'id'=>'aircraft',
	'options' => ['class' => 'form-horizontal', 'name'=>'aircraft'],
	'fieldConfig' => [
		'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
		'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
		/*'offsetOptions' => ['class' => 'col-sm-offset-4'],
        'wrapperOptions' => ['class' => 'col-sm-8'],*/
		'errorOptions' => ['class'=>'error'],
		'hintOptions' => [],
	],
]); ?>

<div class="training-form">
	<div class="row">
		<div class="col-md-9">

			<?php

				$model->active = $model->active === NULL ? 1 : $model->active;
				$model->theory_hours = $model->theory_hours === NULL ? 0 : $model->theory_hours;
				$model->elearning_hours = $model->elearning_hours === NULL ? 0 : $model->elearning_hours;
				$model->aircraft_hours = $model->aircraft_hours === NULL ? 0 : $model->aircraft_hours;
				$model->simulator_hours = $model->simulator_hours === NULL ? 0 : $model->simulator_hours;
				$model->currency_id = $model->currency_id === NULL ? 'PLN' : $model->currency_id;

			?>



    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

   <?php if ($model->school_id != 0): ?>
	<?= $form->field($model, 'school_id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
	<div class="form-group field-aircraft-type">
		<label for="aircraft-type" class=" col-md-2 control-label text-right"><?= Yii::t('backend/aircraft', 'School name') ?></label>
		<div class="col-sm-8">
			<?= Html::input('text', 'schoolname', $school_name, ['class'=>'form-control', 'readonly'=>'true']) ?>
		</div>
		<div class="help-block"></div>
	</div>
    <?php else: ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose School')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
	
	<?php endif; ?>

    <?= $form->field($model, 'license_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\LicenseType::find()->orderBy('id')->asArray()->all(), 'id', 'license'),
        'options' => ['placeholder' => Yii::t('backend/training', 'Choose License type')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'training_type_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->orderBy('id')->asArray()->all(), 'id', 'type'),
        'options' => ['placeholder' => Yii::t('backend/training', 'Choose Training type')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


	<?= $form->field($model, 'address_theory_id')->widget(\kartik\widgets\Select2::classname(), [
		'data' => \yii\helpers\ArrayHelper::map($model->school->addresses, 'id', function($ar, $dv){return $ar['address'].' ('.$ar['city'].'/'.$ar['countryName'].')';}),
		'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose theory address')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]); ?>

    <?= $form->field($model, 'theory_hours')->textInput(['placeholder' => 'Theory Hours']) ?>

	<?= $form->field($model, 'theory_details')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'elearning_hours')->textInput(['placeholder' => 'Elearning Hours']) ?>

	<?= $form->field($model, 'elearning_details')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'theory_type')->textInput(['maxlength' => true, 'placeholder' => 'Theory Type']) ?>

	<?= $form->field($model, 'address_practice_id')->widget(\kartik\widgets\Select2::classname(), [
		'data' => \yii\helpers\ArrayHelper::map($model->school->addresses, 'id', function($ar, $dv){return $ar['address'].' ('.$ar['city'].'/'.$ar['countryName'].')';}),
		'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose practice address')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]); ?>

    <?= $form->field($model, 'aircraft_hours')->textInput(['placeholder' => 'Aircraft Hours']) ?>

    <?= $form->field($model, 'simulator_hours')->textInput(['placeholder' => 'Simulator Hours']) ?>


    <?php

		/* $form->field($model, 'start_text')->textInput(['maxlength' => true, 'placeholder' => 'Start Text'])

        $form->field($model, 'start_date')->widget(\kartik\datecontrol\DateControl::classname(), [
        'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
        'saveFormat' => 'php:d-m-Y',
        'ajaxConversion' => true,
        'options' => [
            'pluginOptions' => [
                'placeholder' => Yii::t('backend/training', 'Choose Start Date'),
                'autoclose' => true,
            ]
        ],	
    ]); */ ?>

    <?= $form->field($model, 'price')->textInput(['placeholder' => 'Price']) ?>


	<?= $form->field($model, 'currency_id')->widget(\kartik\widgets\Select2::classname(), [
		'data' => \yii\helpers\ArrayHelper::map(\backend\models\Currency::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
		'options' => ['placeholder' => Yii::t('backend/training', 'Choose currency')],
		'pluginOptions' => [
			'allowClear' => false
		],
	]); ?>

    <?php
		/*
		$form->field($model, 'price_theory_group')->textInput(['placeholder' => 'Price Theory Group'])
		$form->field($model, 'price_theory_individually')->textInput(['placeholder' => 'Price Theory Individually'])
		$form->field($model, 'price_pracitce_cheap')->textInput(['placeholder' => 'Price Pracitce Cheap'])
		$form->field($model, 'price_pracitce_expensive')->textInput(['placeholder' => 'Price Pracitce Expensive'])
		$form->field($model, 'price_additional')->textInput(['placeholder' => 'Price Additional'])
		*/
	?>

	<?php
	echo $form->field($model, 'information')->widget(\yii\redactor\widgets\Redactor::className());
	$Languages = \yii\helpers\ArrayHelper::map(\lajax\translatemanager\models\Language::find()->where(['=', 'status', '1'])->asArray()->all(), 'language_id', 'language');

	foreach ($Languages as $key => $value) {
		if ('pl' != $key) {
			echo $form->field($model->translate($key), "[$key]information")->widget(\yii\redactor\widgets\Redactor::className())->label(Yii::t('backend/training', 'Information') . ' ' . $value);
		}
	}
	?>

	
	<?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

			<?= $form->field($model, 'lang')->widget(\kartik\widgets\Select2::classname(), [
				'data' => \yii\helpers\ArrayHelper::map(\lajax\translatemanager\models\Language::find()->orderBy('name_ascii')->asArray()->all(), 'language', 'name_ascii'),
				'options' => ['placeholder' => Yii::t('backend/training', 'Choose languages'), 'multiple' => true],
				'pluginOptions' => [
					'allowClear' => true
				],
			]); ?>

	<?= $form->field($model, 'active', ['template' => "<div class='col-sm-2'>{label}</div>\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>"])->checkbox(['label' => 'active']) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
    <?php
    $forms = [
		[
			'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/training', 'Start Dates')),
			'content' => $this->render('_formTrainingStartDates', [
				'row' => \yii\helpers\ArrayHelper::toArray($model->startDates)
			]),
		],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/training', 'Aircraft to Training')),
            'content' => $this->render('_formAircraftToTraining', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->aircraft),
                'school_id' => $model->school_id
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/training', 'Simulator to Training')),
            'content' => $this->render('_formSimulatorToTraining', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->simulator),
                'school_id' => $model->school_id
            ]),
        ],

    ];
	
	if ($model->school_id != 0)
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
			<div class="form-group">
				<?= Html::submitButton($model->isNewRecord ? Yii::t('backend/training', 'Create') : Yii::t('backend/training', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
			</div>




		</div>
	</div>

</div>

<?php ActiveForm::end(); ?>
