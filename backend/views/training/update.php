<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

$this->title = Yii::t('backend/training', 'Update {modelClass}: ', [
    'modelClass' => 'Training',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/training', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/training', 'Update');
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/training', 'Edit Training');
?>
<div class="training-update">

    

    <?= $this->render('_form', [
        'model' => $model,
        'school_name' => $school_name,
        'providerStartDate' => $providerStartDate,
        'providerAircraftToTraining' => $providerAircraftToTraining,
    ]) ?>

</div>
