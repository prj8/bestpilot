<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/training', 'Training'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/training', 'View Training');
?>
<div class="training-view">

    <div class="row">
        <div class="col-sm-9">
        </div>
        <div class="col-sm-3 text-right" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('backend/training', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('backend/training', 'Will open the generated PDF file in a new window')
                ]
            )?>
            
            <?= Html::a(Yii::t('backend/training', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend/training', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend/training', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">

<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend/training', 'School'),
        ],
        [
            'attribute' => 'licenseType.license',
            'label' => Yii::t('backend/training', 'License'),
        ],
        [
            'attribute' => 'trainingType.type',
            'label' => Yii::t('backend/training', 'Training Type'),
        ],
        'address_theory:ntext',

        [
            'attribute' => 'addressTheory.id',
            'value'=>($model->addressTheory != null ? $model->addressTheory->address.' '.$model->addressTheory->city.'/'.$model->addressTheory->countryName : Yii::t('backend/training', '(not set)')),
            'label' => Yii::t('backend/training', 'Theory address'),
        ],

        'theory_hours',
        'theory_details:ntext',
        'elearning_hours',
        'elearning_details:ntext',
        'theory_type',
        'address_practice:ntext',

        [
            'attribute' => 'addressPractice.id',
            'value'=>($model->addressPractice != null ? $model->addressPractice->address.' '.$model->addressPractice->city.'/'.$model->addressPractice->countryName : Yii::t('backend/training', '(not set)')),
            'label' => Yii::t('backend/training', 'Practice address'),
        ],

        'aircraft_hours',
        'simulator_hours',
        'price',
        'remarks:ntext',
        [
            'value' => implode( \yii\helpers\ArrayHelper::map(
                                    \lajax\translatemanager\models\Language::find()->where(['language_id'=>$model->lang])->asArray()->all(), 'name_ascii', 'name_ascii'), ', '),
            'label' => Yii::t('backend/training', 'Languages'),
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
		</div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <div class="widget wgreen">
                <?php
                $layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		
		</div>
		<!-- Widget ends -->
		
HTML;
                ?>

                <?php
                if($providerStartDate->totalCount){
                    $gridColumnStartDate = [

                        ['attribute' => 'id', 'visible' => false],
                        [
                            'attribute' => 'start_text',
                            'label' => Yii::t('backend/training', 'Start text')
                        ],
                        [
                            'attribute' => 'start_date',
                            'label' => Yii::t('backend/training', 'Start date')
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerStartDate,
                        'pjax' => true,
                        'layout' => $layout,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aircraft-to-training']],

                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/training', 'Start dates'))
                        ],
                        'columns' => $gridColumnStartDate
                    ]);
                }
                ?>

            </div>
        </div>
    </div>



    <div class="row">
        <div class="col-md-12">
            <div class="widget worange">
                <?php
                $layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		
		</div>
		<!-- Widget ends -->
		
HTML;
                ?>

                <?php
                if($providerAircraftToTraining->totalCount){
                    $gridColumnAircraftToTraining = [

                        ['attribute' => 'id', 'visible' => false],
                        [
                            'attribute' => 'type_id',
                            'value' => function($model){return $model->aircraftType->name;},
                            'label' => Yii::t('backend/aircraft', 'Aircraft type')
                        ],
                        [
                            'attribute' => 'register_no',
                            'label' => Yii::t('backend/aircraft', 'Aircraft register no')
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerAircraftToTraining,
                        'pjax' => true,
                        'layout' => $layout,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aircraft-to-training']],

                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/training', 'Aircraft To Training'))
                        ],
                        'columns' => $gridColumnAircraftToTraining
                    ]);
                }
                ?>

            </div>
        </div>
    </div>
	
	
	 <div class="row">
        <div class="col-md-12">
            <div class="widget wgreen">
                <?php
                $layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		
		</div>
		<!-- Widget ends -->
		
HTML;
                ?>

                <?php
                if($providerSimulatorToTraining->totalCount){
                    $gridColumnSimulatorToTraining = [

                        ['attribute' => 'id', 'visible' => false],
                        [
                            'attribute' => 'type',
                            'label' => Yii::t('backend/simulator', 'Simulator type')
                        ],
                        [
                            'attribute' => 'class',
                            'label' => Yii::t('backend/simulator', 'Simulator class')
                        ],
                        [
                            'attribute' => 'aircraft_types',
                            'label' => Yii::t('backend/simulator', 'Aircraft types')
                        ],
                    ];
                    echo Gridview::widget([
                        'dataProvider' => $providerSimulatorToTraining,
                        'pjax' => true,
                        'layout' => $layout,
                        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-simulator-to-training']],

                        'replaceTags' => [
                            '{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/training', 'Simulator To Training'))
                        ],
                        'columns' => $gridColumnSimulatorToTraining
                    ]);
                }
                ?>

            </div>
        </div>
    </div>



	
</div>