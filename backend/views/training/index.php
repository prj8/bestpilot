<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TrainingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = Yii::t('backend/training', 'Trainings');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/training', 'View trainings');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="training-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		<!-- <?= Html::a(Yii::t('backend/training', 'Create Training'), ['create'], ['class' => 'btn btn-success']) ?> -->
        <!-- <?= Html::a(Yii::t('backend/training', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?> -->
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
			'attribute' => 'school_id',
			'label' => Yii::t('backend/training', 'School'),
			'value' => function($model){
				return $model->school->name;
			},
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid-training-search-school_id']
        ],
        [
			'attribute' => 'license_id',
			'label' => Yii::t('backend/training', 'License'),
			'value' => function($model){
				return $model->licenseType->license;
			},
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\LicenseType::find()->asArray()->all(), 'id', 'license'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'License types', 'id' => 'grid-training-search-license_id']
		],
        [
			'class'=>'kartik\grid\EditableColumn',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
				'options'=>[
					'data'=> \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->asArray()->all(), 'id', 'type')
				]
			],
			'attribute' => 'training_type_id',
			'label' => Yii::t('backend/training', 'Training Type'),
			'value' => function($model){
				return $model->trainingType != null ? $model->trainingType->type : 'brak';
			},
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\TrainingType::find()->asArray()->all(), 'id', 'type'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'Training types', 'id' => 'grid-training-search-training_type_id']
		],
        /*'address_theory:ntext',*/
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'theory_hours',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'5%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'elearning_hours',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'5%',
			'pageSummary'=>true
		],
		[
			'attribute' => 'countryName',
			'label' => Yii::t('backend/training', 'Country'),
			'value' => function($model){
				return $model->school->country->country;
			},
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Country::find()->asArray()->all(), 'id', 'country'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'Country', 'id' => 'grid-training-search-country']
		],
		/*[
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'theory_type',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'10%',
			'pageSummary'=>true
		],*/
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'aircraft_hours',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'5%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'simulator_hours',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'5%',
			'pageSummary'=>true
		],
		[
			'attribute'=>'startText',
			'label' => Yii::t('backend/training', 'Start text'),
			'value' => function($model){
				return implode(', ', ArrayHelper::map($model->startDates, 'start_text', 'start_text'));
			},
		],
		[
			'attribute'=>'startDate',
			'label' => Yii::t('backend/training', 'Start date'),
			'value' => function($model){
				return implode(', ', ArrayHelper::map($model->startDates, 'start_date', 'start_date'));
			},
			'filterType'=>GridView::FILTER_DATE_RANGE,
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
				'convertFormat' => 'true',
				'pluginOptions' => ['locale'=>['format'=>'Y-m-d']]
			],

		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'price',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'width'=>'5%',
			'pageSummary'=>true
		],
        /*'address_practice:ntext',*/
        /*'price_theory_group',
        'price_theory_individually',
        'price_pracitce_cheap',
        'price_pracitce_expensive',*/
        /*'price_additional',*/
        'remarks:ntext',
		'updated_at:datetime',
        ['attribute' => 'lock', 'visible' => false],
		[
			'class' => 'yii\grid\ActionColumn',
			'headerOptions' => [
				'class' => 'action-column-min',
				'style'=>'width:7%'
			],
			'template' => '{view} {update} {duplicate} {delete}',
			'buttons' => [
				'view'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['training/view', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'View')]);
				},
				'update'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['training/update', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Update')]);
				},
				'duplicate'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-duplicate"></span>', ['training/duplicate', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Duplicate')]);
				},
				'delete'=>function($url,$model,$key){
					return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['training/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/training', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/training', 'Delete')]);
				},
			],
		],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
		'resizableColumns'=>true,
		'tableOptions'=>['class'=>'training-index'],
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-training']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
		
        'toolbar' => [
            '{toggleData}',
        ],
		
    ]); ?>

</div>
<?php
/*
$this->registerJs("

		$('.kv-editable').click(function(e){
			$('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
			return true;
		});
	");
*/
?>
