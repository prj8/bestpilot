<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Address */

$this->title = Yii::t('backend\training', 'Update {modelClass}: ', [
    'modelClass' => 'Address',
]) . ' ' . $model->address1;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\training', 'Address'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->address1, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend\training', 'Update');
?>
<div class="address-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
