<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Simulator */

$this->title = $model->type;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/simulator', 'Simulator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = 'Simulator details view';
?>
<div class="col-lg-12 col-md-12 simulator-view">

    <div class="row">
        <div class="col-sm-3 text-right pull-right" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend/simulator', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend/simulator', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend/simulator', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
		<div class="col-lg-12 col-md-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend\school', 'School name'),
        ],
        'type',
        'class',
        'aircraft_types:ntext',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
		</div>
    </div>
	 <div class="row">
		<div class="col-md-12">
			<div class="widget wred">
			<?php
		$layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		<div class="widget-foot">
			{footer}
			<div class="clearfix"></div>
		</div>
		</div>
		<!-- Widget ends -->
		
HTML;
	?>
				
<?php
if($providerSimulatorToTraining->totalCount){
    $gridColumnSimulatorToTraining = [
        ['class' => 'yii\grid\SerialColumn', 'visible' => false],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'training.name',
                'label' => Yii::t('backend/simulator', 'Training')
            ],
			[
				'class' => 'yii\grid\ActionColumn',
				'options'=>['class'=>'action-column-max'],
				'buttons' => [
					'view'=>function($url,$model,$key){
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['training/view', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'View')]);
					},
					'update'=>function($url,$model,$key){
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['training/update', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Update')]);
					},
					'delete'=>function($url,$model,$key){
						return ''; /*Html::a('<span class="glyphicon glyphicon-trash"></span>', ['training/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/training', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/training', 'Delete')]);*/
					},
				],
			],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSimulatorToTraining,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-simulator-to-training']],
		'layout' => $layout,
        'columns' => $gridColumnSimulatorToTraining,
		'replaceTags' => [
			'{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/simulator', 'Simulator To Training')),
			'{footer}' => ''
		],
    ]);
} 
?>		
			</div>
		</div>
	</div>
</div>