<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Simulator */

$this->title = Yii::t('backend/simulator', 'Create Simulator');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/simulator', 'Simulator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simulator-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'school_name' => $school_name,
    ]) ?>

</div>
