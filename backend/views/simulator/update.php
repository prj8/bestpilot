<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Simulator */

$this->title = Yii::t('backend/simulator', 'Update {modelClass}: ', [
    'modelClass' => 'Simulator',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/simulator', 'Simulator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/simulator', 'Update');

$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/simulator', 'Edit Simulator');
?>
<div class="simulator-update">


    <?= $this->render('_form', [
        'model' => $model,
        'school_name' => $school_name,
    ]) ?>

</div>
