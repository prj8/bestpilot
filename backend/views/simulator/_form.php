<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Simulator */
/* @var $form yii\widgets\ActiveForm */


\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'SimulatorToTraining', 
        'relID' => 'simulator-to-training', 
        'value' => \yii\helpers\Json::encode($model->simulatorToTrainings),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);

?>

<div class="simulator-form">
	<?php $form = ActiveForm::begin([
		'options'=>['class' => 'form-horizontal'], // important
		'fieldConfig' => [
			'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
			'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
			/*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
			'errorOptions' => ['class'=>'error'],
			'hintOptions' => [],
		],
	]); ?>
<div class="row">
		<div class="col-md-9">



    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?php if ($model->school_id != 0): ?>
	<?= $form->field($model, 'school_id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
	<div class="form-group field-schoolname">
		<label for="schoolname" class=" col-md-2 control-label text-right"><?= Yii::t('backend/simulator', 'School name') ?></label>
		<div class="col-sm-8">
			<?= Html::input('text', 'schoolname', $school_name, ['class'=>'form-control', 'readonly'=>'true']) ?>
		</div>
		<div class="help-block"></div>
	</div>
    <?php else: ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend/simulator', 'Choose School')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
	
	<?php endif; ?>

    <?= $form->field($model, 'type')->textInput(['maxlength' => true, 'placeholder' => 'Type']) ?>

    <?= $form->field($model, 'class')->textInput(['maxlength' => true, 'placeholder' => 'Class']) ?>

    <?= $form->field($model, 'aircraft_types')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
	
	<?php
	
	
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/simulator', 'SimulatorToTraining')),
            'content' => $this->render('_formSimulatorToTraining', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->simulatorToTrainings),
                'school_id' => $model->school_id,
            ]),
        ],
    ];
	if ($model->id != NULL && $model->id > 0)
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
	
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/simulator', 'Create') : Yii::t('backend/simulator', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
	

</div>
