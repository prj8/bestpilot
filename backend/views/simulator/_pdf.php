<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Simulator */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\aircraft', 'Simulator'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simulator-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend\aircraft', 'Simulator').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'school_id',
        'type',
        'class',
        'aircraft_types:ntext',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
