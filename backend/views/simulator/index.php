<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\SimulatorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend/simulator', 'Simulator');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = $this->title;
$this->params['breadcrumbs'][] = Yii::t('backend/simulator', 'View simulator list');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="simulator-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend/simulator', 'Create Simulator'), ['create'], ['class' => 'btn btn-success']) ?>
        <!--<?= Html::a(Yii::t('backend/simulator', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?> -->
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
	
    <?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'type',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'20%',
			'pageSummary'=>true
		],
        [ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'school_id',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
				'options'=>[
					'data'=> \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name')
				]
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'20%',
			'pageSummary'=>true,
			'value' => function($model){
                    return $model->school->name;
                },
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid-aircraft-search-school_id']
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'class',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'10%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'aircraft_types',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'30%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'remarks',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'pageSummary'=>true
		],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
			'headerOptions' => [
				'class' => 'action-column-max',
			]
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
		'resizableColumns'=>true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-simulator']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{toggleData}',
        ],
    ]); ?>

</div>


<?php
/*
$this->registerJs("

    $('.kv-editable').click(function(e){
        $('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
        return true;
    });
");
*/
?>