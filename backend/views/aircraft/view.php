<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */

$register = $model->register_no!='' ? ' ['.$model->register_no.']' : '';
$this->title = $model->aircraftType->name.' '.$register;;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/aircraft', 'Aircraft'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = 'Aircraft details view';
?>
<div class="aircraft-view">

    <div class="row">
        <div class="col-sm-9">
           
        </div>
        <div class="col-sm-3 text-right" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend/aircraft', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend/aircraft', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend/aircraft', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
		<div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend/aircraft', 'School name'),
        ],
        [
            'attribute' => 'aircraftType.name',
            'label' => Yii::t('backend/aircraft', 'Aircraft type'),
        ],
        [
            'attribute'=>'image',
            'value'=>\Yii::$app->params['uploadWWW'].'/aircraft/'.$model->image,
            'format' => ['image',['width'=>'100']],
        ],
        'register_no',
        'aircraft_year',
        'engine_year',
        'glass_cockpit',
		[
			'attribute'=>'gps',
			'value'=>($model->gps==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')),
		],
		[
			'attribute'=>'radio',
			'value'=>($model->radio==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')),
		],
		[
			'attribute'=>'ils',
			'value'=>($model->ils==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')),
		],
		[
			'attribute'=>'night',
			'value'=>($model->night==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')),
		],
		[
			'attribute'=>'ifr',
			'value'=>($model->ifr==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')),
		],
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
		</div>
    </div>
    
    <div class="row">
		<div class="col-md-12">
			<div class="widget wred">
			<?php
		$layout = <<< HTML
		<!-- Widget head -->
		<div class="widget-head">
		  <div class="pull-left">{heading}</div>
		  <div class="widget-icons pull-right">
			<a class="wminimize" href="#"><i class="fa fa-chevron-up"></i></a> 
			<a class="wclose" href="#"><i class="fa fa-times"></i></a>
		  </div>
		  <div class="pull-right">{summary}</div>
		  <div class="clearfix"></div>
		</div>             
		<!-- Widget content -->
		<div class="widget-content">
		{items}
		{pager}
		<div class="widget-foot">
			{footer}
			<div class="clearfix"></div>
		</div>
		</div>
		<!-- Widget ends -->
		
HTML;
	?>
			
				
<?php
if($providerAircraftToTraining->totalCount){
    $gridColumnAircraftToTraining = [
        ['class' => 'yii\grid\SerialColumn', 'visible' => false],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'training.name',
                'label' => Yii::t('backend/aircraft', 'Training')
            ],
			[
				'class' => 'yii\grid\ActionColumn',
				'options'=>['class'=>'action-column-max'],
				'buttons' => [
					'view'=>function($url,$model,$key){
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['training/view', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'View')]);
					},
					'update'=>function($url,$model,$key){
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['training/update', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Update')]);
					},
					'delete'=>function($url,$model,$key){
						return ''; /*Html::a('<span class="glyphicon glyphicon-trash"></span>', ['training/delete', 'id'=>$model->id], ['title'=>Yii::t('backend/training', 'Delete'), 'data-method'=>'post', 'data-confirm'=>Yii::t('backend/training', 'Are you sure you want to delete this item?'), 'aria-label'=>Yii::t('backend/training', 'Delete')]);*/
					},
				],
			],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAircraftToTraining,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aircraft-to-training']],
        'layout' => $layout,
        'columns' => $gridColumnAircraftToTraining,
		'replaceTags' => [
			'{heading}' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('backend/aircraft', 'Aircraft To Training')),
			'{footer}' => '',
		],
    ]);
}
?>		
			</div>
		</div>
	</div>
</div>