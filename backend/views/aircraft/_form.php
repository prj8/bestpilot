<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */
/* @var $form yii\widgets\ActiveForm */

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END, 
    'viewParams' => [
        'class' => 'AircraftToTraining', 
        'relID' => 'aircraft-to-training', 
        'value' => \yii\helpers\Json::encode($model->aircraftToTrainings),
        'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
]);
?>

<div class="aircraft-form">
<div class="row">
		<div class="col-md-9">

    <?php $form = ActiveForm::begin([
		'options'=>['class' => 'form-horizontal','enctype'=>'multipart/form-data', ], // important
		'fieldConfig' => [
			'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
			'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
		],
									]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
	
	 <?php if ($model->school_id != 0): ?>
	<?= $form->field($model, 'school_id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
	<div class="form-group field-schoolname">
		<label for="schoolname" class=" col-md-2 control-label text-right"><?= Yii::t('backend/aircraft', 'School name') ?></label>
		<div class="col-sm-8">
			<?= Html::input('text', 'schoolname', $school_name, ['class'=>'form-control', 'readonly'=>'true']) ?>
		</div>
		<div class="help-block"></div>
	</div>
    <?php else: ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose School')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>
	
	<?php endif; ?>

	<?= $form->field($model, 'type_id')->widget(\kartik\widgets\Select2::classname(), [
		'data' => \yii\helpers\ArrayHelper::map(\backend\models\AircraftType::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
		'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose Aircraft type')],
		'pluginOptions' => [
			'allowClear' => true
		],
	]); ?>

	<?= $form->field($model, 'image')->widget(FileInput::classname(), [
		'options' => ['accept' => 'image/*', 'multiple' => false],
		'pluginOptions'=>[
			'showUpload' => false,
			'showRemove' => false,
			'allowedFileExtensions'=>['jpg','gif','png'],
			'initialPreview'=> $model->image != NULL ? ['<img style="max-width:200px;" src="'.\Yii::$app->params["uploadWWW"].'/aircraft/' .$model->image.'" />'] : [],
			'overwriteInitial'=>true,
			'initialPreviewConfig' => [['url'=>Url::to(['aircraft/delete-photo', 'id'=>$model->id]), 'extra'=>['id'=>$model->id]]]
		],
	]);
	?>

    <?= $form->field($model, 'register_no')->textInput(['maxlength' => true, 'placeholder' => 'Register No']) ?>

    <?= $form->field($model, 'aircraft_year')->textInput(['maxlength' => true, 'placeholder' => 'Aircraft Year']) ?>

    <?= $form->field($model, 'engine_year')->textInput(['maxlength' => true, 'placeholder' => 'Engine Year']) ?>

    <?= $form->field($model, 'glass_cockpit')->textInput(['maxlength' => true, 'placeholder' => 'Glass Cockpit']) ?>

    <?= $form->field($model, 'gps')->dropDownList(['0'=>Yii::t('backend/aircraft', 'No'), '1'=>Yii::t('backend/aircraft', 'Yes')]) ?>

    <?= $form->field($model, 'radio')->dropDownList(['0'=>Yii::t('backend/aircraft', 'No'), '1'=>Yii::t('backend/aircraft', 'Yes')]) ?>

    <?= $form->field($model, 'ils')->dropDownList(['0'=>Yii::t('backend/aircraft', 'No'), '1'=>Yii::t('backend/aircraft', 'Yes')]) ?>

    <?= $form->field($model, 'night')->dropDownList(['0'=>Yii::t('backend/aircraft', 'No'), '1'=>Yii::t('backend/aircraft', 'Yes')]) ?>

    <?= $form->field($model, 'ifr')->dropDownList(['0'=>Yii::t('backend/aircraft', 'No'), '1'=>Yii::t('backend/aircraft', 'Yes')]) ?>

    <?= $form->field($model, 'remarks')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
	
	
	
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">

    <?php
	
	
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode(Yii::t('backend/aircraft', 'AircraftToTraining')),
            'content' => $this->render('_formAircraftToTraining', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->aircraftToTrainings),
                'school_id' => $model->school_id,
            ]),
        ],
    ];
	if ($model->id != NULL && $model->id > 0 && $model->school_id != 0)
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
	
    ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend/aircraft', 'Create') : Yii::t('backend/aircraft', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
