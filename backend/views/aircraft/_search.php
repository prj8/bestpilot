<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\AircraftSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-aircraft-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'school_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('backend\aircraft', 'Choose School')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>


    <?= $form->field($model, 'register_no')->textInput(['maxlength' => true, 'placeholder' => 'Register No']) ?>

    <?= $form->field($model, 'multi_engine')->textInput(['placeholder' => 'Multi Engine']) ?>

    <?php /* echo $form->field($model, 'aircraft_year')->textInput(['maxlength' => true, 'placeholder' => 'Aircraft Year']) */ ?>

    <?php /* echo $form->field($model, 'engine_year')->textInput(['maxlength' => true, 'placeholder' => 'Engine Year']) */ ?>

    <?php /* echo $form->field($model, 'glass_cockpit')->textInput(['maxlength' => true, 'placeholder' => 'Glass Cockpit']) */ ?>

    <?php /* echo $form->field($model, 'gps')->textInput(['placeholder' => 'Gps']) */ ?>

    <?php /* echo $form->field($model, 'radio')->textInput(['placeholder' => 'Radio']) */ ?>

    <?php /* echo $form->field($model, 'ils')->textInput(['placeholder' => 'Ils']) */ ?>

    <?php /* echo $form->field($model, 'night')->textInput(['placeholder' => 'Night']) */ ?>

    <?php /* echo $form->field($model, 'ifr')->textInput(['placeholder' => 'Ifr']) */ ?>

    <?php /* echo $form->field($model, 'remarks')->textarea(['rows' => 6]) */ ?>

    <?php /* echo $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend\aircraft', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend\aircraft', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
