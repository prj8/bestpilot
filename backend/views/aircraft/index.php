<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AircraftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = Yii::t('backend/aircraft', 'Aircraft');
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/aircraft', 'View aircraft');
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="col-lg-12 col-md-12 aircraft-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend/aircraft', 'Create Aircraft'), ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('backend/aircraft', 'Advance Search'), '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php 
    $gridColumn = [
		['attribute' => 'id', 'visible' => false],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'school_id',
			'label' => Yii::t('backend/aircraft', 'School'),
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
				'options'=>[
					'data'=> \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name')
				]
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'pageSummary'=>true,
			'value' => function($model){
                    return $model->school->name;
                },
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->active()->asArray()->all(), 'id', 'name'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'School', 'id' => 'grid-aircraft-search-school_id']
		],
		[
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'type_id',
			'label' => Yii::t('backend/aircraft', 'Type'),
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_SELECT2,
				'options'=>[
					'data'=> \yii\helpers\ArrayHelper::map(\backend\models\AircraftType::find()->asArray()->all(), 'id', 'name')
				]
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'pageSummary'=>true,
			'value' => function($model){
                    return $model->aircraftType->name;
                },
			'filterType' => GridView::FILTER_SELECT2,
			'filter' => \yii\helpers\ArrayHelper::map(\backend\models\AircraftType::find()->asArray()->all(), 'id', 'name'),
			'filterWidgetOptions' => [
				'pluginOptions' => ['allowClear' => true],
			],
			'filterInputOptions' => ['placeholder' => 'Type', 'id' => 'grid-aircraft-search-type_id']
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'register_no',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true
		],
        [
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'aircraft_year',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'engine_year',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'glass_cockpit',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'7%',
			'pageSummary'=>true,
			'filterType' => GridView::FILTER_CHECKBOX,
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'gps',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
			],
			'value'=>function($data){ return ($data->gps==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true,
			'filterType' => GridView::FILTER_CHECKBOX,
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'radio',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
			],
			'value'=>function($data){ return ($data->radio==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true,
			'filterType' => GridView::FILTER_CHECKBOX,
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'ils',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
			],
			'value'=>function($data){ return ($data->ils==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true,
			'filterType' => GridView::FILTER_CHECKBOX,
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'night',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
			],
			'value'=>function($data){ return ($data->night==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true,
			'filterType' => GridView::FILTER_CHECKBOX,
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'ifr',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_CHECKBOX,
			],
			'value'=>function($data){ return ($data->ifr==1 ? Yii::t('backend/aircraft', 'Yes'):Yii::t('backend/aircraft', 'No')); },
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'5%',
			'pageSummary'=>true,
			'filter' => Html::activeDropDownList(
                            $searchModel, 'ifr', ['0'=>'No', '1'=>'Yes'],
                            ['class' => 'form-control', 'prompt' => '']
                        )
		],
		[ 
			'class'=>'kartik\grid\EditableColumn',
			'attribute'=>'remarks',
			'editableOptions'=>[
				'inputType'=>\kartik\editable\Editable::INPUT_TEXT
			],
			'hAlign'=>'right',
			'vAlign'=>'middle',
			'width'=>'10%',
			'pageSummary'=>true
		],
        ['attribute' => 'lock', 'visible' => false],
        [
            'class' => 'yii\grid\ActionColumn',
			'headerOptions' => [
				'class' => 'action-column-min'
			]
        ],
    ]; 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-aircraft']],
		'resizableColumns'=>true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        'export' => false,
        // your toolbar can include the additional full export menu
        
        'toolbar' => [
            '{toggleData}',
        ],
    ]); ?>

</div>

<?php
/*
$this->registerJs("

    $('.kv-editable').click(function(e){
        $('.kv-editable-popover.in').not('#'+$(this).attr('id')+' .kv-editable-popover.in').removeClass('in').hide();
        return true;
    });
");
*/
?>