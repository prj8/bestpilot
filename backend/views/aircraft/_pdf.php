<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\aircraft', 'Aircraft'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="aircraft-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('backend\aircraft', 'Aircraft').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'school.name',
                'label' => Yii::t('backend\aircraft', 'School')
            ],
        'type',
        'register_no',
        'multi_engine',
        'aircraft_year',
        'engine_year',
        'glass_cockpit',
        'gps',
        'radio',
        'ils',
        'night',
        'ifr',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerAircraftToTraining->totalCount){
    $gridColumnAircraftToTraining = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'training.name',
                'label' => Yii::t('backend\aircraft', 'Training')
            ],
    ];
    echo Gridview::widget([
        'dataProvider' => $providerAircraftToTraining,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('backend\aircraft', 'Aircraft To Training')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnAircraftToTraining
    ]);
}
?>
    </div>
</div>
