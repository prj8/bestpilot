<div class="form-group" id="add-aircraft-to-training">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);

echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'AircraftToTraining',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN],
        'training_id' => [
            'label' => 'Training',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(
	\yii\helpers\ArrayHelper::toArray(
					\backend\models\Training::find()->where(['school_id'=>$school_id])->orderBy('name')->all(), 
					[
						'backend\models\Training'=> [
							'id',
							'name'=>function($training){return $training->name.' ['.$training->price.' PLN]';}
							
						]
					]), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('backend/aircraft', 'Choose Training')]
			],
            'columnOptions' => ['width' => '200px']
        ],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend/aircraft', 'Delete'), 'onClick' => 'delRowAircraftToTraining(' . $key . '); return false;', 'id' => 'aircraft-to-training-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend/aircraft', 'Add Aircraft To Training'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAircraftToTraining()']).Html::hiddenInput('school_id', $school_id),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

