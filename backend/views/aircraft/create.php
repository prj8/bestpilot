<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */

$this->title = Yii::t('backend/aircraft', 'Create Aircraft');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/aircraft', 'Aircraft'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/aircraft', 'Edit Aircraft');
?>
<div class="aircraft-create">

    <div class="row">

        <?= $this->render('_form', [
            'model' => $model,
            'school_name' => $school_name,
        ]) ?>

    </div>




</div>
