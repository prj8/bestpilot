<?php

//use yii\helpers\Html;
use webvimark\modules\UserManagement\components\GhostHtml as Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */

$register = $model->register_no!='' ? ' ['.$model->register_no.']' : '';
$this->title = Yii::t('backend/aircraft', 'Update {modelClass}: ', [
    'modelClass' => 'Aircraft',
]) . ' ' . $model->aircraftType->name.' '.$register;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend/aircraft', 'Aircraft'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->aircraftType->name.' '.$register, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend/aircraft', 'Update');

$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend/aircraft', 'Edit Aircraft');
?>
<div class="aircraft-update">


    <?= $this->render('_form', [
        'model' => $model,
        'school_name' => $school_name,
    ]) ?>

</div>
