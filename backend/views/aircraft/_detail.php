<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Aircraft */

?>
<div class="aircraft-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'school.name',
            'label' => Yii::t('backend/aircraft', 'School'),
        ],
        'type',
        'register_no',
        'multi_engine',
        'aircraft_year',
        'engine_year',
        'glass_cockpit',
        'gps',
        'radio',
        'ils',
        'night',
        'ifr',
        'remarks:ntext',
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>