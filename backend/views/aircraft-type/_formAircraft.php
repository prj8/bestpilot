<div class="form-group" id="add-aircraft">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Aircraft',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'school_id' => [
            'label' => 'School',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\School::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('backend\aircraft', 'Choose School')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'type' => ['type' => TabularForm::INPUT_TEXT],
        'image' => ['type' => TabularForm::INPUT_TEXT],
        'register_no' => ['type' => TabularForm::INPUT_TEXT],
        'multi_engine' => ['type' => TabularForm::INPUT_TEXT],
        'aircraft_year' => ['type' => TabularForm::INPUT_TEXT],
        'engine_year' => ['type' => TabularForm::INPUT_TEXT],
        'glass_cockpit' => ['type' => TabularForm::INPUT_TEXT],
        'gps' => ['type' => TabularForm::INPUT_TEXT],
        'radio' => ['type' => TabularForm::INPUT_TEXT],
        'ils' => ['type' => TabularForm::INPUT_TEXT],
        'night' => ['type' => TabularForm::INPUT_TEXT],
        'ifr' => ['type' => TabularForm::INPUT_TEXT],
        'distance' => ['type' => TabularForm::INPUT_TEXT],
        'max_speed' => ['type' => TabularForm::INPUT_TEXT],
        'remarks' => ['type' => TabularForm::INPUT_TEXTAREA],
        'active' => ['type' => TabularForm::INPUT_TEXT],
        "lock" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('backend\aircraft', 'Delete'), 'onClick' => 'delRowAircraft(' . $key . '); return false;', 'id' => 'aircraft-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('backend\aircraft', 'Add Aircraft'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowAircraft()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

