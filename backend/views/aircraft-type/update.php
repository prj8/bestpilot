<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\AircraftType */

$this->title = Yii::t('backend\aircraft', 'Update {modelClass}: ', [
    'modelClass' => 'Aircraft Type',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\aircraft', 'Aircraft Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend\aircraft', 'Update');

$this->params['page_header'] = Html::encode($this->title);
$this->params['page_subheader'] = Yii::t('backend\training', 'Update Aircraft Type');
?>
<div class="aircraft-type-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
