<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\AircraftType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\aircraft', 'Aircraft Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = Yii::t('backend\aircraft', 'Aircraft Type').' '. Html::encode($this->title);
$this->params['page_subheader'] = Yii::t('backend\training', 'View Aircraft Type');

?>
<div class="aircraft-type-view">

    <div class="row">
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a(Yii::t('backend\aircraft', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('backend\aircraft', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('backend\aircraft', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'range',
        'speed',
        [
            'attribute'=>'photo',
            'value'=>\Yii::$app->params['uploadWWW'].'/aircraft-types/'.$model->photo,
            'format' => ['image',['width'=>'100']],
        ],
        [
            'attribute'=>'active',
            'value'=>($model->active==1 ? Yii::t('backend\aircraft', 'Yes'):Yii::t('backend\aircraft', 'No')),
        ],
        ['attribute' => 'lock', 'visible' => false],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
        </div>
    </div>


</div>
