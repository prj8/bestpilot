<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\AircraftType */

$this->title = Yii::t('backend\aircraft', 'Create Aircraft Type');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend\aircraft', 'Aircraft Type'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['page_header'] = $this->title;
$this->params['page_subheader'] = Yii::t('backend\school', 'Add new aircraft type');
?>
<div class="aircraft-type-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
