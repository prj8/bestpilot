<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model backend\models\AircraftType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="aircraft-type-form">
    <div class="row">
        <div class="col-md-9">

    <?php $form = ActiveForm::begin([
        'options'=>['enctype'=>'multipart/form-data', 'class' => 'form-horizontal'], // important
        'fieldConfig' => [
            'template' => "{label}\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>",
            'labelOptions' => ['class' => 'col-md-2 control-label text-right'],
            /*'offsetOptions' => ['class' => 'col-sm-offset-4'],
            'wrapperOptions' => ['class' => 'col-sm-8'],*/
            'errorOptions' => ['class'=>'error'],
            'hintOptions' => [],
        ],
    ]); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'multiengine', ['template' => "<div class='col-sm-2'>{label}</div>\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>"])->checkbox(['label' => 'Multiengine', ]) ?>

    <?= $form->field($model, 'range')->textInput(['maxlength' => true, 'placeholder' => 'Range']) ?>

    <?= $form->field($model, 'speed')->textInput(['maxlength' => true, 'placeholder' => 'Speed']) ?>

    <?= $form->field($model, 'photo')->widget(FileInput::classname(), [
        'options' => ['accept' => 'image/*', 'multiple' => false],
        'pluginOptions'=>[
            'showUpload' => false,
            'showRemove' => false,
            'allowedFileExtensions'=>['jpg','gif','png'],
            'initialPreview'=> $model->photo != NULL ? ['<img style="max-width:200px;" src="'.\Yii::$app->params["uploadWWW"].'/aircraft-types/' .$model->photo.'" />'] : [],
            'overwriteInitial'=>true,
            'initialPreviewConfig' => [['url'=>Url::to(['aircraft-type/delete-photo', 'id'=>$model->id]), 'extra'=>['id'=>$model->id]]]
        ],
    ]);
    ?>

    <?= $form->field($model, 'active', ['template' => "<div class='col-sm-2'>{label}</div>\n<div class='col-sm-8'>\n{input}\n{hint}\n{error}\n</div>"])->checkbox(['label' => 'Active', ]) ?>

    <?= $form->field($model, 'lock', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-2">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('backend\aircraft', 'Create') : Yii::t('backend\aircraft', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
