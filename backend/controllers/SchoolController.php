<?php

namespace backend\controllers;

use backend\models\User;
use backend\models\UserToSchool;
use Yii;
use backend\models\School;
use backend\models\SchoolSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\Json;
use yii2mod\rbac\filters\AccessControl;

/**
 * SchoolController implements the CRUD actions for School model.
 */
class SchoolController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'allowActions' => [
                    'activate-admin',
                    // The actions listed here will be allowed to everyone including guests.
                ],

            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['update', 'view', 'delete', 'users'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'delete', 'users'],
                        'matchCallback' => function($rule, $action) {
                            if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                            {
                                return UserToSchool::validateUserToSchool(Yii::$app->request->get('id'), Yii::$app->user->id);
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all School models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SchoolSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if (Yii::$app->request->post('hasEditable')) {
			// instantiate your book model for saving
			$ID = Yii::$app->request->post('editableKey');
			$model = School::findOne($ID);

			// store a default json response as desired by editable
			$out = Json::encode(['output'=>'', 'message'=>'']);

			// fetch the first entry in posted data (there should only be one entry 
			// anyway in this array for an editable submission)
			// - $posted is the posted data for Book without any indexes
			// - $post is the converted array for single model validation
			$posted = current($_POST['School']);
			$post = ['School' => $posted];

			// load model like any single model validation
			if ($model->load($post)) {
			// can save model or do something before saving model
			$model->save();

			// custom output to return to be displayed as the editable grid cell
			// data. Normally this is empty - whereby whatever value is edited by
			// in the input by user is updated automatically.
			$output = '';

			// specific use case where you need to validate a specific
			// editable column posted when you have more than one
			// EditableColumn in the grid view. We evaluate here a
			// check to see if buy_amount was posted for the Book model
			/*
			if (isset($posted['buy_amount'])) {
			$output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
			}
			*/

			// similarly you can check if the name attribute was posted as well
			// if (isset($posted['name'])) {
			// $output = ''; // process as you need
			// }
			$out = Json::encode(['output'=>$output, 'message'=>'']);
			}
			// return ajax json encoded response and exit
			echo $out;
			return;
		}
		
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single School model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerAircraft = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircrafts,
        ]);
        $providerTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->trainings,
        ]);
        $providerSimulator = new \yii\data\ArrayDataProvider([
            'allModels' => $model->simulators,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAircraft' => $providerAircraft,
            'providerTraining' => $providerTraining,
            'providerSimulator' => $providerSimulator,
        ]);
    }

    /**
     * Creates a new School model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new School();
		
        if ($model->loadAll(Yii::$app->request->post())) {
			$image = UploadedFile::getInstance($model, 'logo');

            if ($image) {
                // store the source file name
                $model->logo = $image->name;
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $model->logo = Yii::$app->security->generateRandomString().".{$ext}";

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)

                // zmienić katalog na /frontend/web/upload/logos/
                $path = Yii::getAlias('@upload') . '/logos/' . $model->logo;
            }

			
			if ($model->save())
			{
				if ($image) $image->saveAs($path);

                if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                    $model->addUser(Yii::$app->user->id);
				
				return $this->redirect(['view', 'id' => $model->id]);
			}
        } else {
			return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing School model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $providerAircraft = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircrafts,
        ]);
        $providerTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->trainings,
        ]);
        $providerSimulator = new \yii\data\ArrayDataProvider([
            'allModels' => $model->simulators,
        ]);
        $logo = $model->logo;
        if ($model->loadAll(Yii::$app->request->post())) {
			$image = UploadedFile::getInstance($model, 'logo');

            if ($image)
            {
                // store the source file name
                $model->logo = $image->name;
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $model->logo = Yii::$app->security->generateRandomString().".{$ext}";

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                $path = Yii::$app->params['uploadPath'] . '/logos/' . $model->logo;
            } else {
                $model->logo = $logo;
            }
			if ($model->saveAll(['userToSchools', 'aircrafts', 'simulators', 'trainings', 'country']))
			{
                if ($image) $image->saveAs($path);
				return $this->redirect(['view', 'id' => $model->id]);
			} else {
                return $this->render('update', [
                    'model' => $model,
                    'providerAircraft' => $providerAircraft,
                    'providerTraining' => $providerTraining,
                    'providerSimulator' => $providerSimulator
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'providerAircraft' => $providerAircraft,
                'providerTraining' => $providerTraining,
                'providerSimulator' => $providerSimulator
            ]);
        }
    }

    /**
     * Updates an existing School model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUsers($id)
    {

        $model = $this->findModel($id);

        $user = null;

        if (($email = Yii::$app->request->post('email', null)) != null) {
            $user = \backend\models\User::find()->where(['=', 'email', $email])->one();
            if (!$user)
            {
                // tworzymy nowego usera
                $user = new \backend\models\User();
                $user->username = $email;
                $user->email = $email;
                $user->password = '%&@)%$&jdljkdklsjakdls';
                $user->generateConfirmationToken();
                $user->generateAuthKey();
                $user->status = 1;
                if (!$user->save())
                    throw new \yii\base\UserException(Yii::t('backend\school', 'Wystąpił nieoczekiwany błąd. Spróbuj później.'));
            }

            //dodajemy uprawnienia 'schoolAdmin'
            \webvimark\modules\UserManagement\models\User::assignRole($user->id, 'school-admin');

            //dodanie usera do szkoły
            $model->addUser($user->id);

            //wysłanie zaproszenia na email
            $user->sendAdminInvitation();

            \Yii::$app->getSession()->setFlash('success', \Yii::t('school/users', 'Invitation was sended.'));

        }
        $userModel = new UserToSchool();
        $providerUsers = new \yii\data\ArrayDataProvider([
            'allModels' => $model->userToSchools,
        ]);


        return $this->render('users', [
            'model' => $model,
            'userModel' => $userModel,
            'providerUsers' => $providerUsers,
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing School model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUserDelete($id)
    {
        $userModel = UserToSchool::find()->where(['=', 'id', $id])->one();
        $school_id = $userModel->school_id;
        $userModel->delete();

        return $this->redirect(['users', 'id' => $school_id]);
    }

    public function actionActivateAdmin($token)
    {
        $this->layout = 'loginLayout.php';
        if ($user = \backend\models\User::find()->where(['=', 'confirmation_token', $token])->one())
        {
            $model = new \backend\models\UserRegistration();
            if ($model->load(Yii::$app->request->post()))
            {
                $user->username = $user->email;
                $user->setPassword($model->password);
                $user->generateAuthKey();
                $user->removeConfirmationToken();
                $user->email_confirmed = 1;
                $user->save();
                \Yii::$app->getSession()->setFlash('success', \Yii::t('school/admin-registration', 'User activated. You can login now.'));
                return \Yii::$app->getResponse()->redirect(['user-management/auth/login'])->send();
            } else {
                $model->username = $user->email;
                return $this->render('activate-admin', [
                    'model' => $model,
                ]);
            }
        } else {
            return Yii::$app->getResponse()->redirect(['user-management/auth/login'])->send();
        }


    }

    /**
     * Delete photo of aircraft with $id
     *
     * @param integer $id
     * @return bool
     */
    public function actionDeleteLogo($id)
    {
        $model = $this->findModel($id);
        if ($model->logo != NULL && $model->logo != ''){
            $path = Yii::getAlias('@upload') . '/logos/' . $model->logo;
            @unlink($path);
            $model->logo = NULL;
            $model->save();
        }
        \Yii::$app->response->format = 'json';
        return true;
    }
	

    /**
     * Deletes an existing School model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the School model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return School the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = School::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend\school', 'The requested page does not exist.'));
        }
    }


    /**
    * Action to load a tabular form grid
    * for Aircraft
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddUsers()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Users');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formUsers', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\school', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for Aircraft
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAircraft()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Aircraft');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAircraft', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\school', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Training
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddTraining()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Training');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formTraining', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\school', 'The requested page does not exist.'));
        }
    }


    /**
     * Action to load a tabular form grid
     * for AircraftToTraining
     * @author Yohanes Candrajaya <moo.tensai@gmail.com>
     * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
     *
     * @return mixed
     */
    public function actionAddAddresses()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Addresses');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAddress', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\school', 'The requested page does not exist.'));
        }
    }
}
