<?php

namespace backend\controllers;

use Yii;
use backend\models\Aircraft;
use backend\models\AircraftSearch;
use backend\models\UserToSchool;
use backend\models\School;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii2mod\rbac\filters\AccessControl;

/**
 * AircraftController implements the CRUD actions for Aircraft model.
 */
class AircraftController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'allowActions' => [
                    // The actions listed here will be allowed to everyone including guests.
                ]
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'delete'],
                        'matchCallback' => function($rule, $action) {
                            if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                            {
                                return UserToSchool::validateUserToAircraft(Yii::$app->request->get('id'), Yii::$app->user->id);
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Aircraft models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AircraftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if (Yii::$app->request->post('hasEditable')) {
			// instantiate your book model for saving
			$Id = Yii::$app->request->post('editableKey');
			$model = Aircraft::findOne($Id);

			// store a default json response as desired by editable
			$out = Json::encode(['output'=>'', 'message'=>'']);

			// fetch the first entry in posted data (there should only be one entry 
			// anyway in this array for an editable submission)
			// - $posted is the posted data for Book without any indexes
			// - $post is the converted array for single model validation
			$posted = current($_POST['Aircraft']);
			$post = ['Aircraft' => $posted];

			// load model like any single model validation
			if ($model->load($post)) {
			// can save model or do something before saving model
			$model->save();

			// custom output to return to be displayed as the editable grid cell
			// data. Normally this is empty - whereby whatever value is edited by
			// in the input by user is updated automatically.
			$output = '';

			// specific use case where you need to validate a specific
			// editable column posted when you have more than one
			// EditableColumn in the grid view. We evaluate here a
			// check to see if buy_amount was posted for the Book model
			/*
			if (isset($posted['buy_amount'])) {
			$output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
			}
			*/

			// similarly you can check if the name attribute was posted as well
			// if (isset($posted['name'])) {
			// $output = ''; // process as you need
			// }
			$out = Json::encode(['output'=>$output, 'message'=>'']);
			}
			// return ajax json encoded response and exit
			echo $out;
			return;
		}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Aircraft model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerAircraftToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircraftToTrainings,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAircraftToTraining' => $providerAircraftToTraining,
        ]);
    }

    /**
     * Creates a new Aircraft model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
		$model = new Aircraft();
        if (isset($_GET['school_id']))
		{
			$school = School::findOne($_GET['school_id']);
			$model->school_id = $school->id;
			$school_name = $school->name;
		} else {
			$school_name = '';
		}
		
		
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'school_name' => $school_name,
            ]);
        }
    }

    /**
     * Updates an existing Aircraft model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $photo = $model->image;
		if ($school = School::findOne($model->school_id)){
			$school_name = $school->name;
		} else {
			$school_name = '';
		}
		
        if ($model->loadAll(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');
            if ($image) {
                if ($image != NULL && $image != ''){
                    $path = Yii::getAlias('@upload') . '/aircraft/' . $image;
                    @unlink($path);
                }
                // store the source file name
                $model->image = $image->name;
                /** @noinspection PhpPassByRefInspection */
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $model->image = Yii::$app->security->generateRandomString().".{$ext}";

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)

                // zmienić katalog na /frontend/web/upload/logos/
                $path = Yii::getAlias('@upload') . '/aircraft/' . $model->image;
            } else {
                $model->image = $photo;
            }
            if ($model->saveAll(['school', 'aircraftType', 'aircraftToTraining'])){
                if ($image) $image->saveAs($path);
            }
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'school_name' => $school_name,
            ]);
        }
    }

    /**
     * Delete photo of aircraft with $id
     *
     * @param integer $id
     * @return bool
     */
    public function actionDeletePhoto($id)
    {
        $model = $this->findModel($id);
        if ($model->image != NULL && $model->image != ''){
            $path = Yii::getAlias('@upload') . '/aircraft/' . $model->image;
            @unlink($path);
            $model->image = NULL;
            $model->save();
        }
        \Yii::$app->response->format = 'json';
        return true;
    }

    /**
     * Deletes an existing Aircraft model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Aircraft model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Aircraft the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Aircraft::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend\aircraft', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for AircraftToTraining
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAircraftToTraining()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('AircraftToTraining');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add') {
                $row[] = [];
				$school_id = isset($_POST['school_id']) ? $_POST['school_id'] : 0;
			}
            return $this->renderAjax('_formAircraftToTraining', ['row' => $row, 'school_id' => $school_id]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\aircraft', 'The requested page does not exist.'));
        }
    }
}
