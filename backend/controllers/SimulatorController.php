<?php

namespace backend\controllers;

use Yii;
use backend\models\Simulator;
use backend\models\UserToSchool;
use backend\models\SimulatorSearch;
use backend\models\School;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii2mod\rbac\filters\AccessControl;

/**
 * SimulatorController implements the CRUD actions for Simulator model.
 */
class SimulatorController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],

            'access' => [
                'class' => AccessControl::className(),
                'allowActions' => [
                    // The actions listed here will be allowed to everyone including guests.
                ]
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['update', 'view', 'delete'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'delete'],
                        'matchCallback' => function($rule, $action) {
                            if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                            {
                                return UserToSchool::validateUserToSimulator(Yii::$app->request->get('id'), Yii::$app->user->id);
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Simulator models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SimulatorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if (Yii::$app->request->post('hasEditable')) {
			// instantiate your book model for saving
			$Id = Yii::$app->request->post('editableKey');
			$model = Simulator::findOne($Id);

			// store a default json response as desired by editable
			$out = Json::encode(['output'=>'', 'message'=>'']);

			// fetch the first entry in posted data (there should only be one entry 
			// anyway in this array for an editable submission)
			// - $posted is the posted data for Book without any indexes
			// - $post is the converted array for single model validation
			$posted = current($_POST['Simulator']);
			$post = ['Simulator' => $posted];

			// load model like any single model validation
			if ($model->load($post)) {
			// can save model or do something before saving model
			$model->save();

			// custom output to return to be displayed as the editable grid cell
			// data. Normally this is empty - whereby whatever value is edited by
			// in the input by user is updated automatically.
			$output = '';

			// specific use case where you need to validate a specific
			// editable column posted when you have more than one
			// EditableColumn in the grid view. We evaluate here a
			// check to see if buy_amount was posted for the Book model
			/*
			if (isset($posted['buy_amount'])) {
			$output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
			}
			*/

			// similarly you can check if the name attribute was posted as well
			// if (isset($posted['name'])) {
			// $output = ''; // process as you need
			// }
			$out = Json::encode(['output'=>$output, 'message'=>'']);
			}
			// return ajax json encoded response and exit
			echo $out;
			return;
		}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Simulator model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
		$providerSimulatorToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->simulatorToTrainings,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerSimulatorToTraining' => $providerSimulatorToTraining,
        ]);
    }

    /**
     * Creates a new Simulator model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Simulator();
		if (isset($_GET['school_id']))
		{
			$school = School::findOne($_GET['school_id']);
			$model->school_id = $school->id;
			$school_name = $school->name;
		} else {
			$school_name = '';
		}

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'school_name' => $school_name,
            ]);
        }
    }

    /**
     * Updates an existing Simulator model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if ($school = School::findOne($model->school_id)){
			$school_name = $school->name;
		} else {
			$school_name = '';
		}
		
		

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll(['school'])) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'school_name' => $school_name,
            ]);
        }
    }

    /**
     * Deletes an existing Simulator model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the Simulator model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Simulator the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Simulator::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend\simulator', 'The requested page does not exist.'));
        }
    }
	
	
    /**
    * Action to load a tabular form grid
    * for AircraftToTraining
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddSimulatorToTraining()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('SimulatorToTraining');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add') {
                $row[] = [];
				$school_id = isset($_POST['school_id']) ? $_POST['school_id'] : 0;
			}
            return $this->renderAjax('_formSimulatorToTraining', ['row' => $row, 'school_id' => $school_id]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\simulator', 'The requested page does not exist.'));
        }
    }
}
