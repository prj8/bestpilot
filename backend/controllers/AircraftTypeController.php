<?php

namespace backend\controllers;

use Yii;
use backend\models\AircraftType;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii2mod\rbac\filters\AccessControl;

/**
 * AircraftTypeController implements the CRUD actions for AircraftType model.
 */
class AircraftTypeController extends Controller
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'allowActions' => [
                    // The actions listed here will be allowed to everyone including guests.
                ]
            ]
        ];
    }

    /**
     * Lists all AircraftType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AircraftType::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AircraftType model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $providerAircraft = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircrafts,
        ]);
        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAircraft' => $providerAircraft,
        ]);
    }

    /**
     * Creates a new AircraftType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AircraftType();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            $image = UploadedFile::getInstance($model, 'photo');
            if ($image) {
                // store the source file name
                $model->photo = $image->name;
                /** @noinspection PhpPassByRefInspection */
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $model->photo = Yii::$app->security->generateRandomString().".{$ext}";

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)

                // zmienić katalog na /frontend/web/upload/logos/
                $path = Yii::getAlias('@upload') . '/aircraft-types/' . $model->photo;
            }


            if ($model->saveAll())
            {
                if ($image) $image->saveAs($path);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AircraftType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $photo = $model->photo;
        if ($model->loadAll(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'photo');
            if ($image) {
                // store the source file name
                $model->photo = $image->name;
                /** @noinspection PhpPassByRefInspection */
                $ext = end((explode(".", $image->name)));

                // generate a unique file name
                $model->photo = Yii::$app->security->generateRandomString().".{$ext}";

                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)

                // zmienić katalog na /frontend/web/upload/logos/
                $path = Yii::getAlias('@upload') . '/aircraft-types/' . $model->photo;
            } else {
                $model->photo = $photo;
            }
            if ($model->save())
            {
                if ($image) $image->saveAs($path);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Delete photo of aircraft with $id
     *
     * @param integer $id
     * @return bool
     */
    public function actionDeletePhoto($id)
    {
        $model = $this->findModel($id);
        if ($model->photo != NULL && $model->photo != ''){
            $path = Yii::getAlias('@upload') . '/aircraft-types/' . $model->photo;
            @unlink($path);
            $model->photo = NULL;
            $model->save();
        }
        \Yii::$app->response->format = 'json';
        return true;
    }
    /**
     * Deletes an existing AircraftType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    
    /**
     * Finds the AircraftType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AircraftType the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AircraftType::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend\aircraft', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for Aircraft
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAircraft()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Aircraft');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formAircraft', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\aircraft', 'The requested page does not exist.'));
        }
    }
}
