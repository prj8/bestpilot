<?php

namespace backend\controllers;

use backend\models\Aircraft;
use backend\models\Simulator;
use backend\models\UserToSchool;
use backend\models\base\TrainingStartDates;
use Yii;
use backend\models\Training;
use backend\models\TrainingSearch;
use backend\models\School;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json,
    yii\data\ArrayDataProvider,
    kartik\mpdf\Pdf;
use yii2mod\rbac\filters\AccessControl;

/**
 * TrainingController implements the CRUD actions for Training model.
 */
class TrainingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'allowActions' => [
                    // The actions listed here will be allowed to everyone including guests.
                ]
            ],
            'access2' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['update', 'view', 'delete', 'duplicate'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'delete', 'duplicate'],
                        'matchCallback' => function($rule, $action) {
                            if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                            {
                                return UserToSchool::validateUserToTraining(Yii::$app->request->get('id'), Yii::$app->user->id);
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ],
            'access3' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['create'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'matchCallback' => function($rule, $action) {
                            if (\webvimark\modules\UserManagement\models\User::hasRole('school-admin', false))
                            {
                                return UserToSchool::validateUserToSchool(Yii::$app->request->get('school_id'), Yii::$app->user->id);
                            } else {
                                return true;
                            }
                        }
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Training models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		if (Yii::$app->request->post('hasEditable')) {
			// instantiate your book model for saving
			$ID = Yii::$app->request->post('editableKey');
			$model = Training::findOne($ID);

			// store a default json response as desired by editable
			$out = Json::encode(['output'=>'', 'message'=>'']);

			// fetch the first entry in posted data (there should only be one entry 
			// anyway in this array for an editable submission)
			// - $posted is the posted data for Book without any indexes
			// - $post is the converted array for single model validation
			$posted = current($_POST['Training']);
			$post = ['Training' => $posted];

			// load model like any single model validation
			if ($model->load($post)) {
			// can save model or do something before saving model
			$model->save();

			// custom output to return to be displayed as the editable grid cell
			// data. Normally this is empty - whereby whatever value is edited by
			// in the input by user is updated automatically.
			$output = '';

			// specific use case where you need to validate a specific
			// editable column posted when you have more than one
			// EditableColumn in the grid view. We evaluate here a
			// check to see if buy_amount was posted for the Book model
			/*
			if (isset($posted['buy_amount'])) {
			$output = Yii::$app->formatter->asDecimal($model->buy_amount, 2);
			}
			*/

			// similarly you can check if the name attribute was posted as well
			// if (isset($posted['name'])) {
			// $output = ''; // process as you need
			// }
			$out = Json::encode(['output'=>$output, 'message'=>'']);
			}
			// return ajax json encoded response and exit
			//echo $out;
			return $out;
		}

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Duplicate training record
     * @param integer $id
     * @return mixed
     */
    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $duplicated = new Training();
        $duplicated->attributes = $model->attributes;
        $duplicated->save();

        return $this->redirect(['update', 'id' => $duplicated->id]);
    }

    /**
     * Displays a single Training model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $providerAircraftToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircraft,
        ]);
        $providerSimulatorToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->simulator,
        ]);

        $providerStartDate= new \yii\data\ArrayDataProvider([
            'allModels' => $model->startDates
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'providerAircraftToTraining' => $providerAircraftToTraining,
            'providerSimulatorToTraining' => $providerSimulatorToTraining,
            'providerStartDate' => $providerStartDate,
        ]);
    }

    /**
     * Creates a new Training model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Training();
		if (isset($_GET['school_id']))
		{
			$school = School::findOne($_GET['school_id']);
			$model->school_id = $school->id;
			$school_name = $school->name;
		} else {
			$school_name = '';
		}
        if ($model->loadAll(Yii::$app->request->post()) && $model->save()) {
            foreach (Yii::$app->request->post('TrainingTranslation', []) as $language => $data) {
                foreach ($data as $attribute => $translation) {
                    $model->translate($language)->$attribute = $translation;
                }
                $model->translate($language)->training_id = $model->id;
                $model->translate($language)->save();
            }
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
			if (isset($_GET['school_id'])) $model->school_id = $_GET['school_id'];
            return $this->render('create', [
                'model' => $model,
                'school_name' => $school_name,
            ]);
        }
    }

    /**
     * Updates an existing Training model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
		if ($school = School::findOne($model->school_id))
		{
			$school_name = $school->name;
		} else {
			$school_name = '';
		}

        $providerStartDate= new \yii\data\ArrayDataProvider([
            'allModels' => $model->startDates,
            'pagination' => [
                'pageSize' => -1
            ]
        ]);

        $providerAircraftToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircraft,
            'pagination' => [
                'pageSize' => -1
            ]
        ]);

        $providerSimulatorToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->simulator,
            'pagination' => [
                'pageSize' => -1
            ]
        ]);
		
        if ($model->loadAll(Yii::$app->request->post())) {



            if ($model->save()) {

                $tmp = array();
                $post = Yii::$app->request->post();
                if (isset($post['Aircraft'])){
                    foreach ($post['Aircraft'] as $a)
                    {
                        $tmp[] = $a['id'];
                    }
                }
                $aircrafts = Aircraft::find()->where(['in', 'id', $tmp])->all();
                $model->linkAll('aircraft', $aircrafts, [], true, true);

                $tmp = array();
                if (isset($post['Simulator'])){
                    foreach ($post['Simulator'] as $a)
                    {
                        $tmp[] = $a['id'];
                    }
                }
                $simulator = Simulator::find()->where(['in', 'id', $tmp])->all();
                $model->linkAll('simulator', $simulator, [], true, true);


                $tmp = array();
                foreach($model->startDates as $sd)
                {
                    $sd->start_date = \DateTime::createFromFormat('Y-m-d', $sd->start_date)->format('Y-m-d');
                    $sd->training_id = $model->id;
                    $sd->save();
                    $tmp[] = $sd->id;
                }
                //print_r(Yii::$app->request->post()['StartDates']);
                if (!isset(Yii::$app->request->post()['StartDates']))
                {
                    TrainingStartDates::deleteAll(['=', 'training_id', $model->id]);
                }
                $start_dates = TrainingStartDates::find()->where(['not in', 'id', $tmp])->andWhere(['=', 'training_id', $model->id])->all();
                foreach($start_dates as $sd)
                {
                    $sd->delete();
                }
                foreach (Yii::$app->request->post('TrainingTranslation', []) as $language => $data) {
                    foreach ($data as $attribute => $translation) {
                        $model->translate($language)->$attribute = $translation;
                    }
                    $model->translate($language)->training_id = $model->id;
                    $model->translate($language)->save();
                }
                //exit();
                return $this->redirect(['view', 'id' => $model->id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
                'providerStartDate' => $providerStartDate,
                'school_name' => $school_name,
                'providerAircraftToTraining' => $providerAircraftToTraining,
                'providerSimulatorToTraining' => $providerSimulatorToTraining,
            ]);
        }
    }

    /**
     * Deletes an existing Training model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }
    
    /**
     * 
     * Export Training information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);

        $providerAircraftToTraining = new \yii\data\ArrayDataProvider([
            'allModels' => $model->aircraft,
        ]);
		$this->layout = NULL;
        $content = $this->renderPartial('_pdf', [
            'model' => $model,
            'providerAircraftToTraining' => $providerAircraftToTraining,
        ]);

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'filename' => 'training_export.pdf',
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    
    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Training::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('backend\training', 'The requested page does not exist.'));
        }
    }
    
    /**
    * Action to load a tabular form grid
    * for AircraftToTraining
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddAircraft()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Aircraft');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
				$school_id = isset($_POST['school_id']) ? $_POST['school_id'] : 0;
            return $this->renderAjax('_formAircraftToTraining', ['row' => $row, 'school_id' => $school_id]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\training', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for AircraftToTraining
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddSimulator()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('Simulator');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
				$school_id = isset($_POST['school_id']) ? $_POST['school_id'] : 0;
            return $this->renderAjax('_formSimulatorToTraining', ['row' => $row, 'school_id' => $school_id]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\training', 'The requested page does not exist.'));
        }
    }

    /**
    * Action to load a tabular form grid
    * for AircraftToTraining
    * @author Yohanes Candrajaya <moo.tensai@gmail.com>
    * @author Jiwantoro Ndaru <jiwanndaru@gmail.com>
    *
    * @return mixed
    */
    public function actionAddStartDates()
    {
        if (Yii::$app->request->isAjax) {
            $row = Yii::$app->request->post('StartDates');
            if((Yii::$app->request->post('isNewRecord') && Yii::$app->request->post('_action') == 'load' && empty($row)) || Yii::$app->request->post('_action') == 'add')
                $row[] = [];
            return $this->renderAjax('_formTrainingStartDates', ['row' => $row]);
        } else {
            throw new NotFoundHttpException(Yii::t('backend\training', 'The requested page does not exist.'));
        }
    }
}
