<?php

namespace backend\controllers;

/**
 * Controller for managing multilinguality.
 * @author Lajos Molnár <lajax.m@gmail.com>
 * @since 1.0
 */
class LanguageController extends \lajax\translatemanager\controllers\ LanguageController{

    public function behaviors() {
        return [];
    }

}
