<?php

if (YII_ENV=='dev')
{
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/../../common/config/params-local.php'),
        require(__DIR__ . '/params.php'),
        require(__DIR__ . '/params-local.php')
    );
} else {
    $params = array_merge(
        require(__DIR__ . '/../../common/config/params.php'),
        require(__DIR__ . '/params.php')
    );
}


return [ 
    'id' => 'app-backend',
    'name' => 'BestPilot Adm',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
	'language' => 'en-US',
	'layout'=>'@app/views/layouts/king.php',
    'modules' => [
		'redactor' => 'yii\redactor\RedactorModule',
		'gridview' => [
          'class' => '\kartik\grid\Module',
          // see settings on http://demos.krajee.com/grid#module
        ],
        'datecontrol' => [
                'class' => '\kartik\datecontrol\Module',
                'displaySettings' => [
                    \kartik\datecontrol\Module::FORMAT_DATE => 'dd-MM-yyyy',
                ],
              // see settings on http://demos.krajee.com/datecontrol#module
        ],
        'treemanager' =>  [
              'class' => '\kartik\tree\Module',
              // see settings on http://demos.krajee.com/tree-manager#module
        ],
        'user-management' => [
			'class' => 'webvimark\modules\UserManagement\UserManagementModule',
			'layout' =>'@app/views/layouts/king.php',

			// 'enableRegistration' => true,

			// Here you can set your handler to change layout for any controller or action
			// Tip: you can use this event in any module
			'on beforeAction'=>function(yii\base\ActionEvent $event) {
					if ( $event->action->uniqueId == 'user-management/auth/login' )
					{
						$event->action->controller->layout = '@app/views/layouts/loginLayout.php';
                        if (Yii::$app->request->get('token', null) !== null){
                            return Yii::$app->getResponse()->redirect(['school/activate-admin', 'token'=>Yii::$app->request->get('token', null)])->send();
                        }
                        if (!Yii::$app->user->isGuest){
                            return Yii::$app->getResponse()->redirect(['site/index'])->send();
                        }
					};
				},
            'on afterAction'=>function(yii\base\ActionEvent $event) {
					if ( $event->action->uniqueId == 'user-management/auth/logout' )
					{
                        return Yii::$app->getResponse()->redirect(['user-management/auth/login'])->send();
					};

					if ( $event->action->uniqueId == 'user-management/auth/login' )
                    {
                        if (!Yii::$app->user->isGuest){
                            return Yii::$app->getResponse()->redirect(['site/index'])->send();
                        }
                    }
				},
		],
        'rbac' => [
            'class' => 'yii2mod\rbac\Module',
            // Some controller property maybe need to change.
            'controllerMap' => [
                'assignment' => [
                    'class' => 'yii2mod\rbac\controllers\AssignmentController',
                    'userIdentityClass' => 'common\models\User',
                    //'searchClass' => 'Your own search model class',
                    'idField' => 'id',
                    'usernameField' => 'email',
                    'gridViewColumns' => [
                        'id',
                        'username',
                        'email'
                    ]
                ]
            ]
        ],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            //'viewPath' => '@app/views/translatemanager',
            'controllerNamespace' => 'backend\controllers',
            'allowedIPs' => ['*'],
            'layout' => null,
            'roles' => ['admin', 'superadmin', 'director'],
            'patterns' => ['*.js', '*.php'],
            'ignoredCategories' => ['yii'],
            'defaultExportFormat' => 'json'
        ],
    ],
    'components' => [
        'request' => [
			'baseUrl'=>'/',
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
			'class' => 'webvimark\modules\UserManagement\components\UserConfig',
			// Comment this if you don't want to record user logins
			'on afterLogin' => function($event) {
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
            }
		],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
			'baseUrl'=>'/',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'site/index',
                //'<_m>' => '<_m>/default/index',
                //'<_m>/default' => '<_m>/default/index',
                //'<_m>/index' => '<_m>/default/index',
                //'<_m>/<_c>/<_a>' => '<_m>/<_c>/<_a>',
                '<_c>/<_a>'  => '<_c>/<_a>',
                //'<_m>/<_a>' => '<_m>/default/<_a>',
            ],
        ],
    ],
    /*'as access' => [
        'class' => yii2mod\rbac\filters\AccessControl::class,
        'allowActions' => [
            'site/*',
        ]
    ],*/
    'params' => $params,
];
