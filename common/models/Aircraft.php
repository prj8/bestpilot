<?php

namespace common\models;

use Yii;
use \common\models\base\Aircraft as BaseAircraft;

/**
 * This is the model class for table "aircraft".
 */
class Aircraft extends BaseAircraft
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'multi_engine', 'gps', 'radio', 'ils', 'night', 'ifr', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['remarks'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['type', 'glass_cockpit'], 'string', 'max' => 255],
            [['register_no'], 'string', 'max' => 16],
            [['aircraft_year', 'engine_year'], 'string', 'max' => 64],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }


    public function afterFind()
    {
        parent::afterFind();
        $this->register_no = trim($this->register_no);
    }

}
