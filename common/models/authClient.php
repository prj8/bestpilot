<?php

namespace common\models;

use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $user_id
 * @property string $surname
 * @property string $name
 * @property string $middle_name
 * @property string $birthday
 * @property integer $gender
 * @property string $avatar_url
 * @property string $balance
 * @property string $bonus_balance
 * @property integer $user_affiliate_id
 *
 * @property User $user
 */
class authClient extends ActiveRecord{

    public static function tableName(){
        return '{{%user_authclient}}';
    }

    public function rules()
    {
        return [
            [['user_id', 'authClient_name', 'authClient_id'], 'required'],
            [['user_id'], 'integer'],
            [['user_id'], 'exist', 'targetAttribute' => 'id', 'targetClass' => '\common\models\User'],
            [['authClient_name', 'authClient_id'], 'string'],
            [['created_at', 'last_login'], 'date', 'format'=>'yyyy-M-d H:m:s'],
            [['created_at', 'last_login'], 'default', 'value' => date('Y-m-d H:i:s')],
        ];
    }


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'last_login'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['last_login'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id'=>'user_id']);
    }
}
