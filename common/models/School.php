<?php

namespace common\models;

use \common\models\base\School as BaseSchool;

/**
 * This is the model class for table "school".
 */
class School extends BaseSchool
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'city'], 'required'],
            [['coordinates', 'remarks'], 'string'],
            [['has_en', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'active'], 'integer'],
            [['logo', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'street', 'city', 'www', 'email', 'phone', 'contact_person'], 'string', 'max' => 255],
            [['street_no', 'post_code'], 'string', 'max' => 12],
            ['country_id', 'string', 'max' => 3],
            ['country_id', 'exist', 'skipOnError' => true, 'targetClass' => \backend\models\Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(\backend\models\Country::className(), ['id' => 'country_id']);
    }
	
}
