<?php

namespace common\models;

use \common\models\base\Address as BaseAddress;

/**
 * This is the model class for table "address".
 */
class Address extends BaseAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['country_id'], 'string', 'max' => 3],
            [['city'], 'string', 'max' => 50],
            [['zip-code'], 'string', 'max' => 10],
            [['address', 'coordinates'], 'string', 'max' => 212],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
