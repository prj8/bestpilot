<?php

namespace common\models;

use Yii;
use \common\models\base\SimulatorToTraining as BaseSimulatorToTraining;

/**
 * This is the model class for table "simulator_to_training".
 */
class SimulatorToTraining extends BaseSimulatorToTraining
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['aircraft_id', 'training_id'], 'required'],
            [['aircraft_id', 'training_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
