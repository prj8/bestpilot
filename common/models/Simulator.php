<?php

namespace common\models;

use Yii;
use \common\models\base\Simulator as BaseSimulator;

/**
 * This is the model class for table "simulator".
 */
class Simulator extends BaseSimulator
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['school_id', 'type'], 'required'],
            [['school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['aircraft_types', 'remarks'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['type', 'class'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
