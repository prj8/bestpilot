<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[Simulator]].
 *
 * @see Simulator
 */
class SimulatorQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Simulator[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Simulator|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}