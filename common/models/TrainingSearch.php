<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Training;

/**
 * common\models\TrainingSearch represents the model behind the search form about `common\models\Training`.
 */
 class TrainingSearch extends Training
{

    public $country_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'license_id', 'training_type_id', 'theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'country_id'], 'integer'],
            [['name', 'address_theory', 'theory_type', 'address_practice', 'start_text', 'start_date', 'remarks', 'created_at', 'updated_at', 'deleted_at', 'aircraftType'], 'safe'],
            [['price', 'price_theory_group', 'price_theory_individually', 'price_pracitce_cheap', 'price_pracitce_expensive', 'price_additional'], 'number'],
        ];
    }

     /**
      * @inheritdoc
      */
     public function attributeLabels()
     {
         return [
             'dateRange' => Yii::t('common/training', 'Termin'),
             'hasElearning' => Yii::t('common/training', 'E-learning'),
         ];
     }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Training::find()->joinWith('aircraft');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'license_id' => $this->license_id,
            'training_type_id' => $this->training_type_id,
            //'school.country_id' => $this->country_id,
            'theory_hours' => $this->theory_hours,
            'elearning_hours' => $this->elearning_hours,
            'aircraft_hours' => $this->aircraft_hours,
            'simulator_hours' => $this->simulator_hours,
            'start_date' => $this->start_date,
            'price' => $this->price,
            'price_theory_group' => $this->price_theory_group,
            'price_theory_individually' => $this->price_theory_individually,
            'price_pracitce_cheap' => $this->price_pracitce_cheap,
            'price_pracitce_expensive' => $this->price_pracitce_expensive,
            'price_additional' => $this->price_additional,
            'lock' => $this->lock,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'created' => $this->created,
            'modified' => $this->modified,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'address_theory', $this->address_theory])
            ->andFilterWhere(['like', 'theory_type', $this->theory_type])
            ->andFilterWhere(['like', 'address_practice', $this->address_practice])
            ->andFilterWhere(['like', 'start_text', $this->start_text])
            ->andFilterWhere(['like', 'remarks', $this->remarks])
            ->andFilterWhere(['like', 'aircraft.type', $this->aircraftType]);

        return $dataProvider;
    }
}
