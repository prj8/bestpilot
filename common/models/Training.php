<?php

namespace common\models;

use Yii;
use \common\models\base\Training as BaseTraining;

/**
 * This is the model class for table "training".
 */
class Training extends BaseTraining
{
	
	public $school_rate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'school_id'], 'required'],
            [['school_id', 'license_id', 'training_type_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'country_id'], 'integer'],
            [['address_theory', 'address_practice', 'remarks', 'information'], 'string'],
            [['theory_type', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'price'], 'number'],
            [['name', 'theory_type'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	/*
	public function setSchoolRate($param)
	{
		$this->_school_rate = $param;
	}
	
	public function getSchoolRate()
	{
		return $this->_school_rate;
	}
	*/
}
