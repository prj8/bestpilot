<?php

namespace common\models;

use \common\models\base\Country as BaseCountry;

/**
 * This is the model class for table "countries".
 */
class Country extends BaseCountry
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['country', 'currency_id'], 'required'],
            [['country', 'currency_id'], 'string', 'max' => 212],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(\backend\models\base\Currency::className(), ['id' => 'currency_id']);
    }
	
}
