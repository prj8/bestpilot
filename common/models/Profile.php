<?php

namespace common\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;

/**
 * This is the model class for table "{{%profile}}".
 *
 * @property integer $user_id
 * @property string $phone
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'phone'], 'required'],
            [['user_id', 'need_license'], 'integer'],
            [['phone', 'username'], 'string', 'max' => 255],
            [['phone'], PhoneInputValidator::className()],
            [['user_id'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['need_license'], 'exist', 'skipOnError' => true, 'targetClass' => LicenseType::className(), 'targetAttribute' => ['need_license' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('site-app', 'User ID'),
            'phone' => Yii::t('site-app', 'Phone'),
            'need_license' => Yii::t('site-app', 'Need License'),
        ];
    }
}
