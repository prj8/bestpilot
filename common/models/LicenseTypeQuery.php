<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[LicenseType]].
 *
 * @see LicenseType
 */
class LicenseTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return LicenseType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LicenseType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}