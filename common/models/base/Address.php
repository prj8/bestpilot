<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "address".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $country_id
 * @property string $city
 * @property string $zip-code
 * @property string $address1
 * @property string $address2
 * @property string $coordinates
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 * @property \backend\models\Countries $country
 */
class Address extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['country_id'], 'string', 'max' => 3],
            [['city'], 'string', 'max' => 50],
            [['zip-code'], 'string', 'max' => 10],
            [['address', 'coordinates'], 'string', 'max' => 212],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('backend/school', 'ID'),
            'school_id' => Yii::t('backend/school', 'School ID'),
            'country_id' => Yii::t('backend/school', 'Country ID'),
            'city' => Yii::t('backend/school', 'City'),
            'zip-code' => Yii::t('backend/school', 'Zip Code'),
            'address1' => Yii::t('backend/school', 'Address1'),
            'address2' => Yii::t('backend/school', 'Address2'),
            'coordinates' => Yii::t('backend/school', 'Coordinates'),
            'lock' => Yii::t('backend/school', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\common\models\School::className(), ['id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(\common\models\Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryName()
    {
        return $this->country->country;
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\AddressQuery(get_called_class());
    }
}
