<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\web\UploadedFile;

/**
 * This is the base model class for table "school".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 * @property string $street
 * @property string $street_no
 * @property string $city
 * @property string $post_code
 * @property string $coordinates
 * @property string $www
 * @property string $email
 * @property string $phone
 * @property string $contact_person
 * @property integer $has_en
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \common\models\Aircraft[] $aircrafts
 * @property \common\models\Simulator[] $simulators
 * @property \common\models\Training[] $trainings
 * @property \common\models\UserToSchool[] $userToSchools
 */
class School extends \backend\models\Country
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'city'], 'required'],
            [['coordinates', 'remarks'], 'string'],
            [['has_en', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'active'], 'integer'],
            [['logo', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'street', 'city', 'www', 'email', 'phone', 'contact_person'], 'string', 'max' => 255],
            [['street_no', 'post_code'], 'string', 'max' => 12],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/school', 'ID'),
            'name' => Yii::t('common/school', 'Name'),
            'logo' => Yii::t('common/school', 'Logo'),
            'street' => Yii::t('common/school', 'Street'),
            'street_no' => Yii::t('common/school', 'Street No'),
            'city' => Yii::t('common/school', 'City'),
            'post_code' => Yii::t('common/school', 'Post Code'),
            'coordinates' => Yii::t('common/school', 'Coordinates'),
            'www' => Yii::t('common/school', 'Www'),
            'email' => Yii::t('common/school', 'Email'),
            'phone' => Yii::t('common/school', 'Phone'),
            'contact_person' => Yii::t('common/school', 'Contact Person'),
            'has_en' => Yii::t('common/school', 'Has En'),
            'remarks' => Yii::t('common/school', 'Remarks'),
            'lock' => Yii::t('common/school', 'Lock'),
            'school_rate' => Yii::t('common/school', 'School rate'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircrafts()
    {
        return $this->hasMany(\common\models\Aircraft::className(), ['school_id' => 'id'])->andOnCondition(['active' => '1'])->andOnCondition(['deleted' => '0']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddresses()
    {
        return $this->hasMany(\backend\models\Address::className(), ['school_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimulators()
    {
        return $this->hasMany(\common\models\Simulator::className(), ['school_id' => 'id'])->andOnCondition(['active' => '1'])->andOnCondition(['deleted' => '0']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(\common\models\Training::className(), ['school_id' => 'id'])->andOnCondition(['active' => '1'])->andOnCondition(['deleted' => '0']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserToSchools()
    {
        return $this->hasMany(\common\models\UserToSchool::className(), ['school_id' => 'id']);
    }
	
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolRates()
    {
        return $this->hasMany(SchoolRates::className(), ['school_id' => 'id'])->where(['school_rates.published'=>1])->active();
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool_rate()
    {
        $school_rate = \Yii::$app->cache->get(__CLASS__.'/'.$this -> id.'1');
        if($school_rate === false){
            $school_rate = $this->id > 0 ? SchoolRates::find()->select('AVG(rate) as rate')->where(['school_id' => $this->id, 'school_rates.published'=>1])->asArray()->one()['rate'] : 0;
            \Yii::$app->cache->set(__CLASS__.'/'.$this -> id.'1', $school_rate, 3600);
        }
        return $school_rate;
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\SchoolQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\SchoolQuery(get_called_class());
    }

    public function LogoResize($width=0, $height=null){
        $filename = Yii::getAlias('@upload') . '/logos/' . $this->logo;
        if(file_exists($filename)){
            return Yii::$app->thumbnail->url($filename, ['resize' => [
                'width' => $width,
                'height' => $height,
            ]]);
        }else
            return null;
    }
}
