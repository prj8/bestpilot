<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "simulator".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $type
 * @property string $class
 * @property string $aircraft_types
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \common\models\School $school
 */
class Simulator extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'type'], 'required'],
            [['school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['aircraft_types', 'remarks'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['type', 'class'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'simulator';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/simulator', 'ID'),
            'school_id' => Yii::t('common/simulator', 'School ID'),
            'type' => Yii::t('common/simulator', 'Type'),
            'class' => Yii::t('common/simulator', 'Class'),
            'aircraft_types' => Yii::t('common/simulator', 'Aircraft Types'),
            'remarks' => Yii::t('common/simulator', 'Remarks'),
            'lock' => Yii::t('common/simulator', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\common\models\School::className(), ['id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimulatorToTrainings()
    {
        return $this->hasMany(\common\models\SimulatorToTraining::className(), ['simulator_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\SimulatorQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\SimulatorQuery(get_called_class());
    }
}
