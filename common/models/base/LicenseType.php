<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the base model class for table "license_type".
 *
 * @property integer $id
 * @property string $license
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \common\models\Training[] $trainings
 */
class LicenseType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['license'], 'required'],
            [['lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'order'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['license'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'license_type';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/training', 'ID'),
            'license' => Yii::t('common/training', 'License'),
            'lock' => Yii::t('common/training', 'Lock'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
        return $this->hasMany(\common\models\Training::className(), ['license_id' => 'id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['description', 'text', 'requirements'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }
    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    public function getTranslations(){
        return $this->hasMany(LicenseTypeTranslation::className(), ['license_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\LicenseTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\LicenseTypeQuery(get_called_class());
    }
}
