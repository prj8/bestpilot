<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior,
    yii\helpers\ArrayHelper,
    common\models\base\SchoolRates;
use creocoder\translateable\TranslateableBehavior;

/**
 * This is the base model class for table "training".
 *
 * @property integer $id
 * @property string $name
 * @property integer $school_id
 * @property integer $license_id
 * @property integer $training_type_id
 * @property string $address_theory
 * @property integer $theory_hours
 * @property integer $elearning_hours
 * @property string $theory_type
 * @property string $address_practice
 * @property integer $aircraft_hours
 * @property integer $simulator_hours
 * @property string $start_text
 * @property string $start_date
 * @property double $price
 * @property double $price_theory_group
 * @property double $price_theory_individually
 * @property double $price_pracitce_cheap
 * @property double $price_pracitce_expensive
 * @property double $price_additional
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \common\models\AircraftToTraining[] $aircraftToTrainings
 * @property \common\models\School $school
 * @property \common\models\LicenseTypes $license
 * @property \common\models\TrainingTypes $trainingType
 */
class Training extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    public $school_rate=null;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'license_id', 'training_type_id'], 'required'],
            [['school_id', 'license_id', 'training_type_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'country_id'], 'integer'],
            [['address_theory', 'address_practice', 'remarks', 'price_additional', 'information', 'lang'], 'string'],
            [['name', 'theory_type', 'created_at', 'updated_at', 'deleted_at', 'dateRange', 'aircraftType', 'address_theory_id', 'address_practice_id'], 'safe'],
            [['theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'price'], 'number'],
            [['name', 'theory_type'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%training}}';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/training', 'ID'),
            'name' => Yii::t('common/training', 'Name'),
            'school_id' => Yii::t('common/training', 'School ID'),
            'license_id' => Yii::t('common/training', 'License Type ID'),
            'training_type_id' => Yii::t('common/training', 'Training Type ID'),
            'address_theory' => Yii::t('common/training', 'Address Theory'),
            'theory_hours' => Yii::t('common/training', 'Theory Hours'),
            'elearning_hours' => Yii::t('common/training', 'Elearning Hours'),
            'theory_type' => Yii::t('common/training', 'Theory Type'),
            'address_practice' => Yii::t('common/training', 'Address Practice'),
            'aircraft_hours' => Yii::t('common/training', 'Aircraft Hours'),
            'simulator_hours' => Yii::t('common/training', 'Simulator Hours'),
            'price' => Yii::t('common/training', 'Price'),
            'remarks' => Yii::t('common/training', 'Remarks'),
            'information' => Yii::t('common/training', 'Information'),
            'requirements' => Yii::t('common/training', 'Requirements'),
            'lock' => Yii::t('common/training', 'Lock'),
            'school_rate' => Yii::t('common/training', 'School rate'),
            'aircraftAge' => Yii::t('common/training', 'Arcraft Age'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraft()
    {
        return $this->hasMany(\common\models\Aircraft::className(), ['id' => 'aircraft_id'])->viaTable('aircraft_to_training', ['training_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSimulator()
    {
        return $this->hasMany(\common\models\Simulator::className(), ['id' => 'simulator_id'])->viaTable('simulator_to_training', ['training_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\common\models\School::className(), ['id' => 'school_id'])->where(['school.deleted'=>0]);
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(\backend\models\base\Country::className(), ['id' => 'country_id']);
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolRates()
    {
        return $this->hasMany(\common\models\base\SchoolRates::className(), ['school_id' => 'school_id'])->where(['school_rates.published'=>1])->active();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartDates()
    {
        return $this->hasMany(\common\models\base\TrainingStartDates::className(), ['training_id' => 'id'])->orderBy(['training_start_dates.start_date'=>'ASC']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLicenseType()
    {
        return $this->hasOne(\common\models\LicenseType::className(), ['id' => 'license_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainingType()
    {
        return $this->hasOne(\common\models\TrainingType::className(), ['id' => 'training_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressPractice()
    {
        return $this->hasOne(\common\models\Address::className(), ['id' => 'address_practice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressTheory()
    {
        return $this->hasOne(\common\models\Address::className(), ['id' => 'address_theory_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguages()
    {
        $langs = [];
        foreach (self::find()->select('lang')->where('lang != ""')->each() as $lang){
            $langs = array_merge($langs, $lang->lang);
        }

        return \lajax\translatemanager\models\Language::find()->where(['or', ['language_id'=>array_unique($langs)], ['language'=>array_unique($langs)]])->asArray()->all();
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'translateable' => [
                'class' => TranslateableBehavior::className(),
                'translationAttributes' => ['information'],
                // translationRelation => 'translations',
                // translationLanguageAttribute => 'language',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\TrainingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\TrainingQuery(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();
        $this->lang = array_filter(explode(',', $this->lang));
        if ($this->school_rate != null) {
            $this->school_rate = isset($this->school->school_rate) ? $this->school->school_rate : 0;
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->lang==NULL) $this->lang = ['pl'];
            $this->lang = implode(',', $this->lang);
            return true;
        }

        return false;
    }

    public function transactions(){
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT | self::OP_UPDATE,
        ];
    }

    public function getTranslations(){
        return $this->hasMany(TrainingTranslation::className(), ['training_id' => 'id']);
    }
}
