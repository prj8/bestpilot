<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_rates".
 *
 * @property integer $id
 * @property integer $rate
 * @property integer $school_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 */
class SchoolRates extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rate', 'published', 'school_id', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'user_id', 'training_id'], 'integer'],
            [['rate'], 'number', 'min' => 1, 'max' => 5],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['lock', 'published'], 'default', 'value' => '0'],
            [['name', 'text'], 'string'],
            ['email', 'email'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \common\models\User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['training_id'], 'exist', 'skipOnError' => true, 'targetClass' => Training::className(), 'targetAttribute' => ['training_id' => 'id']],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school_rates';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/school', 'ID'),
            'rate' => Yii::t('common/school', 'Na ile gwiazdek oceniasz szkolenie?'),
            'school_id' => Yii::t('common/school', 'School ID'),
            'lock' => Yii::t('common/school', 'Lock'),
            'school_rate' => Yii::t('common/school', 'School rate'),
            'name' => Yii::t('common/school', 'Imię i Nazwisko'),
            'email' => Yii::t('common/school', 'Twój email'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\backend\models\School::className(), ['id' => 'school_id']);
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\SchoolRatesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\SchoolRatesQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id'=>'user_id']);
    }
}
