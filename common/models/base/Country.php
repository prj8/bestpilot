<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "countries".
 *
 * @property integer $id
 * @property string $country
 *
 * @property \backend\models\School[] $schools
 */
class Country extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country', 'currency_id'], 'required'],
            [['id'], 'integer'],
            [['country', 'currency_id'], 'string', 'max' => 212],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\models\base\Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/countries', 'ID'),
            'country' => Yii::t('common/countries', 'Country'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchools()
    {
        return $this->hasMany(\common\models\School::className(), ['country_id' => 'id'])->inverseOf('county');
    }
    
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \backend\models\CountryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\CountryQuery(get_called_class());
    }
}
