<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "aircraft".
 *
 * @property integer $id
 * @property integer $school_id
 * @property string $type
 * @property string $register_no
 * @property integer $multi_engine
 * @property string $aircraft_year
 * @property string $engine_year
 * @property string $glass_cockpit
 * @property integer $gps
 * @property integer $radio
 * @property integer $ils
 * @property integer $night
 * @property integer $ifr
 * @property string $remarks
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \common\models\School $school
 * @property \common\models\AircraftToTraining[] $aircraftToTrainings
 */
class Aircraft extends \backend\models\base\Aircraft
{
    use \mootensai\relation\RelationTrait;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_id', 'multi_engine', 'gps', 'radio', 'ils', 'night', 'ifr', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['remarks'], 'string'],
            [['created_at', 'updated_at', 'deleted_at', 'type_id'], 'safe'],
            [['type', 'glass_cockpit'], 'string', 'max' => 255],
            [['register_no'], 'string', 'max' => 16],
            [['aircraft_year', 'engine_year'], 'string', 'max' => 64],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ];
    }
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'aircraft';
    }

    /**
     * 
     * @return string
     * overwrite function optimisticLock
     * return string name of field are used to stored optimistic lock 
     * 
     */
    public function optimisticLock() {
        return 'lock';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common/aircraft', 'ID'),
            'school_id' => Yii::t('common/aircraft', 'School ID'),
            'register_no' => Yii::t('common/aircraft', 'Register No'),
            'multi_engine' => Yii::t('common/aircraft', 'Multi Engine'),
            'aircraft_year' => Yii::t('common/aircraft', 'Aircraft Year'),
            'engine_year' => Yii::t('common/aircraft', 'Engine Year'),
            'glass_cockpit' => Yii::t('common/aircraft', 'Glass Cockpit'),
            'gps' => Yii::t('common/aircraft', 'Gps'),
            'radio' => Yii::t('common/aircraft', 'Radio'),
            'ils' => Yii::t('common/aircraft', 'Ils'),
            'night' => Yii::t('common/aircraft', 'Night'),
            'ifr' => Yii::t('common/aircraft', 'Ifr'),
            'remarks' => Yii::t('common/aircraft', 'Remarks'),
            'lock' => Yii::t('common/aircraft', 'Lock'),
            'aircraftAge' => Yii::t('common/aircraft', 'Aircraft Age'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchool()
    {
        return $this->hasOne(\common\models\School::className(), ['id' => 'school_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrainings()
    {
		return $this->hasMany(\common\models\Training::className(), ['id' => 'training_id'])->viaTable('aircraft_to_training', ['aircraft_id' => 'id']);
    }
	
/**
     * @inheritdoc
     * @return array mixed
     */ 
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftType()
    {
        return $this->hasOne(\backend\models\AircraftType::className(), ['id' => 'type_id']);
    }

    /**
     * @inheritdoc
     * @return \common\models\AircraftQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\AircraftQuery(get_called_class());
    }
}
