<?php

namespace common\models;

use Yii;
use \common\models\base\AircraftToTraining as BaseAircraftToTraining;

/**
 * This is the model class for table "aircraft_to_training".
 */
class AircraftToTraining extends BaseAircraftToTraining
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['aircraft_id', 'training_id'], 'integer'],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
}
