<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Aircraft;

/**
 * common\models\AircraftSearch represents the model behind the search form about `common\models\Aircraft`.
 */
 class AircraftSearch extends Aircraft
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'multi_engine', 'gps', 'radio', 'ils', 'night', 'ifr', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted'], 'integer'],
            [['register_no', 'aircraft_year', 'engine_year', 'glass_cockpit', 'remarks', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Aircraft::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_id' => $this->school_id,
            'multi_engine' => $this->multi_engine,
            'gps' => $this->gps,
            'radio' => $this->radio,
            'ils' => $this->ils,
            'night' => $this->night,
            'ifr' => $this->ifr,
            'lock' => $this->lock,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'created' => $this->created,
            'modified' => $this->modified,
            'deleted' => $this->deleted,
        ]);

        $query->andFilterWhere(['like', 'register_no', $this->register_no])
            ->andFilterWhere(['like', 'aircraft_year', $this->aircraft_year])
            ->andFilterWhere(['like', 'engine_year', $this->engine_year])
            ->andFilterWhere(['like', 'glass_cockpit', $this->glass_cockpit])
            ->andFilterWhere(['like', 'remarks', $this->remarks]);

        return $dataProvider;
    }
}
