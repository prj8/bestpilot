<?php

namespace common\models;

use Yii;
use \common\models\base\LicenseType as BaseLicenseType;

/**
 * This is the model class for table "license_type".
 */
class LicenseType extends BaseLicenseType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['license'], 'required'],
            [['lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'order'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['license'], 'string', 'max' => 255],
            [['lock'], 'default', 'value' => '0'],
            [['lock'], 'mootensai\components\OptimisticLockValidator']
        ]);
    }
	
	public function getLicenceDescription(){
		return $this->description;
	}

	public function getTLicenceDescription(){
        if(!$description = \Yii::$app->cache->get(__CLASS__.'/description'.$this -> id.\Yii::$app->language)){
            $description = $this -> hasTranslation() ? $this -> translate() -> description : $this -> description;
            \Yii::$app->cache->set(__CLASS__.'/description'.$this -> id.\Yii::$app->language, $description, 3600*24);
        }

        return $description;
	}

	public function getTText(){
        if(!$text = \Yii::$app->cache->get(__CLASS__.'/text'.$this -> id.\Yii::$app->language)){
            $text = $this -> hasTranslation() ? $this -> translate() -> text : $this -> text;
            \Yii::$app->cache->set(__CLASS__.'/text'.$this -> id.\Yii::$app->language, $text, 3600*24);
        }

        return $text;
	}

	public function getTRequirements(){
        if(!$requirements = \Yii::$app->cache->get(__CLASS__.'/requirements'.$this -> id.\Yii::$app->language)){
            $requirements = $this -> hasTranslation() ? $this -> translate() -> requirements : $this -> requirements;
            \Yii::$app->cache->set(__CLASS__.'/requirements'.$this -> id.\Yii::$app->language, $requirements, 3600*24);
        }

        return $requirements;
	}
	
}
