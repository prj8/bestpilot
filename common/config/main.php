<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
	'bootstrap' => ['debug'],
    'language' => 'en-US',
    'sourceLanguage' => 'pl',
    'name' => 'bestpilot.com',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
		'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
		'i18n' => [
			'translations' => [
				'*' => [
					'class' => 'yii\i18n\DbMessageSource',
					'db' => 'db',
					'sourceLanguage' => 'xx-XX', // Developer language
					'sourceMessageTable' => '{{%language_source}}',
					'messageTable' => '{{%language_translate}}',
					'cachingDuration' => 86400,
					'enableCaching' => true,
				],
			],
		],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['guest', 'user'],
        ],
        'thumbnail' => [
            'class' => 'sadovojav\image\Thumbnail',
            'cachePath' => '@upload/thumbnail',
            'prefixPath' => '/upload',
            //'cacheExpire' => 604800,
            'cacheExpire' => 10,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport'=>false,
            'viewPath' => '@common/mail',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'office@bestpilot.com',
                'password' => 'formularzevionica!@#',
                'port' => '465',
                'encryption' => 'ssl',
                'plugins' => [
                    [
                        'class' => '\Openbuildings\Swiftmailer\CssInlinerPlugin',
                        //'constructArgs' => [20],
                    ],
                ],
            ],
        ],
    ],
	'modules' => [
		'debug' => [
			'class' => 'yii\debug\Module',
			'allowedIPs' => ['127.0.0.1'],
		],
	],
];
