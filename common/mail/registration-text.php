<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/user-confirmation', 'token' => $user->confirmation_token]);
?>
<?= Yii::t('mail/registration', 'Hello'); ?> <?= $user->username ?>,

<?= Yii::t('mail/registration', 'Thank you for registration in BestPilot.com.'); ?>
<?= Yii::t('mail/registration', 'To complete process of registration you need to click in link below:'); ?>
<?= $confirmationLink ?>


<?= Yii::t('mail/registration', 'Team BestPilot.com'); ?>

