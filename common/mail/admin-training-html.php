<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$trainingLink = Yii::$app->urlManager->createAbsoluteUrl(['training/view', 'id' => $model->id]);
?>
<div class="training-confirmation">
    <p><?= Yii::t('mail/confirmTraining', 'Witaj'); ?>,</p>

    <p><?= Yii::t('mail/confirmTraining', 'Użytkownik:'); ?><br />
        <?= Yii::t('mail/confirmTraining', 'Imię i nazwisko:'); ?> <?= $user->name ?><br />
        <?= Yii::t('mail/confirmTraining', 'Email:'); ?> <a href="mailto:<?= $user->email ?>"><?= $user->email ?></a><br />
        <?= Yii::t('mail/confirmTraining', 'Telefon:'); ?> <?= $user->phone ?><br />
    </p>
    <p><?= Yii::t('mail/confirmTraining', 'Zapisał się na szkolenie:'); ?>
        <?= Yii::t('mail/confirmTraining', 'Nazwa:'); ?> <?= $model->name ?><br />
        <a href="<?= $trainingLink ?>" target="_blank"><?= $trainingLink ?></a>
    </p>
    <p>&nbsp;<br />&nbsp;</p>
    <p><?= Yii::t('mail/confirmTraining', 'Team BestPilot.com'); ?></p>
</div>
