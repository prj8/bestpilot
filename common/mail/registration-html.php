<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$confirmationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/user-confirmation', 'token' => $user->confirmation_token]);
?>
<div class="user-confirmation">
    <p><?= Yii::t('mail/registration', 'Hello'); ?> <?= Html::encode($user->username) ?>,</p>

    <p><?= Yii::t('mail/registration', 'Thank you for registration in BestPilot.com'); ?></p>
    <p><?= Yii::t('mail/registration', 'To complete process of registration you need to click in link below:'); ?></p>
    <p><?= Html::a(Html::encode($confirmationLink), $confirmationLink) ?></p>
    <p>&nbsp;<br />&nbsp;</p>
    <p><?= Yii::t('mail/registration', 'Team BestPilot.com'); ?></p>
</div>
