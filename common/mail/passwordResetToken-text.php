<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<?= Yii::t('mail/password', 'Hello '); ?><?= $user->username ?>,

<?= Yii::t('mail/password', 'Follow the link below to reset your password:'); ?>

<?= $resetLink ?>

<?= Yii::t('mail/confirmTraining', 'Team BestPilot.com'); ?>
