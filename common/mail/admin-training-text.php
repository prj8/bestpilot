<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$trainingLink = Yii::$app->urlManager->createAbsoluteUrl(['training/view', 'id' => $model->id]);
?>

    <?= Yii::t('mail/confirmTraining', 'Witaj'); ?>,

    <?= Yii::t('mail/confirmTraining', 'Użytkownik:'); ?>
        <?= Yii::t('mail/confirmTraining', 'Imię i nazwisko:'); ?> <?= $user->name ?>
        <?= Yii::t('mail/confirmTraining', 'Email:'); ?> <?= $user->email ?>
        <?= Yii::t('mail/confirmTraining', 'Telefon:'); ?> <?= $user->phone ?>

    <?= Yii::t('mail/confirmTraining', 'Zapisał się na szkolenie:'); ?>
        <?= Yii::t('mail/confirmTraining', 'Nazwa:'); ?> <?= $model->name ?>
<?= $trainingLink ?>



    <?= Yii::t('mail/confirmTraining', 'Team BestPilot.com'); ?>

