<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
<div class="password-reset">
    <p><?= Yii::t('mail/password', 'Hello'); ?> <?= Html::encode($user->username) ?>,</p>

    <p><?= Yii::t('mail/password', 'Follow the link below to reset your password:'); ?></p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>

    <p><?= Yii::t('mail/confirmTraining', 'Team BestPilot.com'); ?></p>
</div>
