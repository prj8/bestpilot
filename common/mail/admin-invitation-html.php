<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

if ($token != '')
    $link = Yii::$app->urlManager->createAbsoluteUrl(['user-management/auth/login', 'token' => $token]);
else
    $link = Yii::$app->urlManager->createAbsoluteUrl(['user-management/auth/login']);
?>
<div class="school-invitation">
    <p><?= Yii::t('backend/school/inviteUser', 'Witaj'); ?>,</p>

    <p><?= Yii::t('backend/school/inviteUser', 'Dostałeś zaproszenie do zarządzania szkołą {szkola} w portalu BestPilot.com'); ?><br />
        <?= Yii::t('backend/school/inviteUser', 'Aby z niego skorzystać skorzystaj z linku poniżej'); ?><br />
    </p>
    <p><?= Html::a(Html::encode($link), $link) ?></p>
    <p>&nbsp;<br />&nbsp;</p>
    <p><?= Yii::t('backend/school/inviteUser', 'Team BestPilot.com'); ?></p>
</div>
