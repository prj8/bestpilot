<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

if ($token!= '')
    $link = Yii::$app->urlManager->createAbsoluteUrl(['user-management/auth/login', 'token' => $token]);
else
    $link = Yii::$app->urlManager->createAbsoluteUrl(['user-management/auth/login']);
?>

    <?= Yii::t('backend/school/inviteUser', 'Witaj'); ?>,

    <?= Yii::t('backend/school/inviteUser', 'Dostałeś zaproszenie do zarządzania szkołą {szkola} w portalu BestPilot.com'); ?>
        <?= Yii::t('backend/school/inviteUser', 'Aby z niego skorzystać skorzystaj z linku poniżej'); ?>

    <?= $link ?>


    <?= Yii::t('backend/school/inviteUser', 'Team BestPilot.com'); ?>
