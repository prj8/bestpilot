<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$confirmationLink = Yii::$app->urlManager->createAbsoluteUrl(['site/user-confirmation', 'token' => $user->confirmation_token]);
?>
<?= Yii::t('mail/confirmTraining', 'Witaj'); ?> <?= Html::encode($user->name) ?>,

<?= Yii::t('mail/confirmTraining', 'Dziękujemy za zapisanie się na treining {name}', ['name'=>$model->name]); ?></p>
<?= Yii::t('mail/confirmTraining', 'Wkrótce nasz pracownik skontaktuje się z Tobą w celu potwierdzenia szczegółów.'); ?></p>


<?= Yii::t('mail/confirmTraining', 'Team BestPilot.com'); ?>
