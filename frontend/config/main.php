<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
use \yii\web\Request;
$baseUrl = str_replace('/frontend/web', '', (new Request)->getBaseUrl());

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'name' => 'bestpilot.com',
	'modules' => [
		'user-management' => [
			'class' => 'webvimark\modules\UserManagement\UserManagementModule',
			'enableRegistration' => true,
			'on beforeAction'=>function(yii\base\ActionEvent $event) {
					if ( $event->action->uniqueId == 'user-management/auth/login' )
					{
						$event->action->controller->layout = 'loginLayout.php';
					};
				},
		],
        'translatemanager' => [
            'class' => 'lajax\translatemanager\Module',
            'allowedIPs' => ['127.0.0.1'],
        ],
	],
    'components' => [
        'request' => [
			'baseUrl'=>'/',
			'cookieValidationKey' => '4ebLLtq4_AzPgYHUN1ZzYnsQIWKTD0Yf',
            'csrfParam' => '_csrf-frontend',
            'as sypexGeo' => [
                'class' => 'omnilight\sypexgeo\GeoBehavior',
                'sypexGeo' => [
                    'database' => '@common/GeoIP/SxGeoCity.dat',
                ]
            ]
        ],
       'user' => [
			'class' => 'webvimark\modules\UserManagement\components\UserConfig',
            'identityClass' => 'common\models\User',
			// Comment this if you don't want to record user logins
			'on afterLogin' => function($event) {
                \webvimark\modules\UserManagement\models\UserVisitLog::newVisitor($event->identity->id);
                //return Yii::$app->getResponse()->redirect(['office/index'])->send();
            },
            'on afterRegistration' => function($event) {
               //return Yii::$app->getResponse()->redirect(['office/index'])->send();
            },
            'loginUrl' => ['/site/login'],
		],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'languagepicker' => [
            'class' => 'twofox\languagepicker\Component', // List of available languages (icons and text)
            'languages' => function () {                 // List of available languages (icons only)
                if (!$Languages = \Yii::$app -> cache -> get('Languages')){
                    $Languages = \lajax\translatemanager\models\Language::getLanguageNames(true);
                    \Yii::$app -> cache -> set('Languages', $Languages);
                }
                return $Languages;
            }
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'css' => [
                        '/bootstrap/css/bootstrap.css?1',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'sourcePath' => null,
                    'js' => [
                        YII_ENV_DEV ? '/bootstrap/js/bootstrap.js' : '/bootstrap/js/bootstrap.min.js',
                    ]
                ],
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '1470008553039964',
                    'clientSecret' => '146724e4892e9e7c51cbe91fb2993773',
                    'attributeNames' => ['name', 'email', 'first_name', 'last_name'],
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\Google',
                    'clientId' => '638678355999-j2dq3p4i7gil77tbl6kmbap12ornfnuh.apps.googleusercontent.com',
                    'clientSecret' => 'wEpwlUdsb5rV0m0-T4s5dSCR',
                ],
            ],
        ],
        'urlManager' => [
			'baseUrl'=>'/',
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'defaultLanguage' => 'pl',
            'showDefault' => false,
            //'suffix' => '/',
            'class' => '\metalguardian\language\UrlManager',
            'languages' => function () {
                return array_keys(\Yii::$app->languagepicker->languages);
            },
            'autoLanguageRules' => true,
            'rules' => [
                '' => 'site/index',
                '<_c>/<_a>' => '<_c>/<_a>',
            ],
        ]
    ],
    'on beforeAction' => function ($event) {
        $session = new \yii\web\Session;
        $session->open();
        $model = new \backend\models\base\Currency();
        if ($model->load(\Yii::$app->request->post())) {
            if(\backend\models\base\Currency::find()->where(['id'=>$model->id, 'active'=>1])->one()){
                \Yii::$app->params['currency'] = $model->id;
                $session['currency'] = $model->id;
            }
        }elseif(isset($session['currency'])){
            \Yii::$app->params['currency'] = $session['currency'];
        }else{

            $city = \Yii::$app->request->getCity();
            if (\Yii::$app->request->get('language', NULL)== NULL) {
                if ($city != null && strlen($city['country']['iso']) > 0)
                if (strtolower($city['country']['iso']) != 'pl') {
                    \Yii::$app->response->redirect(\yii\helpers\Url::current(['language' => 'en']));
                }
            }

            $currency = isset($city['country']['iso']) ? \imanilchaudhari\CurrencyConverter\CountryToCurrency::getCurrency($city['country']['iso']) : \Yii::$app->params['currency'];

            if(\backend\models\base\Currency::find()->where(['id'=>$currency, 'active'=>1])->one()){
                $session['currency'] = $currency;
                \Yii::$app->params['currency'] = $currency;
            }else
                $session['currency'] = \Yii::$app->params['currency'];
        }
    },
    'params' => $params,
];
