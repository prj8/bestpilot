<?php
return [
    'adminEmail' => 'office@bestpilot.com',
    'defaultView' => 'grid',
    'currency' => 'EUR',
];
