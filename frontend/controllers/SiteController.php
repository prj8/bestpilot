<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use frontend\models\TrainingSearch,
    common\models\User,
    common\models\Profile,
    common\models\authClient;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
                'successUrl' => \yii\helpers\Url::to(['site/index']),
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrainingSearch();	
        return $this->render('index', [
            'searchModel' => $searchModel
        ]);
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionUserConfirmation()
    {
        if ($user = User::findOne(['=', 'confirmation_token', Yii::$app->request->get('token', '0')])) {
            $header = Yii::t('registration/confirmationEmail', 'Thank you for confirming your email address.');
            $text = Yii::t('registration/confirmationEmail', 'Now you can login to BestPilot and find your training.');
            $status = 'success';
        } else {
            $header = Yii::t('registration/confirmationEmail', 'Your email is already confirmed.');
            $text = Yii::t('registration/confirmationEmail', 'You can login to BestPilot and find your training.');
            $status = 'error';
        }
        return $this->render('userConfirmation', [
            'header' => $header,
            'text' => $text,
            'status' => $status
        ]);
    }

    public function oAuthSuccess($client) {
        // get user data from client
        $userAttributes = $client->getUserAttributes();
        if($client->getId()=='google')
            $userAttributes['email'] = $userAttributes['emails'][0]['value'];

        if(!$find = authClient::find()->where(['authClient_name'=>$client->getId(), 'authClient_id'=>$userAttributes['id']])->one()){
            if (!$user = User::findByUsername($userAttributes['email'])){
                $model = new SignupForm();
                $model->email = $userAttributes['email'];
                $model->password = \Yii::$app->security->generateRandomString(8);
                $model->password_repeat = $model->password;

                if($model->signup()){
                    $user = User::findByUsername($model->email);

                    if(!$profile = Profile::findOne($user->id))
                        $profile = new Profile();

                    if($user->status != User::STATUS_ACTIVE){
                        $user->status = User::STATUS_ACTIVE;
                        $user->save(false);
                    }

                    $profile->username = $client->getId()=='google' ? $userAttributes['displayName'] : $userAttributes['name'];
                    $profile->user_id = $user->id;
                    $profile->save(false);
                }
            }

            $find = new authClient();
            $find->authClient_name = $client->getId();
            $find->authClient_id = $userAttributes['id'];
            $find->user_id = $user->id;
        }

        $find->last_login = date('Y-m-d H:i:s');
        $find->save();

        $modelLoginForm = new LoginForm();
        $modelLoginForm->username = $find->user->email;
        $modelLoginForm->loginSocial();
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['office/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Thank you for contacting us. We will respond to you as soon as possible.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('pages', 'There was an error sending email.'));
            }

            return $this->redirect(\Yii::$app->request->getReferrer());
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) { //funkcja signup wysyła maila do usera
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Check your email for further instructions.'));
                return $this->redirect(['site/index']);
                /*
                if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(['site/index']);
                }
                */
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', Yii::t('pages', 'Check your email for further instructions.'));
            } else {
                Yii::$app->session->setFlash('error', Yii::t('pages', 'Sorry, we are unable to reset password for email provided.'));
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', Yii::t('pages', 'New password was saved.'));

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
