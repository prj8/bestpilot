<?php

namespace frontend\controllers;

use common\models\School;
use frontend\models\TrainingSearch;
use yii\web\NotFoundHttpException;
use frontend\models\SchoolRates;

class SchoolController extends \yii\web\Controller
{
    public function actionView($id)
    {
        if($model = $this->findModel($id)){
            $searchModel = new TrainingSearch();

            /* school rates */
            $SchoolRatesForm = new SchoolRates();

            if(!\Yii::$app->user->isGuest){
                $SchoolRatesForm->user_id = \Yii::$app->user->id;
                $SchoolRatesForm->email = \Yii::$app->user->identity->email;

                $user = \Yii::$app->user->identity;
                if ($user->profile != null)
                {
                    $SchoolRatesForm->name = $user->profile->username;
                    $ContactForm->phone = $user->profile->phone;
                    $ContactForm->name = $user->profile->username;
                }
                $ContactForm->email = \Yii::$app->user->identity->email;

            }

            $SchoolRatesForm->school_id = $model->id;
            $SchoolRatesForm->training_id = null;

            if ($SchoolRatesForm->load(\Yii::$app->request->post()) && $SchoolRatesForm->validate()) {
                if($SchoolRatesForm->save()){
                    $SchoolRatesForm->text = '';
                    $SchoolRatesForm->name = '';
                    $SchoolRatesForm->email = '';
                    \Yii::$app->session->setFlash('success', \Yii::t('frontend/training', 'Thank you for your review'));
                    return $this->redirect(['school/view','id'=>$model->id]);
                }else
                    \Yii::$app->session->setFlash('warning', \Yii::t('frontend/training', 'Error. Your review was not added.'));
            }

            return $this->render('view', [
                'searchModel' => $searchModel,
                'model'=>$model,
                'schoolRatesForm'=>$SchoolRatesForm
            ]);
        }

        return $this->goHome();
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = School::find()->where(['id'=>$id, 'deleted'=>0])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('pages', 'The requested page does not exist.'));
        }
    }

}
