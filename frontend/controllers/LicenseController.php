<?php

namespace frontend\controllers;

use common\models\LicenseType;
use yii\web\NotFoundHttpException;

class LicenseController extends \yii\web\Controller
{
    public function actionIndex()
    {
        if($model = LicenseType::find()->where(['deleted'=>0])->orderBy('order DESC')->all()){
            return $this->render('index', ['model'=>$model]);
        }
        return $this->goHome();
    }

    public function actionView($id){
        if($model = $this->findModel($id)){
            return $this->render('view', ['model'=>$model]);
        }
        return $this->goHome();
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LicenseType::find()->where(['id'=>$id, 'deleted'=>0])->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(\Yii::t('backend\training', 'The requested page does not exist.'));
        }
    }

}
