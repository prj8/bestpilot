<?php
namespace frontend\controllers;

use Yii;
//use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;
use yii\web\Controller,
    yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
//use common\models\LoginForm;
use common\models\Training;
use frontend\models\TrainingSearch;
//use frontend\models\PasswordResetRequestForm;
//use frontend\models\ResetPasswordForm;
//use frontend\models\SignupForm;
use frontend\models\SchoolContactForm,
    frontend\models\SchoolRates;

/**
 * Site controller
 */
class TrainingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new TrainingSearch();
        $trainingsDataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $md5 = md5(serialize($searchModel));
        if(\Yii::$app->request->post('md5', null) != null){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['md5'=>$md5];
        }

		return $this->render('index', [
            'searchModel' => $searchModel,
            'trainingsDataProvider' => $trainingsDataProvider,
            'md5'=>$md5
        ]);
    }

    /**
     * Displays a single Training model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if(!$model = $this->findModel($id)){
            $this->redirect(['/training/index']);
        }

        $ContactForm = new SchoolContactForm();
        if ($ContactForm->load(Yii::$app->request->post())) {
            if ($ContactForm->validate() && $ContactForm->sendEmail($model)) {
                Yii::$app->session->setFlash('success', Yii::t('frontend/training', 'Thank you for contacting us. We will respond to you as soon as possible.'));
                return $this->redirect(['training/view','id'=>$model->id]);
            } else {
                Yii::$app->session->setFlash('warning', Yii::t('frontend/training', 'There was an error sending email.'));
            }
            //return $this->refresh();
            //print_r($ContactForm);
        }



        $SchoolRatesForm = new SchoolRates();

        if(!\Yii::$app->user->isGuest){
            $SchoolRatesForm->user_id = \Yii::$app->user->id;
            $SchoolRatesForm->email = \Yii::$app->user->identity->email;

            $user = \Yii::$app->user->identity;
            if ($user->profile != null)
            {
                $SchoolRatesForm->name = $user->profile->username;
                $ContactForm->phone = $user->profile->phone;
                $ContactForm->name = $user->profile->username;
            }
            $ContactForm->email = \Yii::$app->user->identity->email;

        }

        $SchoolRatesForm->school_id = $model->school_id;
        $SchoolRatesForm->training_id = $model->id;

        if ($SchoolRatesForm->load(Yii::$app->request->post()) && $SchoolRatesForm->validate()) {
            if($SchoolRatesForm->save()){
                $SchoolRatesForm->text = '';
                $SchoolRatesForm->name = '';
                $SchoolRatesForm->email = '';
                Yii::$app->session->setFlash('success', Yii::t('frontend/training', 'Thank you for your review'));
                return $this->redirect(['training/view','id'=>$model->id]);
            }else
                Yii::$app->session->setFlash('warning', Yii::t('frontend/training', 'Error. Your review was not added.'));
        }

        //Yii::$app->session->setFlash('success', Yii::t('frontend/training', 'Thank you for your review'));

        $providerAircraftToTraining = new ArrayDataProvider([
            'allModels' => $model->aircraft,
        ]);

        return $this->render('view', [
            'model' => $model,
            'ContactForm' => $ContactForm,
            'providerAircraftToTraining' => $providerAircraftToTraining,
            'schoolRatesForm'=>$SchoolRatesForm
        ]);
    }

    /**
     * Finds the Training model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Training the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Training::find()->where(['training.id'=>$id, 'training.deleted'=>0])->joinWith('licenseType')->one()) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('frontend/training', 'The requested page does not exist.'));
        }
	}
}
