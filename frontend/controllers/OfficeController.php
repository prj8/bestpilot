<?php

namespace frontend\controllers;

use yii\filters\AccessControl;

class OfficeController extends \yii\web\Controller
{
    /**
     * @inheritdoc
     */

        /*
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'login'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
*/

    public function actionLogin()
    {

        $model = \Yii::$app->user->identity;

        if(!$profile = $model->profile){
            $profile = new \common\models\Profile();
            $profile->user_id = $model->id;
        }

        if ($model->load(\Yii::$app->request->post()) && $profile->load(\Yii::$app->request->post())) {
            $model->save();
            $profile->save();
        }


        return $this->render('index', compact('model', 'profile'));
    }


    public function actionIndex()
    {
        //exit();
        $model = \Yii::$app->user->identity;
        if(!$profile = $model->profile){
            $profile = new \common\models\Profile();
            $profile->user_id = $model->id;
        }

        if ($model->load(\Yii::$app->request->post()) && $profile->load(\Yii::$app->request->post())) {
            if($profile->save() && $model->save())
                \Yii::$app->session->setFlash('success', \Yii::t('pages', 'Data saved successfully'));
            else
                \Yii::$app->session->setFlash('error', \Yii::t('pages', 'Recording error'));
        }
        return $this->render('index', compact('model', 'profile'));
    }

}
