
function formatMoney(inum, n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = parseFloat(inum).toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
}

jQuery(document).ready(function () {

    main_w = 0;

    $(window).load( function(){
        main_w = $(window).width();
        setTimeout(function () {
            $(".se-pre-con").fadeOut(700, function () {
                $(this).remove();
            });
        }, 350);
    });


    $(document).on("pjax:end", function() {
        selectedStyle();
        wiIndwxResize();
    });

    $('body').on('click', '#termin-range span.date', function(){
        $('#termin-range span.date').removeClass('active');
        $(this).addClass('active');
    }).on('infinitescroll:afterRetrieve', function(){
       setTimeout(wiIndwxResize, 250);
    }).on('click', '.thumbnail>.close', function(){
        $(this).closest('.to_map').addClass('hidden');
    }).on("change", 'select[name="sorting"]', function(){
        var value = $(this).val();
        $(".tab-pane.active .hidden-sorting."+value).trigger("click");
    }).on("click", '.wrap > .alert button', function(){
        $('.wrap > .alert').slideUp(350, function() {
            $(this).remove();
        });
    });

    /*$('select').on('select2:open', function (evt) {
        setTimeout(function(){
            $('.select2-results').mCustomScrollbar({axis:"y", setHeight: 200});
        }, 1000);
    });*/

    $(window).on("resize", function(event){
        if(main_w == $(window).width()){
            event.preventDefault();
            return;
        }
        wiIndwxResize(true);
        main_w = $(window).width();
    });

    selectedStyle();
    setTimeout(wiIndwxResize23, 250);

    $(window).scroll(function() {
        if($('body.site.index').length == 0 && $(window).width() >= 900){
            if ($(this).scrollTop() > 50){
                $('.navbar-inverse.navbar-top').addClass('small-h');
            } else {
                $('.navbar-inverse.navbar-top').removeClass('small-h');
            }
        }

        wiIndwxResize23();
        setTimeout(wiIndwxResize23, 250);
    });

    $('.training-categories ul.LicenseType a').each(function(){
        var href = decodeURIComponent($(this).attr('href')) + '&ww='+$(window).width();
        $(this).attr('href', href);
    });

    var href = decodeURIComponent($('.link.back').attr('href')) + '?ww='+$(window).width();
    $('.link.back').attr('href', href);

    $('button.navbar-toggle').click(function () {
        $('button.navbar-toggle').toggleClass('collapsed');
        if($('button.navbar-toggle:not(.collapsed)').length==0){
            $(".mobile-menu").animate({left: 0}, 350);
        }else
            $(".mobile-menu").animate({left: '-100%'}, 350);
    });

    if($('[name="TrainingSearch[country_id]"]').length > 0){
        var value = $('[name="TrainingSearch[country_id]"]').val();
        if(value==''){
            $.getJSON('https://ipinfo.io', function(data){
                $('[name="TrainingSearch[country_id]"]').val(data.country).trigger("change");
                console.log(data.country);
            })
        }
    }

    $("form").on("afterValidate", function (event, messages) {
        $('.form-group span.icon-alert_triangle', this).remove();
        $('input[name="ww"]', this).val($(window).width());
        checkErrors();
    });

    wiIndwxResize();
    checkErrors();

    setTimeout(function(){
        wiIndwxResize();
    }, 500);

    setTimeout(function(){
        wiIndwxResize();
    }, 1500);
});

function checkErrors(){
    if($('.form-group.has-error:first p.help-block').length > 0){
        var message = $('.form-group.has-error:first p.help-block').text();
        var template = '<div id="message-w4" class="alert fade in kv-alert alert-warning">\
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                <h1 class="text-center success">'+message+'</h1>\
            </div>';
        $('.wrap').append(template);
        setTimeout(function(){
            $("#message-w4").slideUp(350, function() {
                $(this).remove();
            });
        }, 5000);

        if($('.form-group.has-error input[type="text"]').length > 0){
            $('.form-group.has-error input[type="text"]').each(function () {
                var container = $(this).closest('.form-group');
                if(container.find('span.icon-alert_triangle').length == 0){
                    container.prepend('<span class="icon-alert_triangle"></span>');
                }
            });
        }
    }

    if($('.has-error:first .help-block').length > 0 && message!=''){
        var message = $('.has-error:first .help-block').text();
        var template = '<div id="message-w41" class="alert fade in kv-alert alert-warning">\
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                <h1 class="text-center success">'+message+'</h1>\
            </div>';
        $('.wrap').append(template);
        setTimeout(function(){
            $("#message-w41").slideUp(350, function() {
                $(this).remove();
            });
        }, 5000);

    }
}

function wiIndwxResize(res){
    $(".select2-search__field").attr("readonly", "true");

    wiIndwxResize23();

    var matchHeightOP = {byRow: false};

    if(typeof res != 'undefined'){
        matchHeightOP['remove'] = true;
    }

    if(main_w != $(window).width()){
        if($(window).width() >= 600)
            $(".jumbotron").css("height", $(window).height() - $(".base-info").outerHeight());
        else
            $(".jumbotron").css({"min-height": $(window).height(), 'height': 'auto'});
    }

    $(".base-info > div > div").matchHeight();
    $(".first-row > div > .panel").matchHeight();
    $(".aircrafts > div > .item").matchHeight();
    $(".simulators > div > .item").matchHeight();

    $(".row").each(function(){
        $("> div.item.grid:not(.to_map) .thumbnail", this).matchHeight(matchHeightOP);
    });

    $(".clearfix.back").css('padding-right', 'auto');
    if($(window).width() >= 1300){
        $(".clearfix.back").css('padding-right', $('.navbar-nav.nav>li.login').outerWidth()+15);
    }

    if($('body.training.view').length > 0){
        if($(window).width() >= 768){
            $(".check_width").each(function() {
                $(this).css("width", $(this).data("sum")+"%");
            });
        }else{
            $(".check_width:not(.hidden-xs)").each(function() {
                minus = 0;
                $(this).closest(".clearfix").find(">.hidden-xs.check_width").each(function(){
                    minus += parseFloat($(this).data("sum"));
                });

                count1 = $(this).closest(".clearfix").find(".check_width").not('.hidden-xs').length;
                minus = count1 > 0 ? minus / count1 : minus;

                var width = $(this).data("sum");
                $(this).css("width", (parseFloat($(this).data("sum"))+minus)+"%");
            });
        }
    }

    /*$('.bg-grey.training-list .training-categories>div.form-group').css('width', 'auto');
    if($('.bg-grey.training-list .training-categories>div.form-group').length > 0) {
        if ($(window).width() <= 400) {
            whg = $(window).width() - $('ul.LicenseType > li:not(.hidden-xs):first').outerWidth();
            $('.bg-grey.training-list .training-categories>div.form-group').css('width', whg / 2 - 1);
        }
    }*/

    $(".first-row > div > .panel").each(function(){
        $('.panel-body', this).css('height', $(this).height() - $('.panel-footer', this).outerHeight() - $('.panel-heading', this).outerHeight());
    });
}

function selectedStyle(){
    $('.contact_form input[type="checkbox"]').styler();
}

function wiIndwxResize23(){
    var navbarfixedtop = 0;

    if($('body.site.index').length == 0){
        $(".navbar-fixed-top").each(function(i){
            if(i>0){
                $(this).css('top', navbarfixedtop);
            }
            navbarfixedtop += $(this).outerHeight();
        });
    }

    $(".first_margin").css({"margin-top": navbarfixedtop});
    $(".mobile-menu").css({"top": $(".navbar.navbar-fixed-top").outerHeight(), 'width': $(window).width()});

    if($(window).width() <= 768){
        $('body.index .base-info > div')
            .addClass('owl-carousel owl-theme')
            .removeClass('row')
            .owlCarousel(
                    {
                        items:1,
                        responsive : {
                            768 : {
                                items:2
                            }
                        }
                    }
                );
    }else{
        $('body.index .base-info > div').removeClass('owl-carousel owl-theme').addClass('row').owlCarousel('destroy');
    }
}