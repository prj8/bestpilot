<?php

namespace frontend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LicenseType,
    backend\models\base\Currency,
    common\models\Country,
    common\models\base\TrainingStartDates,
    yii\helpers\ArrayHelper,
    common\models\Aircraft,
    imanilchaudhari\CurrencyConverter\CurrencyConverter;

/**
 * common\models\TrainingSearch represents the model behind the search form about `common\models\Training`.
 */
 class TrainingSearch extends Training
{


    public $aircraftType=null;
	private $_dateRange = null;
	private $_hasElearning=3;
	private $_aircraftAge=null;
    public $country_id;
    public $license_id2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_id', 'license_id', 'training_type_id', 'theory_hours', 'elearning_hours', 'aircraft_hours', 'simulator_hours', 'lock', 'created_by', 'updated_by', 'deleted_by', 'created', 'modified', 'deleted', 'aircraftAge', 'license_id2'], 'integer'],
            [['name', 'address_theory', 'theory_type', 'address_practice', 'remarks', 'created_at', 'updated_at', 'deleted_at', 'aircraftType', 'dateRange'], 'safe'],
            [['price'], 'number'],
            ['lang', 'string'],
			[['hasElearning'], 'integer'],
            ['country_id', 'string', 'max' => 3],
            ['country_id', 'exist', 'skipOnError' => true, 'targetClass' => \backend\models\Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['license_id'], 'required', 'message'=>\Yii::t('training/search', "Choose license type")],
            [['country_id'], 'required', 'message'=>\Yii::t('training/search', "Choose country")]
        ];
    }

     /**
      * @inheritdoc
      */
     public function attributeLabels()
     {
         return [
             'dateRange' => \Yii::t('frontend/training', 'Termin'),
             'aircraftType' => \Yii::t('frontend/training', 'aircraftType'),
             'aircraftAge' => \Yii::t('frontend/training', 'aircraftAge'),
             'price' => \Yii::t('frontend/training', 'Price'),
             'school_rate' => \Yii::t('frontend/training', 'School rate'),
         ];
     }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDateRange()
    {

        return $this->_dateRange;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setDateRange($param)
    {
		$this->_dateRange = $param;

    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHasElearning()
    {
        return $this->_hasElearning;
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function setHasElearning($param)
    {
		$this->_hasElearning = $param;
		
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAircraftAge()
    {
        return $this->_aircraftAge;
    }
	
    /**
     * @return \yii\db\ActiveQuery
     */
    public function setAircraftAge($param)
    {
		$this->_aircraftAge = $param;
		
    }


     /**
      * @return \yii\db\ActiveQuery
      */

     public function allLanguages($where)
     {
         $langs = [];
         $tmp = $where != null ? self::find()->select('lang')->joinWith('school')->where('lang != ""')->andWhere($where) : self::find()->select('lang')->where('lang != ""');
         foreach ($tmp->each() as $lang){
             $langs = array_merge($langs, $lang->lang);
         }

         return \lajax\translatemanager\models\Language::find()->where(['or', ['language_id'=>array_unique($langs)], ['language'=>array_unique($langs)]])->asArray()->all();
     }


    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {

        $converter = new CurrencyConverter();

        if ($this->license_id==7) {
            $this->HasElearning(null);
        }

		/*
		
			AVG powoduje pobranie tylko jednego wiersza - trzeba zrobić AVG na grupie z joina czy jakoś tak.
		*/
        $query = Training::find()
                                ->select(['`training`.*', '(SELECT training_start_dates.start_date FROM training_start_dates WHERE training_start_dates.training_id=training.id AND training_start_dates.start_date >= NOW() ORDER BY training_start_dates.start_date ASC LIMIT 1) as start_date_echo', '(SELECT AVG(school_rates.rate) FROM school_rates WHERE school_rates.school_id=training.school_id AND school_rates.published=1 AND training.school_id IS NOT NULL) as school_rate'])
                                ->joinWith('aircraft')
                                ->joinWith('simulator')
                                ->joinWith(['addressPractice', 'addressTheory at'])
                                ->joinWith('startDates')
                                ->joinWith('school');

        $dataProvider = new ActiveDataProvider([
            'query' => $query->distinct(),
			'sort'=>[
                'attributes'=>[
                        'price'=>['label'=>\Yii::t('frontend/training', 'Price')],
                        'school_rate'=>['label'=>\Yii::t('frontend/training', 'School rate')],
                        'start_date'=>[
                            'asc' => ['start_date_echo' => SORT_ASC],
                            'desc' => ['start_date_echo' => SORT_DESC],
                            'label'=>\Yii::t('frontend/training', 'Start date')
                        ],
                ],
                'defaultOrder' => ['price'=>SORT_ASC]
            ],
            'pagination' => [
                'pageSize' => 12,
            ],
        ]);

        $this->load($params);

        if(empty($this->license_id) || is_null($this->license_id)){
            $this->license_id = LicenseType::find()->select('id')->asArray()->one()['id'];
        }

        if(empty($this->country_id) || is_null($this->country_id)){
            $city = \Yii::$app->request->getCity();
            $this->country_id = isset($city['country']['iso']) && Country::find()->where(['id'=>$city['country']['iso']])->count() > 0 ? $city['country']['iso'] : Country::find()->select('id')->asArray()->one()['id'];
        }

        $currency = Currency::find()->where(['id'=>\Yii::$app->params['currency']])->one();
        $country = Country::find()->where(['id'=>$this->country_id])->one();
        \Yii::$app->params['currency_symbol'] = ArrayHelper::getValue($currency, 'symbol');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $this->license_id2 = $this->license_id;

        $query->andFilterWhere([
            'training.school_id' => $this->school_id,
            'training.license_id' => $this->license_id,
            'school.deleted' => 0,
            'school.active'=>1,
            'training.deleted' => 0,
            'training.active' => 1,
            'training_type_id' => $this->training_type_id,
            'school.country_id' => $this->country_id,
        ]);


        if(!empty($this->lang)){
            $query->andFilterWhere(['like', 'training.lang', $this->lang]);
        }
		
		//Yii::trace($this->dateRange, __METHOD__);
		if ($this->dateRange!=NULL && isset($this->dateRange['start']) && isset($this->dateRange['end']) && !empty($this->dateRange['start']) && !empty($this->dateRange['end'])) {
            $training_id = ArrayHelper::map(TrainingStartDates::find()->select(['training_id'])->where(['and', ['>=', 'start_date', date('Y-m-d')], ['>=', 'start_date', \DateTime::createFromFormat('Y-m-d', $this->dateRange['start'])->format('Y-m-d')], ['<=', 'start_date', \DateTime::createFromFormat('Y-m-d', $this->dateRange['end'])->format('Y-m-d')]])->all(), 'training_id', 'training_id');
            $training_id2 = ArrayHelper::map(Training::find()->select(['training.id'])->joinWith('startDates')->where(['training_start_dates.start_date'=>null])->all(), 'id', 'id');

            $query->andFilterWhere([
			    'or',
                ['in', 'training.id', array_values($training_id)],
                ['in', 'training.id', array_values($training_id2)]
            ]);
		}

        \Yii::$app->params['Aircrafts'] = [];
        $Aircrafts1 = Aircraft::find()->joinWith(['school'])->joinWith(['trainings'])->joinWith(['aircraftType'])
            ->where(['training.license_id'=>$this->license_id, 'training.deleted'=>0, 'school.country_id'=>$this->country_id, 'school.deleted'=>0])
            ->groupBy('aircraft.type_id')
            ->all();

        $Aircrafts = [];
        foreach ($Aircrafts1 as $aircraft){
            $Aircrafts[$aircraft->type_id] = $aircraft->aircraftType->name;
        }

        asort($Aircrafts);
        \Yii::$app->params['Aircrafts'] = $Aircrafts;


        /* Tryb szkolenia */

        $data = [];
        if ($this->license_id!=7) {
            if (count(\Yii::$app->params['Aircrafts']) > 0) {
                $count = Training::find()->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.training_type_id' => 3, 'training.license_id' => $this->license_id, 'school.country_id' => $this->country_id, 'school.deleted' => 0, 'school.active' => 1])->count();
                if ($count > 0)
                    $data[3] = \Yii::t('common/training', 'Teoria+Praktyka');

                $count = Training::find()->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.training_type_id' => 2, 'training.license_id' => $this->license_id, 'school.country_id' => $this->country_id, 'school.deleted' => 0, 'school.active' => 1])->count();
                if ($count > 0)
                    $data[2] = \Yii::t('common/training', 'Praktyka');
            }

            $count = Training::find()->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.training_type_id' => 1, 'training.license_id' => $this->license_id, 'school.country_id' => $this->country_id, 'school.deleted' => 0, 'school.active' => 1])->count();
            if ($count > 0)
                $data[1] = \Yii::t('common/training', 'Teoria');

            krsort($data);
            \Yii::$app->params['hasElearning'] = $data;

        } else {
            \Yii::$app->params['hasElearning'] = array();
        }


        if(!isset($this->hasElearning) || empty($this->hasElearning) || !isset($data[$this->hasElearning])){
            $this->hasElearning = key($data);
        }

        $minFilterPrice = Training::find()->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.license_id'=>$this->license_id, 'school.country_id' => $this->country_id, 'school.deleted' => 0, 'school.active'=>1]);
        $maxFilterPrice = Training::find()->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.license_id'=>$this->license_id, 'school.country_id' => $this->country_id, 'school.deleted' => 0, 'school.active'=>1]);

        if(count(\Yii::$app->params['hasElearning']) > 1){
            if(in_array($this->hasElearning, [1,2,3])){

                //TODO: filterPrice -> chyba trzeba zrobić odświeżanie filtrów.
                $query->andFilterWhere(['training.training_type_id'=>$this->hasElearning]);
                //$minFilterPrice->andWhere(['training.training_type_id'=>$this->hasElearning]);
                //$maxFilterPrice->andWhere(['training.training_type_id'=>$this->hasElearning]);
            }

            if($this->hasElearning!=1 && is_array($this->aircraftType)){
                $query->andFilterWhere(['in', 'aircraft.type_id', $this->aircraftType]);
                $query->andFilterWhere(['aircraft.deleted' => 0]);
            }
        }

        $minFilterPrice = intval($minFilterPrice->min('training.price'));
        $maxFilterPrice = intval($maxFilterPrice->max('training.price'));

        if($country->currency_id!=\Yii::$app->params['currency']){
            $minFilterPrice = $converter->convert($country->currency_id, \Yii::$app->params['currency'], $minFilterPrice);
            $maxFilterPrice = $converter->convert($country->currency_id, \Yii::$app->params['currency'], $maxFilterPrice);
        }

        \Yii::$app->params['minFilterPrice'] = $minFilterPrice;
        \Yii::$app->params['maxFilterPrice'] = $maxFilterPrice;

        if(empty($this->price))
            $this->price = \Yii::$app->params['maxFilterPrice'];

        if($this->price > 0 && $this->price >= \Yii::$app->params['minFilterPrice']){
            $ratio = 1;
            if($country->currency_id!=\Yii::$app->params['currency']){
                $ratio = $converter->convert($country->currency_id, \Yii::$app->params['currency'], 1);
            }
            $query->andFilterWhere(['<=', 'training.price', $this->price/$ratio]);
        }

        \Yii::$app->params['maxAircraftAge'] = date('Y') - intval(\common\models\Aircraft::find()->joinWith('trainings')->joinWith('school')->where(['training.active' => 1, 'training.deleted' => 0, 'training.license_id'=>$this->license_id, 'school.country_id' =>$this->country_id])->andWhere(['>', 'aircraft.aircraft_year', 0])->min('aircraft.aircraft_year'));
        if(\Yii::$app->params['maxAircraftAge'] > 80){
            \Yii::$app->params['maxAircraftAge'] = 0;
        }

        if($this->hasElearning!=1 && empty($this->aircraftAge)){
            $this->aircraftAge = \Yii::$app->params['maxAircraftAge'];
        }

        if ($this->hasElearning!=1 && $this->aircraftAge > 0 && count(\Yii::$app->params['Aircrafts']) > 0 && $this->aircraftAge<=\Yii::$app->params['maxAircraftAge']){
			$query->andFilterWhere(['or', ['>=', 'aircraft.aircraft_year', date('Y')-$this->aircraftAge], 'aircraft.aircraft_year IS NULL', 'aircraft.aircraft_year=""', 'aircraft.aircraft_year=0']);
            $query->andFilterWhere(['aircraft.deleted' => 0]);
		}

		$query->notDeleted();
		$query->active();

        //print_r($query);



        return $dataProvider;
    }
}
