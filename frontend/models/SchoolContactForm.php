<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class SchoolContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $regulamin;
    public $offers;
    public $personal_data;
    public $body;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email'/*, 'personal_data'*/], 'required', 'message' => Yii::t('contact_form', 'Field {attribute} can not be empty')],
            // email has to be a valid email address
            ['email', 'email', 'message' => Yii::t('contact_form', 'Proszę podać prawidłowy adres e-mail.')],
            //[['personal_data'], 'compare', 'operator' => '==', 'compareValue' => true, 'message' => Yii::t('contact_form', 'Agree to the processing personal data should be checked')],
            [['regulamin'], 'compare', 'operator' => '==', 'compareValue' => true, 'message' => Yii::t('contact_form', 'Agree to the terms of the Regulations should be checked')],
            [['offers'], 'compare', 'operator' => '=='],
            ['phone', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('contact_form', 'Imię i Nazwisko'),
            'email' => Yii::t('contact_form', 'e-mail'),
            'phone' => Yii::t('contact_form', 'nr telefonu'),
            'body' => Yii::t('contact_form', 'treść wiadomości'),
            'regulamin' => Yii::t('contact_form', 'Zgadzam się na warunki zawarte w Regulaminie.'),
            'offers' => Yii::t('contact_form', 'Chcę otrzymywać od BestPilot.com informacje o promocyjnych szkoleniach i zniżkach.'),
            'personal_data' => Yii::t('contact_form', 'Wyrażam zgodę na przetwarzanie moich danych osobowych przez firmę BestPilot Sp. z o.o. w celach marketingowych.')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($trainingModel)
    {

        Yii::$app->mailer->compose(
            ['html' => 'confirm-training-html', 'text' => 'confirm-training-text'],
            ['model' => $trainingModel, 'user'=>$this]
        )
            ->setTo([$this->email => $this->name])
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setReplyTo(\Yii::$app->params['adminEmail'])
            ->setSubject(Yii::t('training_form', 'Sign up for training'))
            ->send();

        return Yii::$app->mailer->compose(
            ['html' => 'admin-training-html', 'text' => 'admin-training-text'],
            ['model' => $trainingModel, 'user'=>$this]
        )
            ->setTo(\Yii::$app->params['adminEmail'])
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setReplyTo([$this->email => $this->name])
            ->setSubject(Yii::t('training_form', 'User has signed up for training'))
            ->send();
    }
}
