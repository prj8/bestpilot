<?php

namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $phone;
    public $regulamin;
    //public $personal_data;
    public $body;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'body'], 'required', 'message' => Yii::t('contact_form', 'Field {attribute} can not be empty')],
            // email has to be a valid email address
            ['email', 'email', 'message' => Yii::t('contact_form', 'Check {attribute} field is correct')],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('contact_form', 'Imię i nazwisko'),
            'email' => Yii::t('contact_form', 'Adres e-mail'),
            'phone' => Yii::t('contact_form', 'Nr telefonu'),
            'body' => Yii::t('contact_form', 'Treść wiadomości'),
            'regulamin' => Yii::t('contact_form', 'Zgadzam się na warunki zawarte w Regulaminie.'),
            //'personal_data' => Yii::t('contact_form', 'Wyrażam zgodę na przetwarzanie moich danych osobowych przez firmę BestPilot Sp. z o.o. w celach marketingowych.')
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail($model)
    {
        return Yii::$app->mailer->compose()
            ->setTo(\Yii::$app->params['adminEmail'])
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setReplyTo([$this->email => $this->name])
            ->setSubject(Yii::t('contact_form', 'From contact form'))
            ->setTextBody($this->body)
            ->send();
    }
}
