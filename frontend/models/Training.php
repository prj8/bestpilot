<?php

namespace frontend\models;

use common\models\base\TrainingStartDates;

class Training extends \common\models\Training{
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartDates()
    {
        return $this->hasMany(TrainingStartDates::className(), ['training_id' => 'id'])->orderBy(['training_start_dates.start_date'=>'ASC']);
    }


}