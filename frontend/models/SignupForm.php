<?php
namespace frontend\models;

use yii\base\Model,
    yii;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required', 'message'=>Yii::t('Signup', "Field {attribute} can not be empty")],
            ['email', 'email', 'message' => Yii::t('Signup', 'Check {attribute} field is correct')],
            ['email', 'string', 'max' => 255, 'message' => Yii::t('Signup', 'Field {attribute} can not be longer than 255 chars')],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => Yii::t('Signup', 'This email address has already been taken.')],

            ['password', 'required', 'message'=>Yii::t('Signup', "Field {attribute} can not be empty")],
            ['password', 'string', 'min' => 6, 'message' => Yii::t('Signup', 'Field {attribute} can not be longer than 6 chars')],
            ['password_repeat', 'required', 'message'=>Yii::t('Signup', "Field {attribute} can not be empty")],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message'=>Yii::t('Signup', "Passwords don't match")],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => Yii::t('Signup', 'Hasło'),
            'password_repeat' => Yii::t('Signup', 'Powtórz hasło'),
            'email' => Yii::t('Signup', 'Email')
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateConfirmationToken();

        if ($user->save()) {

            $mail = Yii::$app
                ->mailer
                ->compose(
                    ['html' => 'registration-html', 'text' => 'registration-text'],
                    ['user' => $user]
                )
                ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name])
                ->setTo($this->email)
                ->setSubject(Yii::t('mail/registration', 'Thank you for registration in ') .' '. Yii::$app->name)
                ->send();
            Yii::trace(serialize($mail));
            return $user;
        } else {
            return null;
        }
    }
}
