<?php

namespace frontend\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;

/**
 * This is the base model class for table "school_rates".
 *
 * @property integer $id
 * @property integer $rate
 * @property integer $school_id
 * @property integer $lock
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property integer $created
 * @property integer $modified
 * @property integer $deleted
 *
 * @property \backend\models\School $school
 */
class SchoolRates extends \common\models\SchoolRates{
    /**
     * @return int
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->sendEmail();
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     *
     * @param string $email the target email address
     * @return boolean whether the email was sent
     */
    public function sendEmail()
    {
        return Yii::$app->mailer->compose()
            ->setTo(\Yii::$app->params['adminEmail'])
            ->setFrom(\Yii::$app->params['supportEmail'])
            ->setSubject(Yii::t('reviews', 'New review'))
            ->setReplyTo([$this->email => $this->name])
            ->setTextBody(implode("\n\n", ['From: '.$this->email . ' <'.$this->name.'>', 'Rate: '.$this->rate, 'Text: '.$this->text]))
            ->send();
    }
}
