<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&subset=latin-ext',
        'css/fontello-embedded.css',
        'css/jquery.formstyler.css',
        'css/owl.carousel.css',
        'css/owl.theme.default.css',
        'css/style.css?3',
    ];

    public $js = [
    	'js/jquery.matchHeight-min.js',
        //'js/jquery.mCustomScrollbar.min.js',
        '//maps.googleapis.com/maps/api/js?key=AIzaSyAm4GRRg-cF2R7F0EqcA6YsPRsAXtmdj6A&sensor=false&extension=.js',
        'js/google.map.js',
        'js/jquery.formstyler.min.js',
        'js/owl.carousel.min.js',
        'js/functions.js?6',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
