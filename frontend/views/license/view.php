<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = $model->license;
$Country = backend\models\Country::find()->orderBy('country')->all();
$countryID = @Yii::$app->params['country_id'];
if ($countryID==NULL || $countryID=='') $countryID = 'PL';
?>
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-sm-7 contact_form">
                <h1 class="text-blue"><?= Html::encode($model->license) ?></h1>
                <?= $model->ttext ?>
                <p>&nbsp;</p>
                <p><?= Html::a(\Yii::t('license/view', 'Znajdź szkolenie'), ['training/index', 'TrainingSearch[license_id]'=>$model->id, 'TrainingSearch[country_id]'=>$countryID], ['class'=>'text-uppercase btn btn-primary']) ?>
                </p>
            </div>
        </div>

    </div>