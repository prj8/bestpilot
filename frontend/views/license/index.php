<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('pages', 'Wszystkie szkolenia');
?>

<div class="info-data">
    <div class="container">
        <div class="clearfix">
            <?= Html::tag('h1', $this->title, ['class'=>'pull-left']) ?>
        </div>
        <div class="row first-row">
            <?php foreach ($model as $key => $value): ?>
                <div class="col-lg-4 col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading clearfix"><?= Html::tag('h3', $value->license) ?></div>
                        <div class="panel-body">
                            <?= $value->tLicenceDescription ?>
                        </div>
                        <div class="panel-footer text-center"><?= Html::a(\Yii::t('site/index', 'Szczegóły'), ['license/view', 'id'=>$value->id], ['class'=>'text-uppercase btn btn-primary']) ?></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
