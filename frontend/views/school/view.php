<?php

use yii\helpers\Html,
    yii\helpers\Url,
    yii\bootstrap\Tabs;
use yii\helpers\HtmlPurifier;
use yii\widgets\Pjax,
    yii\data\ArrayDataProvider,
    yii\widgets\ListView;

if(isset($model->aircrafts) && count($model->aircrafts) > 0)
    $trainingsDataProvider = new ArrayDataProvider([
        'allModels' => $model->trainings,
        'sort'=>[
            'attributes'=>[
                'price'=>['label'=>\Yii::t('frontend/training', 'Price')],
                'school_rate'=>['label'=>\Yii::t('frontend/training', 'School rate')],
            ],
        ],
        'pagination' => [
            'pageSize' => 15,
        ],
    ]);

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

$this->title = Yii::t('school/view', '{name}', ['name'=>$model->name]);


$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
$swissNumberProto = $phoneUtil->parse($model->phone, $model->country_id);
$model->phone = $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);

Yii::$app->params['country_id'] = $model->country_id;
?>

<div class="container-fluid">
    <div class="row bg-white">
        <div class="container training-details">
            <div class="row">
                <?php if(!empty($model->logo)): ?>
                    <div class="col-lg-2 col-sm-2 col-xs-3 logo-cnt">
                        <img src="<?= $model->LogoResize(200) ?>" />
                    </div>
                <?php endif; ?>
                <div class="<?= empty($model->logo)?'col-lg-5 col-sm-8 col-xs-9':'col-lg-3 col-sm-6' ?>">
                    <h2 class="big text-blue school-title"><?= $model->name ?></h2>
                    <div class="school-ratings">
                    <span class="star-ratings-sprite">
                        <span style="width:<?= $model->school_rate*100/5 ?>%" class="star-ratings-sprite-rating"></span>
                    </span>
                        <?= number_format(floatval($model->school_rate), 1, '.', ''); ?>
                    </div>
                    <hr />
                    <div class="licence-descriptions">
                        <?= $model->description ?>
                    </div>
                </div>
                <div class="col-lg-3 col-lg-offset-2 col-sm-4 phone-details">
                    <span><?= Yii::t('school/view', 'Zadzwoń'); ?></span>
                    <a href="tel:<?= trim($model->phone) ?>" class="btn btn-default btn-lg btn-block phone"><?= trim($model->phone) ?></a>
                    <hr />
                    <div>
                        <p><strong><?= $model->name ?></strong></p>
                        <p><?= $model->street ?> <?= $model->street_no ?><br /><?= $model->post_code ?> <?= $model->city ?>, <?= $model->country->ttitle ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="training-view clearfix">
    <?php
    $items = [];

    if(isset($model->trainings) && count($model->trainings) > 0) {
        $items[] = [
            'label' => Yii::t('school/view', 'Szkolenia'),
            'active' => true,
            'content' => $this->render('view/trainings', ['trainings' => $model->trainings]),
        ];
    }

    if(isset($model->aircrafts) && count($model->aircrafts) > 0){
        $items[] = [
            'label' => Yii::t('school/view', 'Dostępne samoloty'),
            'content' => $this->render('/training/view/Available_aircraft', ['aircrafts'=>$model->aircrafts]),
        ];
    }

    if(isset($model->simulators) && count($model->simulators) > 0){
        $items[] = [
            'label' => Yii::t('school/view', 'Dostępne symulatory'),
            'content' => $this->render('/training/view/Available_simulator', ['simulators'=>$model->simulators]),
        ];
    }

    $mapData = ['model'=>$model, 'markers'=>[['coordinates'=>$model->coordinates, 'title'=>implode(', ', array_filter([$model->street.$model->street_no, $model->post_code.' '.$model->city]))]]];

    $items['map'] = [
        'label' => Yii::t('training/view', 'Mapa'),
        'content' => $this->render('/training/view/Map', $mapData),
        'options' => ['id'=>'view_map']
    ];

    $items['schoolRates'] = [
        'label' => isset($model->schoolRates) && count($model->schoolRates) > 0 ? Yii::t('school-rates', 'Opinie ({num})', ['num'=>count($model->schoolRates)]) : Yii::t('training/view', 'Opinie'),
        'content' => $this->render('@app/views/training/view/Rates', ['schoolRates'=>$model->schoolRates, 'schoolRatesForm'=>$schoolRatesForm]),
    ];


    echo Tabs::widget([
        'options'=>['class'=>'container hidden-xs'],
        'items' => $items,
        'encodeLabels' => false
    ]);


    $items['map'] = [
        'label' => Yii::t('training/view', 'Mapa'),
        'content' => $this->render('/training/view/Map2', $mapData),
        'options' => ['id'=>'view_map2']
    ];


    unset($items['schoolRates']);

    echo \yii\bootstrap\Collapse::widget([
        'options'=>['class'=>'visible-xs'],
        'items' => $items,
        'encodeLabels' => false,
        'id' => 'myCollapsible'
    ]);

    ?>
</div>

<?php
    $this->registerJs('$(\'#list-panel\').tab(\'show\')');