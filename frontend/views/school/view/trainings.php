<?php

use yii\widgets\Pjax,
    yii\helpers\Url,
    yii\data\ArrayDataProvider,
    yii\widgets\ListView,
    yii\helpers\Html;

    $trainingsDataProvider = new ArrayDataProvider([
                                        'allModels' => $trainings,
                                        'sort'=>[
                                            'attributes'=>[
                                                'price'=>['label'=>\Yii::t('frontend/training', 'Price')],
                                                //'school_rate'=>['label'=>\Yii::t('frontend/training', 'School rate')],
                                            ],
                                            'defaultOrder' => ['price'=>SORT_ASC]
                                        ],
                                        'pagination' => [
                                            'pageSize' => 12,
                                        ],
                                   ]);

?>

<?php
$sort = [];

foreach ($trainingsDataProvider->sort->attributes as $k => $attribute){
    $sort[$k] = Yii::t('widok', '{title} (rosnąco)', ['title'=>$attribute['label']]);
    $sort['-'.$k] = Yii::t('widok', '{title} (malejąco)', ['title'=>$attribute['label']]);
}
$sorting = \Yii::$app->request->get('sort', 'price');
?>

<div class="not-padding container">
    <div class="sorting visible-xs"><?= Html::dropDownList('sorting', $sorting, $sort) ?></div>
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#grid-panel" aria-controls="grid" role="tab" data-toggle="tab">
                <i class="icon-siatka ico"></i><?= Yii::t('widok', 'Siatka'); ?>
            </a>
        </li>
        <li role="presentation" class="hidden-xs">
            <a href="#list-panel" aria-controls="list" role="tab" data-toggle="tab">
                <i class="icon-lista ico"></i><?= Yii::t('widok', 'Lista'); ?>
            </a>
        </li>
    </ul>
</div>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="grid-panel">
            <div class="container">
                <div class="hidden hidden-sorting">
                    <?php
                    foreach ($sort as $k=>$sort_details){
                        echo Html::a($k, Url::current(['sort' => $k]), ['class'=>'hidden-sorting '.$k]);
                    }
                    ?>
                </div>
                <div class="row">
                    <?php Pjax::begin(['id' => 'gridData', 'timeout'=>5000]) ?>
                    <?php echo ListView::widget([
                        'dataProvider' => $trainingsDataProvider,
                        'itemView' => '_trainingGrid',
                        'itemOptions' => ['class'=>'col-lg-4 col-sm-6 col-md-4 item grid'],
                        'options' => ['class'=>'col-lg-12'],
                        'layout' => '<div class="row"><div class="col-lg-12 sorting hidden-xs">'.Yii::t('widok', 'Sortowanie:').' {sorter}</div></div><div class="row">{items}</div><div class="row"><div class="col-lg-12">{pager}</div></div>',
                    ]);
                    ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="list-panel">
            <div class="container">
                <div class="hidden hidden-sorting">
                    <?php
                    foreach ($sort as $k=>$sort_details){
                        echo Html::a($k, Url::current(['sort' => $k]), ['class'=>'hidden-sorting '.$k]);
                    }
                    ?>
                </div>
                <div class="row">
                    <?php Pjax::begin(['id' => 'listData', 'timeout'=>5000]) ?>
                    <?php echo ListView::widget([
                        'dataProvider' => $trainingsDataProvider,
                        'itemView' => '_trainingList',
                        'itemOptions' => ['class'=>'row item list'],
                        'options' => ['class'=>'col-lg-12'],
                        'layout' => '<div class="row"><div class="col-lg-12 sorting hidden-xs">'.Yii::t('widok', 'Sortowanie:').' {sorter}</div></div><div class="row"><div class="col-lg-12">{items}</div></div><div class="row"><div class="col-lg-12">{pager}</div></div>',
                    ]);
                    ?>
                    <?php Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>