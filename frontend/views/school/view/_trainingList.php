<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier,
	imanilchaudhari\CurrencyConverter\CurrencyConverter;

	$converter = new CurrencyConverter();
?>


<div class="col-lg-12">
	<div class="row thumbail bg-white">
		<div class="col-lg-2 hidden-sm">
			<h2 class="text-center"><?= HtmlPurifier::process($model->licenseType->license) ?></h2>
		</div>
		<div class="col-lg-3 col-sm-4">
			<table class="details">
				<tr>
					<th>
						<?= Yii::t('training/view', 'Termin:'); ?>
					</th>
					<td>
						<?php
						if(is_array($model->startDates)){
							$date = null;
							foreach ($model->startDates as $termin){
								if($termin->start_date>=date('Y-m-d')){
									$date = $termin;
									break;
								}
							}

							echo is_object($date) ? $date->start_text : '<a href="javascript:;" class="to-contact">'.Yii::t('training/view', 'Contact us').'</a>';
						}else{
							echo Yii::t('training/view', 'od zaraz');
						}
						?>
					</td>
				</tr>

				<tr>
					<th>
						<?= Yii::t('training/view', 'Miasto:'); ?>
					</th>
					<td>
						<span><?= HtmlPurifier::process($model->address_practice_id != 0 ? $model->addressPractice->city : ($model->address_theory_id != 0 ? $model->addressTheory->city : '-')) ?></span>&nbsp;
					</td>
				</tr>

				<?php if(($model->elearning_hours + $model->theory_hours) > 0): ?>
					<tr>
						<th>
							<?= Yii::t('training/view', 'Teoria:'); ?>
						</th>
						<td>
							<?= ($model->elearning_hours + $model->theory_hours) ?>h
						</td>
					</tr>
				<?php endif; ?>

				<?php if(($model->aircraft_hours + $model->simulator_hours) > 0): ?>
					<tr>
						<th>
							<?= Yii::t('training/view', 'Praktyka:'); ?>
						</th>
						<td>
							<?= ($model->aircraft_hours + $model->simulator_hours) ?>h
						</td>
					</tr>
				<?php endif; ?>

			</table>
		</div>
		<div class="col-lg-4 col-sm-5">
			<?= $model->licenseType->description ?>
		</div>
		<div class="col-lg-2 col-sm-3 text-right">
			<h2 class="text-nowrap"><?= number_format(($model->school->country->currency_id!=Yii::$app->params['currency'] ? $converter->convert($model->school->country->currency_id, Yii::$app->params['currency'], $model->price) : $model->price), 0, '.', '<span class="numspan"></span>') ?><span><?= Yii::$app->params['currency'] ?></span></h2>
			<?= Html::a('Szczegóły', ['training/view', 'id' => $model->id], ['class' => 'btn btn-primary text-uppercase']) ?>
		</div>
	</div>
</div>
