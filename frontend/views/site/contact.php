<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('pages', 'Kontakt');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-5 col-sm-7 contact_form">
            <h1 class="text-blue"><?= Html::encode($this->title) ?></h1>

            <h3 class="text-blue">BestPilot</h3>
            <p class="text-blue address"><?= Yii::t('Kontakt', 'al. Stanów Zjednoczonych 51/620') ?> <br /><?= Yii::t('Kontakt', '04-028 Warsaw') ?></p>
            <p class="text-blue"><?= Yii::t('Kontakt', 'VAT-EU: {number}', ['number'=>'5441426877']) ?><br /><?= Yii::t('Kontakt', 'Commercial Registry No.: {number}', ['number'=>'147066893']) ?></p>

            <div class="visible-xs mobile-buttons-call">
                <a href="tel:+48570920787" class="btn btn-default btn-lg btn-block phone">+48 570 920 787</a>
                <a href="mailto:office@bestpilot.com" class="btn btn-default btn-lg btn-block phone">office@bestpilot.com</a>
                <a href="https://www.facebook.com/bestpilotcom/" class="btn btn-default btn-lg btn-block phone" target="_blank"><span class="icon-facebook"></span> Facebook</a>
            </div>

            <?php $form = ActiveForm::begin(['id' => 'contact-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>

                <?= Html::tag('h3', Yii::t('page', 'Masz pytanie?'), ['class'=>'text-blue']) ?>

                <?= $form->field($model, 'name', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('name'), 'class'=>'input-lg form-control']])
                    ->label(false)
                    ->textInput() ?>

                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('email'), 'class'=>'input-lg form-control']])
                    ->label(false)
                    ->textInput() ?>

                <?= $form->field($model, 'body', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('body'), 'class'=>'input-lg form-control']])
                    ->label(false)
                    ->textarea(['rows' => 6]) ?>

                <div class="fo rm-group">
                    <?= Html::submitButton(Yii::t('pages', 'Wyślij'), ['class' => 'btn btn-primary btn-lg', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-lg-3 col-sm-5 col-lg-offset-2 phone-details contact_form hidden-xs">
            <a href="tel:+48570920787" class="btn btn-default btn-lg btn-block phone">+48 570 920 787</a>
            <a href="mailto:office@bestpilot.com" class="btn btn-default btn-lg btn-block phone">office@bestpilot.com</a>
            <a href="https://www.facebook.com/bestpilotcom/" class="btn btn-default btn-lg btn-block phone"><span class="icon-facebook"></span> Facebook</a>
        </div>
    </div>

</div>
<div id="mapa-div" style="height:500px; padding:0px;"></div>

<?php

$this->registerJs('    
    function mapBaloon(){
          var contentString = \'<div id="content">\'+
              \'<div id="bodyContent" style="padding:15px;">\'+
                  \'<h3 class="text-blue">BestPilot</h3><br />\'+
                  \'<p class="text-blue">al. Stanów Zjednoczonych 51/620<br />\'+
                  \'04-028 Warsaw, Poland</p>\'+
              \'</div>\'+
          \'</div>\';
          
          /*var geocoder = new google.maps.Geocoder();          
          var address = $("p.address").text();
          geocoder.geocode({\'address\': address}, function(results, status) {
                if (status === \'OK\') {
                    googlemap.setCenter(results[0].geometry.location);                    
                    var infowindow = new google.maps.InfoWindow({
                        content: contentString,
                        maxWidth: 350,
                        position: results[0].geometry.location,
                    });                    
                    infowindow.open(googlemap);                     
                    google.maps.event.addListener(googlemap, \'click\', function() {
                        return false;
                    });
                }
                
                console.log(results);
          });*/
          
          googlemap.setCenter(new google.maps.LatLng(52.237455,21.079164));                    
          var infowindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 350,
              position: new google.maps.LatLng(52.237455,21.079164),
          });                    
          infowindow.open(googlemap);                     
          google.maps.event.addListener(googlemap, \'click\', function() {
              return false;
          });             
    }    
', \yii\web\View::POS_HEAD);

$this->registerJs('mapInit(15, "mapBaloon()");');
