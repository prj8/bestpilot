<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('pages', 'About');
?>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-sm-7 contact_form">
            <h1 class="text-blue"><?= Html::encode($this->title) ?></h1>
            <div>
                <?php if (Yii::$app->language=='pl'): ?>
                <p><strong>BestPilot.com</strong> to platforma łącząca szkoły latania z przyszłymi i aktualnymi pilotami.</p>
                <p>
                    Jeśli marzysz, aby zostać pilotem samolotu <strong>BestPilot.com</strong> to idealne miejsce! Znajdziesz tu zarówno
                    szkolenia do licencji turystycznej PPL(A), jak również oferty szkoleń zawodowych, "od zera do ATPL(A)"
                    oraz oferty budowania nalotu. Dopasuj parametry szkolenia do swoich potrzeb. Cena,
                    wiek samolotu czy lokalizacja to tylko niektóre z możliwości.
                </p>

                <p>System ocen umożliwia zapoznanie się z opiniami innych uczestników szkoleń. Ty również możesz ocenić wybraną szkołę.</p>

                <p>Nasz cel: <strong>dostarczanie ofert szkoleń samolotowych dopasowanych do potrzeb przyszłych i aktualnych pilotów.</strong></p>

                <h3>Korzyści dla pilotów:</h3>
                <ul>
                    <li>dostęp do limitowanych szkoleń</li>
                    <li>atrakcyjne oferty budowania nalotu</li>
                    <li>zniżki i promocje</li>
                </ul>

                <h3>Korzyści dla szkoły lotniczej:</h3>
                <ul>
                    <li>promowanie oferty</li>
                    <li>dostęp sprecyzowanej grupy docelowej</li>
                </ul>
                <p>Promujmy wspólnie rozwój lotnictwa w Polsce i na świecie.</p>
                <p><strong>BestPilot.com</strong> należy do grupy EVIONICA.</p>
                <?php elseif (Yii::$app->language=='en'): ?>
                    <p><strong>BestPilot.com</strong> is a platform connecting flight schools with current and future pilots.</p>
                    <p>
                        If your dream is to become a pilot <strong>BestPilot.com</strong> is perfect for you! You can find here not only Private Pilot License
                        PPL(A) trainings, but also professional courses, from zero to ATPL(A) programs and time building offers. You can
                        personalize your training and make sure that it suits you best. Price, aircraft manufacturing year or localization are just some possibilities.
                    </p>

                    <p>Thanks to the reviews you can get to know other students’ opinions. You can also rate the flight school and write a comment</p>

                    <p>Our goal:  <strong>deliver the best, personalized offers for current and future pilots.</strong></p>

                    <h3>Benefits for pilots:</h3>
                    <ul>
                        <li>Access to limited offers</li>
                        <li>Attractive time building offers</li>
                        <li>Discounts and rebates</li>
                    </ul>

                    <h3>Benefits for flight academies:</h3>
                    <ul>
                        <li>Promoting offers</li>
                        <li>Access to specific target group</li>
                    </ul>
                    <p>Let’s promote aviation development in Poland and in the world together.</p>
                    <p><strong>BestPilot.com</strong> belongs to EVIONICA group.</p>
                <?php endif; ?>
            </div>
        </div>
    </div>

</div>
