<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('login', 'Request password reset');
?>
<div class="site-login container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 contact_form col-sm-6 col-sm-offset-3">
            <h1 class="small text-blue"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>

                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('email'), 'class'=>'input-lg form-control']])
                    ->label(false)->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('login', 'Send <span class="glyphicon glyphicon-menu-right"></span>'), ['class' => 'btn btn-primary btn-lg']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
