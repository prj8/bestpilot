<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('login', 'Stwórz konto');
?>
<div class="site-login container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 contact_form col-sm-6 col-sm-offset-3">
            <h1 class="small text-blue"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'form-signup', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>

                <?= $form->field($model, 'email', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('email'), 'class'=>'input-lg form-control']])
                    ->label(false)->textInput() ?>

                <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password'), 'class'=>'input-lg form-control']])
                    ->label(false)->passwordInput() ?>

                <?= $form->field($model, 'password_repeat', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password_repeat'), 'class'=>'input-lg form-control']])
                    ->label(false)->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('Signup', 'Zarejestruj się <span class="glyphicon glyphicon-menu-right"></span>'), ['class' => 'btn btn-primary btn-lg', 'name' => 'signup-button']) ?>
                </div>

                <hr />

                <h3 class="text-blue"><?= Yii::t('login', 'Zaloguj przez:') ?></h3>

                <?php $authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['site/auth'],
                    'autoRender' => false
                ]); ?>
                <ul class="social-auth">
                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                        <li><?= Html::a( '<span class="icon-'.$client->getId().'"></span>'. $client->title, ['site/auth', 'authclient'=> $client->name, ], ['class' => "btn btn-lg $client->name"]) ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php AuthChoice::end(); ?>

                <hr />

                <?= Html::a(Html::tag('h3', Yii::t('Signup', 'Zaloguj się <span class="glyphicon glyphicon-menu-right"></span>')), ['site/login']) ?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
