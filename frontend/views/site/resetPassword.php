<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('login', 'Reset password');
?>
<div class="site-login container">
    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 contact_form col-sm-6 col-sm-offset-3">
            <h1 class="small text-blue"><?= Html::encode($this->title) ?></h1>
            <?php $form = ActiveForm::begin(['id' => 'reset-password-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>

                <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password'), 'class'=>'input-lg form-control']])
                    ->label(false)->passwordInput() ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('login', 'Save <span class="glyphicon glyphicon-menu-right"></span>'), ['class' => 'btn btn-primary btn-lg']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
