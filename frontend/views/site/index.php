<?php
	use twofox\select2\Select2;
	use common\models\LicenseType;
	use kartik\widgets\ActiveForm;
	use yii\helpers\Html,
		yii\helpers\ArrayHelper;


	$this->title = Yii::t('page/home', 'BestPilot');
		
	$LicenseType = LicenseType::find()->where(['deleted'=>0])->orderBy('order DESC')->all();
	$Country = backend\models\Country::find()->where(['deleted'=>0])->orderBy('country ASC')->all();

	$countryArr = ArrayHelper::map($Country, 'id', 'ttitle');
	asort($countryArr, SORT_STRING);

	$searchModel->license_id = $LicenseType[0]->id;
?>
<div class="site-index">
    <div class="jumbotron" style="height: 891px;">
        <div class="container"> 
	        <h1><?= \Yii::t('site/index', 'Zostań najlepszym pilotem.') ?></h1>
	        <h2><?= \Yii::t('site/index', 'Znajdź szkolenie dla siebie.') ?></h2>
	        <div class="row">
	        	<?php $form = ActiveForm::begin([
					'action' => ['training/index'],
					'method' => 'get',
					'options'=>['id'=>'training-search-form']
				]); ?>
				<div class="col-lg-3 col-sm-4 col-xs-12">
				<?= $form->field($searchModel, 'license_id')->label(false)->widget(Select2::classname(), [
					'hideSearch' => true,
					'data' => ArrayHelper::map(ArrayHelper::toArray($LicenseType), 'id', 'license'),
					'pluginOptions' => [
						'allowClear' => false,
						'dropdownParent'=> '.jumbotron'
						/*'initSelection' => new JsExpression('function(){
							wiIndwxResize();
						}')*/
					],
				]); ?>
				</div>
				<div class="col-lg-3 col-sm-4 col-xs-12">
					<?=
					Html::hiddenInput('ww', '').
					$form->field($searchModel, 'country_id')->label(false)->widget(Select2::classname(), [
						'hideSearch' => true,
						'data' => $countryArr,
						'options' => [
							'placeholder' => Yii::t('site/index', 'wybierz region')
						],
						'pluginOptions' => [
							'allowClear' => false,
							'dropdownParent'=> '.jumbotron'
							/*'initSelection' => new JsExpression('function(){
								wiIndwxResize();
							}')*/
						],
					]); ?>
				</div>
				<div class="col-lg-3 col-sm-4 col-xs-12">
	        		<?= Html::submitButton(Yii::t('site/index', '<span class="glyphicon glyphicon-search"></span> SZUKAJ'), ['class' => 'btn btn-warning btn-block text-uppercase']) ?>
	        	</div>
	        	<?php ActiveForm::end(); ?>
	        </div>
        </div>
    </div>

    <div class="container base-info">
        <div class="row">
        	<div class="col-lg-3 col-sm-6">
        		<div class="panel panel-default">
					<div class="panel-heading clearfix"><h1><?= \Yii::t('site/index', '103') ?></h1><h3><?= \Yii::t('site/index', 'Wyszkolonych<br />pilotów') ?></h3></div>
					<div class="panel-body">
					    <?= \Yii::t('site/index', 'Dołącz do grona najlepszych pilotów.') ?>
					</div>
				</div>
        	</div>
        	<div class="col-lg-3 col-sm-6">
        		<div class="panel panel-default">
					<div class="panel-heading clearfix"><h1><?= \Yii::t('site/index', '52') ?></h1><h3><?= \Yii::t('site/index', 'Szkoły<br />latania') ?></h3></div>
					<div class="panel-body">
					    <?= \Yii::t('site/index', 'Współpracujemy tylko ze sprawdzo- nymi ośrodkami szkolenia lotniczego.') ?>
					</div>
				</div>
        	</div>
        	<div class="col-lg-3 col-sm-6">
        		<div class="panel panel-default">
					<div class="panel-heading clearfix"><h1><span class="icon-gwarancja_icon"></span></h1><h3><?= \Yii::t('site/index', 'Gwarancja') ?></h3></div>
					<div class="panel-body">
					    <?= \Yii::t('site/index', 'Zyskujesz ubezpieczenie i opiekę na cały czas trwania szkolenia.') ?>
					</div>
				</div>
        	</div>
        	<div class="col-lg-3 col-sm-6">
        		<div class="panel panel-default">
					<div class="panel-heading clearfix"><h1><span class="icon-unikatowe-szkolenia_v2"></span></h1><h3><?= \Yii::t('site/index', 'Unikatowe<br />szkolenia') ?></h3></div>
					<div class="panel-body">
					    <?= \Yii::t('site/index', 'Jako członek BestPilot masz dostęp do specjalnych ofert.') ?>
					</div>
				</div>
        	</div>
        </div>
    </div>
</div>

<div class="info-data">
	<div class="container">
		<div class="clearfix">
			<?= Html::tag('h1', \Yii::t('site/index', 'Szkolenia'), ['class'=>'pull-left']) ?>
			<?= Html::a(\Yii::t('site/index', 'Pokaż wszystkie <span class="glyphicon glyphicon-menu-right"></span>'), ['license/index'], ['class'=>'pull-right strong']) ?>
		</div>
		<div class="row first-row">
			<?php foreach ($LicenseType as $key => $value): if($key>3) break; ?>
			<div class="col-lg-4 col-sm-6 <?= $key==3?'visible-sm':'' ?>">
        		<div class="panel panel-default">
					<div class="panel-heading clearfix"><?= Html::tag('h3', $value->license) ?></div>
					<div class="panel-body">
					    <?= $value->tLicenceDescription ?>
					</div>
					<div class="panel-footer text-center"><?= Html::a(\Yii::t('site/index', 'Szczegóły'), ['license/view', 'id'=>$value->id], ['class'=>'text-uppercase btn btn-primary']) ?></div>
				</div>
        	</div>
			<?php endforeach; ?>
		</div>
		
		<div class="clearfix">
			<?= Html::tag('h1', \Yii::t('site/index', 'Oni skorzystali')) ?>
		</div>
		
		<div class="row second-row">
			<div class="col-lg-5 col-sm-6">
        		<div class="panel panel-default">
					<?php if (Yii::$app->language=='pl'): ?>
						<div class="panel-heading clearfix">
							<h3>Paweł</h3>
							<p class="text-uppercase">Szkolenie PPL(A) - SmartAero</p>
						</div>
						<div class="panel-body">
							<em>W szkole SmartAero robiłem licencję PPL(A).  Według mnie to najlepsza szkoła lotnicza w Warszawie. Instruktorzy chętnie służyli pomocą i odpowiadali na wszystkie pytania. Stan techniczny samolotów oceniam jako znakomity. Zdecydowanie polecam!</em>
						</div>
					<?php elseif (Yii::$app->language=='en'): ?>
						<div class="panel-heading clearfix">
							<h3>Paweł</h3>
							<p class="text-uppercase">Training PPL(A) - SmartAero</p>
						</div>
						<div class="panel-body">
							<em>I have finished PPL(A) training in SmartAero. In my opinion it is the best flight school in Warsaw. Instructors were always ready to help and answer our questions. Technical condition of aircrafts was excellent. I will definitely recommend this school!</em>
						</div>
					<?php endif; ?>
				</div>
        	</div>
			<div class="col-lg-5 col-lg-offset-2 col-sm-6">
        		<div class="panel panel-default">
					<?php if (Yii::$app->language=='pl'): ?>
						<div class="panel-heading clearfix">
							<h3>Anna</h3>
							<p class="text-uppercase">Szkolenie ATPL(A) teoria - Adriana Aviation</p>
						</div>
						<div class="panel-body">
							<em>Szkolenie przebiegło bardzo sprawnie. Wykłady były bardzo interesujące i profesjonalnie zorganizowane. Duży plus dla szkoły za komputerowe kursy, dzięki którym można uczyć się również na odległość.</em>
						</div>
					<?php elseif (Yii::$app->language=='en'): ?>
						<div class="panel-heading clearfix">
							<h3>Anna</h3>
							<p class="text-uppercase">Training ATPL(A) theory - Adriana Aviation</p>
						</div>
						<div class="panel-body">
							<em>Training went very well. Lectures were very interesting and professionally organized. Computer courses are big advantage, thanks to them I was able to learn at home.</em>
						</div>
					<?php endif; ?>
				</div>
        	</div>
		</div>
	</div>
</div>