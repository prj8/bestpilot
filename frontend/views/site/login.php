<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html,
    yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\authclient\widgets\AuthChoice;

$this->title = Yii::t('login', 'Logowanie');
?>
<div class="site-login container">

    <div class="row">
        <div class="col-lg-4 col-lg-offset-4 contact_form col-sm-6 col-sm-offset-3">
            <?php $form = ActiveForm::begin(['id' => 'login-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>
                <h1 class="small text-blue"><?= Html::encode($this->title) ?></h1>

                <?= $form->field($model, 'username', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('username'), 'class'=>'input-lg form-control']])
                         ->label(false)
                         ->textInput() ?>

                <?= $form->field($model, 'password', ['inputOptions' => ['placeholder' => $model->getAttributeLabel('password'), 'class'=>'input-lg form-control']])
                         ->label(false)
                         ->passwordInput() ?>

                <div style="color:#999;margin:1em 0">
                    <?= Yii::t('login/view',  'If you forgot your password you can <a href="{link}">reset it</a>.', ['link'=>Url::to(['/site/request-password-reset'])]) ?>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('training/view', 'Login <span class="glyphicon glyphicon-menu-right"></span>'), ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?>
                </div>

                <hr />

                <h3 class="text-blue"><?= Yii::t('login', 'Zaloguj przez:') ?></h3>

                <?php $authAuthChoice = AuthChoice::begin([
                    'baseAuthUrl' => ['site/auth'],
                    'autoRender' => false
                ]); ?>
                <ul class="social-auth">
                    <?php foreach ($authAuthChoice->getClients() as $client): ?>
                        <li><?= Html::a( '<span class="icon-'.$client->getId().'"></span>'. $client->title, ['site/auth', 'authclient'=> $client->name, ], ['class' => "btn btn-lg $client->name"]) ?></li>
                    <?php endforeach; ?>
                </ul>
                <?php AuthChoice::end(); ?>

                <hr />

                <?= Html::a(Html::tag('h3', Yii::t('login', 'Zarejestruj się <span class="glyphicon glyphicon-menu-right"></span>')), ['site/signup']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
