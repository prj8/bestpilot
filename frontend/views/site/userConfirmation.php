<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

    use yii\helpers\Html;

    $this->title = Yii::t('registration/confirmationEmail', 'Confirm your email address');
?>
<div class="info-page extraTopMargin">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">

                <h1><?= Html::encode($header) ?></h1>
                <div class="alert alert-<%= $status %> extraTopMargin">
                    <?= Html::encode($text) ?>
                </div>
            </div>
        </div>
    </div>
</div>