<?php
    use yii\helpers\Url;
    use common\models\LicenseType;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html,
        yii\helpers\ArrayHelper,
        twofox\select2\Select2;

    $LicenseType = LicenseType::find()->where(['deleted'=>0])->orderBy('order DESC')->all();
    $Country = backend\models\Country::find()->where(['deleted'=>0])->orderBy('country ASC')->all();
    $searchModel->license_id = @Yii::$app->params['license_id'];
    $searchModel->country_id = @Yii::$app->params['country_id'];

$countryArr = ArrayHelper::map($Country, 'id', 'ttitle');
asort($countryArr, SORT_STRING);
?>

<div class="container-fluid  navbar-fixed-top" style="width:100%;border-bottom: 1px solid #e3e3e3 !important;">
    <div class="row bg-grey">
        <div class="container">
            <?php $form = ActiveForm::begin([
                'action' => ['training/index'],
                'method' => 'get',
                'options'=>['id'=>'training-search-form']
            ]); ?>
            <div class="back clearfix">
                <div class="pull-left hidden">
                    <a href="<?= Url::to(['training/index']) ?>" class="link back text-uppercase"><?= Yii::t('training/view', '<span class="icon-arrow_left"></span> wróć do listy szkoleń'); ?></a>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-3 s-item">
                    <?= $form->field($searchModel, 'license_id')->label(false)->widget(Select2::classname(), [
                        'hideSearch' => true,
                        'data' => ArrayHelper::map(ArrayHelper::toArray($LicenseType), 'id', 'license'),
                        'options' => [
                            'placeholder' => Yii::t('site/index', 'wybierz szkolenie')
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'selectOnClose'=>false,
                            'dropdownParent'=> '.navbar-fixed-top .form-group.field-trainingsearch-license_id'
                        ],
                    ]); ?>
                </div>
                <div class="col-lg-2 col-sm-3 col-xs-3 s-item">
                    <?=
                    Html::hiddenInput('ww', '').
                    $form->field($searchModel, 'country_id')->label(false)->widget(Select2::classname(), [
                        'hideSearch' => true,
                        'data' => $countryArr,
                        'options' => [
                            'placeholder' => Yii::t('site/index', 'wybierz region')
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'selectOnClose'=>false,
                            'dropdownParent'=> '.navbar-fixed-top .form-group.field-trainingsearch-country_id'
                        ],
                    ]); ?>
                </div>
                <div class="s-item button pull-left">
                    <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span>', ['class' => 'btn btn-link', 'id'=>'training-search-btn']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>