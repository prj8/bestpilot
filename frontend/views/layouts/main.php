<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use twofox\select2\Select2;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\models\TrainingSearch;
use kartik\widgets\ActiveForm,
    yii\helpers\Url,
    yii\helpers\ArrayHelper;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="57x57" href="/img/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body
    class="<?= Yii::$app->controller->id . ' ' . Yii::$app->controller->action->id . ' ' . Yii::$app->controller->module->id . ' ' . @Yii::$app->controller->actionParams['slug'] ?>">
<?php $this->beginBody() ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-89744500-1', 'auto');
    ga('send', 'pageview');

</script>

<div class="wrap">
    <?php
    $menuItems = [
        ['label' => Yii::t('main/menu', 'Szkolenia'), 'url' => ['/license/index']],
        ['label' => Yii::t('main/menu', 'O nas'), 'url' => ['/site/about']],
        ['label' => Yii::t('main/menu', 'Kontakt'), 'url' => ['/site/contact']],
    ];

    if (Yii::$app->user->isGuest) {
        //$menuItems[] = ['label' => Yii::t('main/menu', 'Signup'), 'url' => ['/site/signup']];
        $menuItems[] = ['label' => Yii::t('main/menu', 'Zaloguj <span class="glyphicon glyphicon-menu-right"></span>'), 'url' => ['/site/login'], 'options' => ['class' => 'login']];
    } else {
        $menuItems[] = ['label' => Yii::t('main/menu', 'Moje konto <span class="glyphicon glyphicon-menu-right"></span>'), 'url' => ['/office/index'], 'options' => ['class' => 'login']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(Yii::t('main/menu', 'Logout'), ['class' => 'btn btn-link'])
            . Html::endForm()
            . '</li>';
    }
    ?>

    <nav id="w0" class="navbar-inverse navbar-top navbar-fixed-top navbar">
        <div class="container">
            <button type="button" class="navbar-toggle pull-left visible-xs">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <?= Html::a(Html::img('/img/logo.svg', ['alt'=>'BestPilot', 'id'=>'logo']), ['/site/index']) ?>

            <?= \twofox\languagepicker\widgets\LanguagePicker::widget([
                'skin' => \twofox\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
                'size' => \twofox\languagepicker\widgets\LanguagePicker::SIZE_SMALL,
                'itemTemplate' => '<a href="{link}" title="{language}" class="text-uppercase">{name}</a>',
                'activeItemTemplate' => '<a href="{link}" title="{language}" class="active text-uppercase">{name}</a>',
                'parentTemplate' => '<div class="language-picker button-list pull-right {size}">{items}</div>',
                'languages' => array_combine(array_keys(\Yii::$app->languagepicker->languages), array_keys(\Yii::$app->languagepicker->languages))
            ]); ?>

            <?=
            Nav::widget([
                'encodeLabels' => false,
                'options' => ['class' => 'navbar-nav pull-right hidden-xs'],
                'items' => $menuItems,
            ]);
            ?>

        </div>
    </nav>

    <nav class="mobile-menu visible-xs">

        <?=
            Nav::widget([
                'encodeLabels' => false,
                'options' => ['class' => 'navbar-nav'],
                'items' => $menuItems,
            ]);
        ?>
    </nav>

    <?php

    if (!in_array(Yii::$app->controller->id . Yii::$app->controller->action->id, ['trainingindex', 'siteindex']))
        echo $this->render('/layouts/__shortMenu', ['searchModel' => new TrainingSearch()]);
    ?>

    <?php if (Yii::$app->controller->id . Yii::$app->controller->action->id !== 'siteindex'): ?>
        <div class="container first_margin">
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </div>
    <?php endif; ?>

    <?= Alert::widget() ?>

    <?= $content ?>
</div>

<footer class="bg-blue">
    <div class="container">
        <div class="row">

            <div class="col-xs-10 col-xs-offset-1 visible-xs">
                <?php
                $form = ActiveForm::begin([
                    'action' => null,
                    'method' => 'post',
                    'options'=>['id'=>'currency-form2', 'class'=>'form-inline']
                ]);
                $model = new \backend\models\base\Currency();
                $model->id = \Yii::$app->params['currency'];

                $url_lang = [];
                foreach (\Yii::$app->languagepicker->languages as $lang=>$title){
                    $url_lang[Url::current(['language'=>$lang])] = $title;
                }

                $Currency = ArrayHelper::map(\backend\models\base\Currency::find()->where(['active'=>1])->asArray()->all(), 'id', 'id');

                ?>

                <div class="form-group" style="overflow: auto;">
                <?=
                Html::tag('label', Yii::t('footer', 'Język')).
                Select2::widget([
                    'name' => 'lang',
                    'data' => $url_lang,
                    'hideSearch' => true,
                    'value'=>Url::current(),
                    'pluginOptions' => [
                        'allowClear' => false,
                    ],
                    'pluginEvents' => [
                        'change'=>'function(){
                            window.location.href = $(this).val()
                        }'
                    ]
                ]);
                ?>
                </div>
                <?= $form->field($model, 'id')
                    ->label(Yii::t('footer', 'Waluta'))
                    ->widget(Select2::classname(), [
                        'data' => $Currency,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'allowClear' => false,
                            //'dropdownParent'=> 'form#currency-form2'
                            //'selectOnClose'=>false,
                        ],
                        'showToggleAll' => false,
                        'pluginEvents' => [
                            'change'=>'function(){
                                $("#currency-form2").submit();
                            }'
                        ]
                    ])
                ?>
                <?php ActiveForm::end(); ?>
            </div>


            <div class="col-lg-12 col-xs-10 col-xs-offset-1 col-sm-offset-0">
                <?= Html::tag('h3', Yii::t('main/menu', 'Masz pytanie?'), ['class' => 'text-white']) ?>
                <a href="tel:+48570920787" class="btn btn-default btn-lg phone strong">+48 570 920 787</a>
                <a href="https://www.facebook.com/bestpilotcom/" class="btn btn-default btn-lg phone" target="_blank"><span class="icon-facebook"></span> Best Pilot</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-xs-12">
                <h3 class="text-white">BestPilot</h3>
                <p class="text-white address"><?= Yii::t('frontend/footer', 'al. Stanów Zjednoczonych 51/620, 04-028 Warsaw'); ?></p>
                <p class="text-white"><?= Yii::t('frontend/footer', 'VAT ID: 5441426877 Commercial Registry No.: 147066893'); ?></p>
            </div>


            <div class="col-xs-12 hidden-xs">
                <?php
                $form = ActiveForm::begin([
                    'action' => null,
                    'method' => 'post',
                    'options'=>['id'=>'currency-form']
                ]);
                $model = new \backend\models\base\Currency();
                $model->id = \Yii::$app->params['currency'];
                ?>
                <?= $form->field($model, 'id', ['inputOptions' => ['id' => 'mycurrencyid'], 'selectors' => ['input' => '#mycurrencyid']])
                    ->label(false)
                    ->widget(Select2::classname(), [
                        'data' => $Currency,
                        'hideSearch' => true,
                        'pluginOptions' => [
                            'allowClear' => false,
                            //'dropdownParent'=> 'footer form#currency-form'
                            //'selectOnClose'=>false,
                        ],
                        'options' => [
                            'id' => 'mycurrencyid'
                        ],
                        'showToggleAll' => false,
                        'pluginEvents' => [
                            'change'=>'function(){
                                $("#currency-form").submit();
                            }'
                        ]
                    ])
                ?>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-sm-12 text-white text-small">
                <p><small><?= Yii::t('frontend/footer', 'Użycie w Serwisie nazw, znaków towarowych, innych oznaczeń słownych, graficznych i słowno-graficznych przynależnych Ośrodkom Szkolenia Lotniczego (ATO) ma znaczenie jedynie informacyjne i służy odróżnieniu usług jednego ATO od usług innego ATO. Serwis nie przypisuje sobie związków z ATO innych niż wyraźnie wynikające z Regulaminu.  Użycie w Serwisie danych kontaktowych i odsyłaczy (aktywnych linków) ma jedynie na celu ułatwienie kontaktu z danym ATO dla zapoznania się z jego ofertą handlową.'); ?></small></p>
            </div>
        </div>
        <div class="row extraTopMargin">
            <div class="col-lg-12 text-white">
                <?= Yii::t('frontend/footer', 'BestPilot.com © 2017'); ?> -
                <a href="<?= Yii::t('frontend/footer', 'bestpilot_regulamin.pdf'); ?>" class="text-white" target="_blank"><?= Yii::t('frontend/footer', 'Regulamin'); ?></a> -
                <a href="<?= Yii::t('frontend/footer', 'bestpilot_polityka_prywatności.pdf'); ?>" class="text-white" target="_blank"><?= Yii::t('frontend/footer', 'Polityka Prywatności'); ?></a> -
                <a href="http://evionica.com" class="text-white" target="_blank"><?= Yii::t('frontend/footer', 'Wykonanie Evionica.com'); ?></a>
            </div>
        </div>
    </div>
</footer>


<div class="se-pre-con load">
    <div class="loader">Loading...</div>
</div>

<?php
$this->registerJs("wiIndwxResize(true);");
$this->endBody() ?>

<script type="text/javascript" src="//userlike-cdn-widgets.s3-eu-west-1.amazonaws.com/b3ef6cad584ef804aee918c21474a40ba861dfb99707ade182361bd8b13b127a.js"></script>
</body>
</html>
<?php $this->endPage() ?>