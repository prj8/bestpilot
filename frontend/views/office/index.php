<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use borales\extensions\phoneInput\PhoneInput;
use yii\web\JsExpression,
    common\models\LicenseType,
    yii\helpers\ArrayHelper,
    twofox\select2\Select2;

$this->title = Yii::t('pages', 'Moje konto');

$js = '
function PhoneInputPars(callback){
    $.get(\'//ipinfo.io\', function() {}, "jsonp").always(function(resp) {
        var countryCode = (resp && resp.country) ? resp.country : ""; 
        callback(countryCode);
    });
}

function customPlaceholder(selectedCountryPlaceholder, selectedCountryData) {       
  return selectedCountryPlaceholder;
}
';
$this->registerJs($js);
?>
    <div class="container">

        <?php $form = ActiveForm::begin(['id' => 'contact-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>
        <div class="row">
            <div class="col-lg-5 contact_form">
                <h1 class="text-blue"><?= Html::encode($this->title) ?></h1>

                <?= $form->field($profile, 'username', ['inputOptions' => ['class'=>'input-lg form-control']])
                    ->textInput()->label(Yii::t('site-app', 'Username')) ?>

                <?= $form->field($model, 'email', ['inputOptions' => ['class'=>'input-lg form-control']])
                    ->textInput() ?>

                <?= $form->field($profile, 'phone', ['inputOptions' => ['class'=>'input-lg form-control']])
                    ->widget(PhoneInput::className(), ['options'=>['class'=>'form-control input-lg'], 'jsOptions'=>['initialCountry'=>'auto', 'nationalMode'=>false, 'allowExtensions'=>true, 'autoHideDialCode'=>false, 'customPlaceholder'=>new JsExpression('customPlaceholder'), 'geoIpLookup'=>new JsExpression('PhoneInputPars')]]) ?>

                <?= $form->field($profile, 'need_license', ['inputOptions' => ['class'=>'inputn']])->widget(Select2::classname(), [
                    'hideSearch' => true,
                    'data' => ArrayHelper::map(LicenseType::find()->where(['deleted'=>0])->orderBy('order DESC')->asArray()->all(), 'id', 'license'),
                    'options' => [
                        'placeholder' => Yii::t('pages', 'wybierz')
                    ],
                    'pluginOptions' => [
                        'allowClear' => false,
                        'selectOnClose'=>false,
                        'dropdownParent'=> '.form-group.field-profile-need_license'
                    ],
                ]); ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('pages', 'Zatwierdź'), ['class' => 'btn btn-primary btn-lg', 'name' => 'contact-button']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
