<div class="container">
	<div class="row">
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

$trainingsDataProvider->pagination->pageSize = 1000;
$locations = array();
$i = 0;

foreach($trainingsDataProvider->models as $model){
	$a_t = count($model->aircraft)>0 ? $model->aircraft[0]->aircraftType->name : "";

	if($model->address_practice_id > 0){
		$i++;
		$coord = explode(",", $model->addressPractice->coordinates);
		if (is_array($coord) && count($coord)>=2)
		{
			$start_text = '';
			if(is_array($model->startDates)){
				$date = null;
				foreach ($model->startDates as $termin){
					if($termin->start_date>=date('Y-m-d')){
						$date = $termin;
						break;
					}
				}
				$start_text = is_object($date) ? $date->start_text : Yii::t('training/view', 'Contact us');
			}else{
				$start_text = Yii::t('training/view', 'od zaraz');
			}

			$locations[$i]= [
				0=>($model->name),
				1=>($model->price),
				2=>$start_text,
				3=>($model->addressPractice->address.', '.$model->addressPractice->{'zip-code'}.' '.$model->addressPractice->city),
				4=>($a_t),
				5=>($model->elearning_hours + $model->theory_hours),
				6=>($model->aircraft_hours + $model->simulator_hours),
				7=>trim($coord[0]),
				8=>trim($coord[1]),
				9=>$model->school->logo,
				10=>(Url::to(['training/view', 'id' => $model->id])),
				11=>$model->id,
			];

		}

	}

	if($model->address_theory_id != $model->address_practice_id && $model->address_theory_id > 0){
		$i++;
		$coord = explode(",", $model->addressTheory->coordinates);
		if (is_array($coord) && count($coord)>=2)
		{
			$start_text = '';
			if(is_array($model->startDates)){
				$date = null;
				foreach ($model->startDates as $termin){
					if($termin->start_date>=date('Y-m-d')){
						$date = $termin;
						break;
					}
				}
				$start_text = is_object($date) ? $date->start_text : Yii::t('training/view', 'Contact us');
			}else{
				$start_text = Yii::t('training/view', 'od zaraz');
			}
			$locations[$i]= [
				0=>($model->name),
				1=>($model->price),
				2=>$start_text,
				3=>($model->addressTheory->address.', '.$model->addressTheory->{'zip-code'}.' '.$model->addressTheory->city),
				4=>($a_t),
				5=>($model->elearning_hours + $model->theory_hours),
				6=>($model->aircraft_hours + $model->simulator_hours),
				7=>trim($coord[0]),
				8=>trim($coord[1]),
				9=>$model->school->logo,
				10=>(Url::to(['training/view', 'id' => $model->id])),
				11=>$model->id,
			];
		}

	}

	if($model->address_practice_id > 0 || $model->address_theory_id > 0)
	echo Html::tag('div', str_ireplace(["\n", "\r", "\t"], "", Yii::$app->view->renderFile(__DIR__.'/_trainingGrid.php', ['model'=>$model])), ['class'=>'hidden to_map col-lg-4 col-sm-6 col-md-4 col-xs-12 item grid', 'id'=>'info_window_'.$model->id, 'data-id'=>$model->id]);
}
?>
</div>
</div>

	<div id="mapa-div" style="height:780px; padding:0px;"></div>
	<div id="locationsHF" class="hidden">
		<?= json_encode($locations); ?>
	</div>