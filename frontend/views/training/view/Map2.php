<?php
    use yii\helpers\HtmlPurifier;
    use yii\helpers\Url;
?>

<div id="mapa-div2" class="col-lg-12" style="height:780px; padding:0px;"></div>

<?php
$tomap = '
    function mapReload2() {	             
            var bounds = new google.maps.LatLngBounds();';

foreach ($markers as $marker){
    $coord = explode(',', $marker['coordinates']);
    $tomap .= 'var marker = new google.maps.Marker({                
                position: new google.maps.LatLng("'.trim(@$coord[0]).'", "'.trim(@$coord[1]).'"),
                map: googlemap2,
                title: "'.htmlspecialchars($marker['title']).'"
            });        
            bounds.extend(marker.position);';
}

$tomap .= ' googlemap2.fitBounds(bounds); 
            googlemap2.setZoom( Math.min(8, googlemap2.getZoom()) );
            /*zoomChangeBoundsListener = google.maps.event.addListenerOnce(googlemap2, \'bounds_changed\', function(event) {
                if (this.getZoom()){
                    this.setZoom(10);
                }
             })*/;
    }
';

$this->registerJs($tomap, \yii\web\View::POS_HEAD);
?>