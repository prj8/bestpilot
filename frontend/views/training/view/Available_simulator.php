

<div class="container">
    <div class="row simulators">
        <?php foreach ($simulators as $simulator): ?>

        <div class="col-lg-3 col-sm-6 col-md-4">
            <div class="item">
                <h2><?= $simulator->type; ?></h2>
                <div>
                    <table class="details">
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Klasa:'); ?></th>
                            <td><?= $simulator->class; ?></td>
                        </tr>
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Typy samolotów:'); ?></th>
                            <td><?= $simulator->aircraft_types; ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>