<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use kartik\rating\StarRating;
?>

<div class="container">
    <?php if(is_array($schoolRates) && count($schoolRates)>0): ?>
        <?= Html::tag('h1', \Yii::t('school-rates', 'Recenzje kursantów ({num})', ['num'=>count($schoolRates)])); ?>
        <?php foreach ($schoolRates as $rate): ?>
        <?php
            $name = !empty($rate->user_id) ? $rate->user->profile->username : $rate->name;
            $name = explode(' ', $name);
            $name = $name[0].(isset($name[1])?' '.mb_strtoupper($name[1][0], 'UTF-8').'.':'');
        ?>
        <div class="row schoolRates">
            <div class="col-lg-6">
                <?= Html::tag('h5', Html::tag('strong', $name).\Yii::$app->formatter->asDatetime($rate->created_at, Yii::$app->params['dateFormat'])) ?>
                <div class="school-ratings">
                    <span class="star-ratings-sprite">
                        <span style="width:<?= $rate->rate*100/5 ?>%" class="star-ratings-sprite-rating"></span>
                    </span>
                </div>
                <div class="description">
                    <?= strip_tags($rate->text) ?>
                </div>
                <hr>
            </div>
        </div>
        <?php endforeach; ?>
        <?= Html::a(\Yii::t('schoolRatesForm', 'Dodaj recenzję'), 'javascript://', ['class' => 'add-opinion btn btn-primary btn-lg text-uppercase']) ?>
    <?php endif; ?>

    <div class="schoolRatesForm row contact_form">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id'=>'schoolRatesForm', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>
            <?= Html::tag('h2', \Yii::t('schoolRatesForm', 'Twoja recenzja')) ?>
            <?= $form->field($schoolRatesForm, 'text', ['inputOptions'=>['class'=>'input-lg form-control', 'placeholder'=>\Yii::t('schoolRatesForm', 'Wpisując swoją recenzję pomagasz innym studentom podjąć lepszą decyzję.')]])->label(false)->textarea() ?>
            <?php if(\Yii::$app->user->isGuest || method_exists(\Yii::$app->user->identity, 'profile') || empty(\Yii::$app->user->identity->profile->username)): ?>
                <?= $form->field($schoolRatesForm, 'name', ['inputOptions'=>['class'=>'input-lg form-control', 'placeholder'=>$schoolRatesForm->getAttributeLabel('name')]])->label(false) ?>
            <?php endif; ?>
            <?php if(\Yii::$app->user->isGuest): ?>
                <?= $form->field($schoolRatesForm, 'email', ['inputOptions'=>['class'=>'input-lg form-control', 'placeholder'=>$schoolRatesForm->getAttributeLabel('email')]])->label(false) ?>
            <?php endif; ?>
            <?= $form->field($schoolRatesForm, 'rate')->widget(StarRating::classname(), [
                'pluginOptions' => [
                    'size'=>'xs',
                    'showClear'=>false,
                    'step' => 1,
                    'emptyStar' => '<i class="glyphicon glyphicon-star"></i>',
                    'starCaptions' => [
                        0 => \Yii::t('school-rates', 'Not Rated'),
                        1 => \Yii::t('school-rates', 'One Star'),
                        2 => \Yii::t('school-rates', 'Two Stars'),
                        3 => \Yii::t('school-rates', 'Three Stars'),
                        4 => \Yii::t('school-rates', 'Four Stars'),
                        5 => \Yii::t('school-rates', 'Five Stars'),
                    ],
                ]
            ]); ?>
            <div class="form-group">
                <?= Html::submitButton(\Yii::t('schoolRatesForm', 'Dodaj recenzję'), ['class' => 'btn btn-primary btn-lg text-uppercase']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div><!-- training-form -->
</div>

<?php

$this->registerJs('
$(".add-opinion").click(function() {
    $(\'html, body\').animate({
        scrollTop: $(".schoolRatesForm").offset().top - $(".navbar-inverse.navbar-top").height() - $(".container-fluid.navbar-fixed-top").height()
    }, 350);
});
');