<?php
    use yii\helpers\HtmlPurifier;
    use yii\helpers\Url;
?>

<div id="mapa-div" class="col-lg-12" style="height:780px; padding:0px;"></div>
<?php
//$coord = explode(",", $model->school->coordinates);

$this->registerJs('
$(".nav-tabs").on("click", "a[href=\"#view_map\"]", function() { 
    mapInit(10, "mapReload()");
});
');

$tomap = '
    function mapReload() {	             
            var bounds = new google.maps.LatLngBounds();';

foreach ($markers as $marker){
    $coord = explode(',', $marker['coordinates']);
    $tomap .= 'var marker = new google.maps.Marker({                
                position: new google.maps.LatLng("'.trim(@$coord[0]).'", "'.trim(@$coord[1]).'"),
                map: googlemap,
                title: "'.htmlspecialchars($marker['title']).'"
            });        
            bounds.extend(marker.position);';
}

$tomap .= '
            
                googlemap.fitBounds(bounds);
                googlemap.setZoom( Math.min(8, googlemap.getZoom()) );
            
            
            
            /*zoomChangeBoundsListener = google.maps.event.addListenerOnce(googlemap, \'bounds_changed\', function(event) {
                if (this.getZoom()){
                    this.setZoom(10);
                }
             });*/;
             
            setTimeout(function(){googlemap.setZoom( Math.min(8, googlemap.getZoom()) );}, 1500)
    }
';

$this->registerJs($tomap, \yii\web\View::POS_HEAD);
?>