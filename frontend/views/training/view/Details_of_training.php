<?php
    use yii\helpers\Html;
?>

<div class="container">
    <div class="row">
        <?php if(!empty($model->information)): ?>
        <div class="col-lg-6">
            <h2><?= Yii::t('training/view', 'Informacje ogólne'); ?></h2>
            <div><?= $model->information ?></div>
        </div>
        <?php endif; ?>

        <?php  if(!empty($model->licenseType->requirements)): ?>
        <div class="col-lg-6 requirements clearfix">
            <h2><?= Yii::t('training/view', 'Wymagania'); ?></h2>
            <div><?= $model->licenseType->trequirements ?></div>
        </div>
        <?php endif; ?>
    </div>

    <h2><?= Yii::t('training/view', 'Program szkolenia'); ?></h2>

    <?php
        $course = $model->theory_hours + $model->elearning_hours + $model->aircraft_hours + $model->simulator_hours;
        $theory_hours = $elearning_hours = $aircraft_hours = $simulator_hours = $Teoria = $Praktyka = 0;
        if($course > 0){
            $theory_hours = $model->theory_hours * 100 / $course;
            $elearning_hours = $model->elearning_hours * 100 / $course;
            $aircraft_hours = $model->aircraft_hours * 100 / $course;
            $simulator_hours = $model->simulator_hours * 100 / $course;

            $Teoria = $model->theory_hours + $model->elearning_hours;
            $Praktyka = $model->aircraft_hours + $model->simulator_hours;
        }
    ?>
    <div class="progress-line">
        <div class="clearfix">
            <?php if($Teoria > 0): ?>
            <div style="width: <?= $theory_hours+$elearning_hours ?>%" data-sum="<?= $theory_hours+$elearning_hours ?>" class="theory_hours_elearning_hours check_width">
                <h3><?= Yii::t('training/view', 'Teoria – {num} h', ['num'=>$Teoria]); ?></h3>
            </div>
            <?php endif; ?>

            <?php if($Praktyka > 0): ?>
            <div style="width: <?= $aircraft_hours+$simulator_hours ?>%" data-sum="<?= $aircraft_hours+$simulator_hours ?>" class="hidden-xs aircraft_hours_simulator_hours check_width">
                <h3><?= Yii::t('training/view', 'Praktyka – {num} h', ['num'=>$Praktyka]); ?></h3>
            </div>
            <?php endif; ?>
        </div>
        <div class="clearfix">
            <?php if($theory_hours > 0): ?>
            <div style="background-color: #0f2565;width: <?= $theory_hours ?>%" data-sum="<?= $theory_hours ?>" class="theory_hours check_width">
                <span class="icon-wyklady"></span><br />
                <strong><?= $model->theory_hours ?></strong>h
            </div>
            <?php endif; ?>

            <?php if($elearning_hours > 0): ?>
            <div style="background-color: #485888;width: <?= $elearning_hours ?>%" data-sum="<?= $elearning_hours ?>" class="elearning_hours check_width">
                <span class="icon-e-learning"></span><br />
                <strong><?= $model->elearning_hours ?></strong>h
            </div>
            <?php endif; ?>

            <?php if($aircraft_hours > 0): ?>
            <div style="background-color: #588dd0;width: <?= $aircraft_hours ?>%" data-sum="<?= $aircraft_hours ?>" class="hidden-xs aircraft_hours check_width">
                <span class="icon-praktyka"></span><br />
                <strong><?= $model->aircraft_hours ?></strong>h
            </div>
            <?php endif; ?>

            <?php if($simulator_hours > 0): ?>
            <div style="background-color: #a5bfe0;width: <?= $simulator_hours ?>%" data-sum="<?= $simulator_hours ?>" class="hidden-xs simulator_hours check_width">
                <span class="icon-symulator"></span><br />
                <strong><?= $model->simulator_hours ?></strong>h
            </div>
            <?php endif; ?>
        </div>
        <div class="clearfix">
            <?php if($theory_hours > 0): ?>
                <div style="width: <?= $theory_hours ?>%" data-sum="<?= $theory_hours ?>" class="theory_hours check_width">
                    <h4><?= Yii::t('training/view', 'wykłady'); ?></h4>
                </div>
            <?php endif; ?>

            <?php if($elearning_hours > 0): ?>
                <div style="width: <?= $elearning_hours ?>%" data-sum="<?= $elearning_hours ?>" class="elearning_hours check_width">
                    <h4><?= Yii::t('training/view', 'e-learning'); ?></h4>
                </div>
            <?php endif; ?>

            <?php if($aircraft_hours > 0): ?>
                <div style="width: <?= $aircraft_hours ?>%" data-sum="<?= $aircraft_hours ?>" class="hidden-xs aircraft_hours check_width">
                    <h4><?= Yii::t('training/view', 'loty samolotem'); ?></h4>
                </div>
            <?php endif; ?>

            <?php if($simulator_hours > 0): ?>
                <div style="width: <?= $simulator_hours ?>%" data-sum="<?= $simulator_hours ?>" class="hidden-xs simulator_hours check_width">
                    <h4><?= Yii::t('training/view', 'symulator'); ?></h4>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="progress-line visible-xs">
        <div class="clearfix">
            <?php if($Praktyka > 0): ?>
                <div style="width: <?= $aircraft_hours+$simulator_hours ?>%" data-sum="<?= $aircraft_hours+$simulator_hours ?>" class="aircraft_hours_simulator_hours check_width">
                    <h3><?= Yii::t('training/view', 'Praktyka – {num} h', ['num'=>$Praktyka]); ?></h3>
                </div>
            <?php endif; ?>
        </div>
        <div class="clearfix">
            <?php if($aircraft_hours > 0): ?>
                <div style="background-color: #588dd0;width: <?= $aircraft_hours ?>%" data-sum="<?= $aircraft_hours ?>" class="aircraft_hours check_width">
                    <span class="icon-praktyka"></span><br />
                    <strong><?= $model->aircraft_hours ?></strong>h
                </div>
            <?php endif; ?>

            <?php if($simulator_hours > 0): ?>
                <div style="background-color: #a5bfe0;width: <?= $simulator_hours ?>%" data-sum="<?= $simulator_hours ?>" class="simulator_hours check_width">
                    <span class="icon-symulator"></span><br />
                    <strong><?= $model->simulator_hours ?></strong>h
                </div>
            <?php endif; ?>
        </div>
        <div class="clearfix">
            <?php if($aircraft_hours > 0): ?>
                <div style="width: <?= $aircraft_hours ?>%" data-sum="<?= $aircraft_hours ?>" class="aircraft_hours check_width">
                    <h4><?= Yii::t('training/view', 'loty samolotem'); ?></h4>
                </div>
            <?php endif; ?>

            <?php if($simulator_hours > 0): ?>
                <div style="width: <?= $simulator_hours ?>%" data-sum="<?= $simulator_hours ?>" class="simulator_hours check_width">
                    <h4><?= Yii::t('training/view', 'symulator'); ?></h4>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <div class="row legend">
        <?php if($model->theory_hours+$model->elearning_hours > 0): ?>
        <div class="col-lg-5">
            <h2><?= Yii::t('training/view', 'Teoria'); ?></h2>
            <?php if($model->theory_hours > 0): ?>
            <div class="data">
                <span style="background-color: #0f2565;"></span>
                <?= $model->theory_hours ?>h
                <strong><?= Yii::t('training/view', 'Wykłady'); ?></strong>
                <?= !empty($model->theory_details) ? Html::tag('div', $model->theory_details, ['class'=>'description theory_details']) : '' ?>
            </div>
            <?php endif; ?>

            <?php if($model->elearning_hours > 0): ?>
            <div class="data">
                <span style="background-color: #485888;"></span>
                <?= $model->elearning_hours ?>h
                <strong><?= Yii::t('training/view', 'E-learning'); ?></strong>
                <?= !empty($model->elearning_details) ? Html::tag('div', $model->elearning_details, ['class'=>'description theory_details']) : '' ?>
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <?php if($model->aircraft_hours+$model->simulator_hours > 0): ?>
        <div class="col-lg-5">
            <h2><?= Yii::t('training/view', 'Praktyka'); ?></h2>
            <?php if($model->aircraft_hours > 0): ?>
            <div class="data">
                <span style="background-color: #588dd0;"></span>
                <?= $model->aircraft_hours ?>h
                <?php if(isset($model->aircraft[0]->type)): ?>
                    <strong><?= Yii::t('training/view', 'Loty samolotem {type}', ['type'=>$model->aircraft[0]->type]); ?></strong>
                <?php else: ?>
                    <strong><?= Yii::t('training/view', 'Loty samolotem '); ?></strong>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <?php if($model->simulator_hours > 0): ?>
            <div class="data">
                <span style="background-color: #a5bfe0;"></span>
                <?= $model->simulator_hours ?>h<br />
                <strong><?= Yii::t('training/view', 'Symulator lotów'); ?></strong>
            </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
    </div>
</div>