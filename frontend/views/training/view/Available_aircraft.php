<?php
    use yii\helpers\Html;
?>

<div class="container">
    <div class="row aircrafts">
        <?php foreach ($aircrafts as $aircraft): ?>

        <?php
            if(!isset($aircraft->aircraftType))
                continue;

            $Wyposażenie = [];
            if($aircraft->gps)
                $Wyposażenie[] = 'GPS';

            if($aircraft->ils)
                $Wyposażenie[] = 'ILS';

            if($aircraft->radio)
                $Wyposażenie[] = 'Radio';

            if(!empty($aircraft->glass_cockpit))
                $Wyposażenie[] = $aircraft->glass_cockpit;
        ?>

        <div class="col-lg-3 col-sm-6 col-md-4">
            <div class="item">
                <?= Html::img($aircraft->photoResize(202, 135)) ?>
                <h2><?= !empty($aircraft->register_no) ? $aircraft->register_no : $aircraft->aircraftType->name; ?></h2>
                <div>
                    <table class="details">
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Typ:'); ?></th>
                            <td><?= $aircraft->aircraftType->name; ?></td>
                        </tr>
                        <?php if($aircraft->aircraft_year > 0): ?>
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Rok produkcji:'); ?></th>
                            <td><?= $aircraft->aircraft_year; ?></td>
                        </tr>
                        <?php endif; ?>
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Zasięg:'); ?></th>
                            <td><?= $aircraft->aircraftType->range; ?></td>
                        </tr>
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Max prędkosc:'); ?></th>
                            <td><?= $aircraft->aircraftType->speed; ?></td>
                        </tr>
                        <?php if(count($Wyposażenie) > 0): ?>
                        <tr>
                            <th class="text-right"><?= Yii::t('training/view', 'Wyposażenie:'); ?></th>
                            <td><?= implode(', ', $Wyposażenie) ?></td>
                        </tr>
                        <?php endif; ?>
                    </table>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>