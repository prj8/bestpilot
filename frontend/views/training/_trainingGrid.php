<?php
use yii\helpers\Html,
	yii\helpers\HtmlPurifier,
	imanilchaudhari\CurrencyConverter\CurrencyConverter;

	$converter = new CurrencyConverter();
?>

<div class="thumbnail">
	<div class="clearfix">
		<?php if(!empty($model->school->logo)): ?>
			<?= Html::img($model->school->LogoResize(125), ['class'=>'pull-left', 'style'=>'max-height:52px;max-width: 95px;']) ?>
		<?php endif; ?>
		<h2 class="pull-right text-right"><?= number_format(($model->school->country->currency_id!=Yii::$app->params['currency'] ? $converter->convert($model->school->country->currency_id, Yii::$app->params['currency'], $model->price) : $model->price), 0, '.', '<span class="numspan"></span>') ?><span><?= Yii::$app->params['currency'] ?></span></h2>
	</div>
	<div class="clearfix title-box">
		<h3><?= Html::a($model->name, ['training/view', 'id' => $model->id], ['data-pjax'=>'0']) ?></h3>
		<div class="school-ratings">
			<span class="star-ratings-sprite">
				<span style="width:<?= $model->school_rate*100/5 ?>%" class="star-ratings-sprite-rating"></span>
			</span>
			<?= number_format(floatval($model->school_rate), 1, '.', ''); ?>
		</div>
	</div>
	<table class="details">
		<tr>
			<th>
				<?= Yii::t('training/view', 'Termin:'); ?>
			</th>
			<td>
				<?php
				if(is_array($model->startDates)){
					$date = null;
					foreach ($model->startDates as $termin){
						if($termin->start_date>=date('Y-m-d')){
							$date = $termin;
							break;
						}
					}
					echo is_object($date) ? $date->start_text : Yii::t('training/view', 'Contact us');
				}else{
					echo Yii::t('training/view', 'od zaraz');
				}
				?>
			</td>
		</tr>
		<tr>
			<th>
				<?= Yii::t('training/view', 'Miasto:'); ?>
			</th>
			<td>
				<span><?= HtmlPurifier::process($model->address_practice_id != 0 ? $model->addressPractice->city : ($model->address_theory_id != 0 ? $model->addressTheory->city : '-')) ?></span>&nbsp;
			</td>
		</tr>
		<?php if (count($model->aircraft) != 0 || count($model->simulator) != 0) : ?>
		<tr>
			<th>
				<?php
				if (count($model->aircraft) == 0 && count($model->simulator) > 0) {
					echo Yii::t('training/view', 'Simulator:');
				} else
					echo Yii::t('training/view', 'Aircraft:');
				?>
			</th>
			<td>
					<span>
						<?php
						if (count($model->aircraft) == 0 && count($model->simulator) > 0) {
							echo trim($model->simulator[0]->type);
						} else {
							if (count($model->aircraft) > 0) {
								echo trim($model->aircraft[0]->aircraftType->name);
							} else {
								echo "-";
							}
						}
						?>
					</span>&nbsp;
			</td>
		</tr>

		<?php endif; ?>

		<?php if(($model->elearning_hours + $model->theory_hours) > 0): ?>
			<tr>
				<th>
					<?= Yii::t('training/view', 'Teoria:'); ?>
				</th>
				<td>
					<?= ($model->elearning_hours + $model->theory_hours) ?>h
				</td>
			</tr>
		<?php endif; ?>

		<?php if(($model->aircraft_hours + $model->simulator_hours) > 0): ?>
		<tr>
			<th>
				<?= Yii::t('training/view', 'Praktyka:'); ?>
			</th>
			<td>
				<?= ($model->aircraft_hours + $model->simulator_hours) ?>h
			</td>
		</tr>
		<?php endif; ?>

	</table>
	<div class="col-lg-12 col-md-12 col-sm-12  col-xs-12 text-center extraTopMargin buttonCnt">

			<?= Html::a(Yii::t('training/view', 'Szczegóły'), ['training/view', 'id' => $model->id], ['class' => 'btn btn-primary text-uppercase', 'data-pjax'=>'0']) ?>

	</div>
</div>