<?php

use yii\helpers\Html,
    yii\helpers\Url,
    common\models\Training;
use twofox\select2\Select2;
use yii\bootstrap\ActiveForm;
use kartik\slider\Slider,
    yii\helpers\ArrayHelper,
    common\models\LicenseType,
    backend\models\Country;
use kartik\date\DatePicker,
    yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model backend\models\TrainingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<?php $form = ActiveForm::begin([
    'action' => ['index'],
    'method' => 'get',
    'options' => ['data-pjax' => true, 'data-md5'=>$md5, 'id' => 'training-search-form']
]);

$Country = Country::find()->where(['deleted'=>0])->orderBy('country ASC')->all();
$LicenseType = LicenseType::find()->where(['deleted'=>0])->indexBy('id')->orderBy('order DESC')->all();
$license_id = $model->license_id;

function validateDate($date){
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}
?>

    <?php $pjax = Pjax::begin(['id' => 'filterdata', 'timeout' => 0, 'formSelector'=>'#training-search-form', 'scrollTo'=>false]) ?>
    <div class="bg-grey training-list">
        <div class="container">
            <div class="row training-categories">
                <div class="col-lg-9 training-categories">
                    <?php
                        $i = 0;
                        $ff = [];
                        echo Html::hiddenInput('view', \Yii::$app->request->get('view', Yii::$app->params['defaultView']));
                        echo Html::hiddenInput('ww', \Yii::$app->request->get('ww', 0));
                    ?>

                    <?=
                        $form->field($model, 'license_id')
                            ->label(false)
                            ->widget(Select2::classname(), [
                                'data' => ArrayHelper::map($LicenseType, 'id', 'license'),
                                'hideSearch' => true,
                                'options' => [
                                    'class'=> $model->license_id > 0 ? 'active' : '',
                                    'data' => ['pjax'=>'0']
                                ],
                                'pluginOptions' => [
                                    'allowClear' => false,
                                    'selectOnClose'=>false,
                                    'dropdownParent'=> '.form-group.field-trainingsearch-license_id'
                                ],
                                'showToggleAll' => false,
                            ]);
                    ?>

                    <?= $form->field($model, 'country_id')
                        ->label(false)
                        ->widget(Select2::classname(), [
                            'data' => ArrayHelper::map($Country, 'id', 'ttitle'),
                            'hideSearch' => true,
                            'options' => [
                                'data' => ['pjax'=>'0']
                            ],
                            'pluginOptions' => [
                                'allowClear' => false,
                                'selectOnClose'=>false,
                                'dropdownParent'=> '.form-group.field-trainingsearch-country_id'
                            ],
                            'showToggleAll' => false,
                        ])
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-white training-search">
        <div class="container form-training-search">
            <div class="row">
                <?php
                if(count(\Yii::$app->params['Aircrafts']) > 0):
                ?>
                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <?= $form->field($model, 'aircraftType', ['inputOptions' => ['class' => 'input-lg']])
                        ->label($model->getAttributeLabel('aircraftType'))
                        ->widget(Select2::classname(), [
                        'data' => \Yii::$app->params['Aircrafts'],
                        'options' => [
                            'placeholder' => Yii::t('frontend/training', 'Choose Aircraft'),
                            'multiple' => true,
                            'hideSearch' => true,
                            'selectOnClose'=>false,
                            'closeOnSelect'=>false,
                            'minimumInputLength'=> 0
                        ],
                        'pluginOptions' => [
                            'allowClear' => false,
                            'selectOnClose'=>false,
                            'closeOnSelect'=>false,
                            'hideSearch' => true,
                            'dropdownParent'=> '.form-group.field-trainingsearch-country_id'
                        ],
                        'showToggleAll' => false,
                        'pluginEvents' => [
                            "select2:open" => "function() { $(\".select2-search__field\").attr(\"readonly\", \"true\"); }",
                        ]
                    ]); ?>
                </div>
                <?php endif; ?>

                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <?php echo $form->field($model, 'price', ['template' => "{label}\n<div class=\"result\"></div><div class=\"slideup\"><span class=\"arrow\"></span><span>" . Yii::t('search', 'do') . ": <h3><span></span> <span>". htmlspecialchars(\Yii::$app->params['currency_symbol']) ."</span></h3></span>{input}</div>\n{hint}"])
                        ->label($model->getAttributeLabel('price'))
                        ->widget(Slider::classname(), [
                        'pluginOptions' => [
                            'min' => \Yii::$app->params['minFilterPrice'],
                            'max' => \Yii::$app->params['maxFilterPrice'],
                            //'value'=>intval(Training::find()->max('price')),
                            'handle' => 'square',
                            'step'=>100
                        ],
                        'pluginEvents' => [
                            'slide' => 'function(obj) { checkvalueslider(this, obj.value) }',
                            'change' => 'function(obj) { checkvalueslider(this, (typeof obj.value.newValue != "undefined" ? obj.value.newValue : null)) }'
                        ]
                    ]);
                    ?>
                </div>

                <?php
                    if ($model->license_id==7) \Yii::$app->params['hasElearning'] = array();
                ?>
                <?php if(count(\Yii::$app->params['hasElearning']) > 1): ?>
                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <?= $form->field($model, 'hasElearning')
                        ->label(Yii::t('common/training', 'Tryb szkolenia'))
                        ->widget(Select2::classname(), [
                            'data' => \Yii::$app->params['hasElearning'],
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => false,
                                'dropdownParent'=> '.form-group.field-trainingsearch-haselearning'
                                //'selectOnClose'=>false,
                            ],
                            'showToggleAll' => false,
                        ]) ?>
                </div>
                <?php endif; ?>

                <?php if(count(\Yii::$app->params['Aircrafts']) > 0): ?>
                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <?php echo $form->field($model, 'aircraftAge', ['template' => "{label}\n<div class=\"result\"></div><div class=\"slideup\"><span class=\"arrow\"></span><span>" . Yii::t('search', 'do') . ": <h3><span></span> <span>". Yii::t('search', 'lat') ."</span></h3></span>{input}</div>\n{hint}"])->widget(Slider::classname(), [
                        'pluginOptions' => [
                            'min' => 1,
                            'max' => \Yii::$app->params['maxAircraftAge'],
                            'step' => 1,
                            //'value' => 40,
                            'ticks_labels' => ['40+'],
                            'handle' => 'square',
                        ],
                        'pluginEvents' => [
                            'slide' => 'function(obj) { checkvalueslider(this, obj.value) }',
                            'change' => 'function(obj) { checkvalueslider(this, (typeof obj.value.newValue != "undefined" ? obj.value.newValue : null)) }'
                        ]
                    ]);
                    ?>
                </div>
                <?php endif; ?>

                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <div id="termin-range">
                        <label class="control-label" for="trainingsearch-Termin"><?= Yii::t('common/training', 'Termin') ?></label>
                        <?php
                        $od = isset($model->dateRange['start']) && validateDate($model->dateRange['start']) ? $model->dateRange['start'] : null;
                        echo DatePicker::widget([
                            'name' => $model->formName().'[dateRange][start]',
                            'value' => $od,
                            'separator' => '',
                            'type' => DatePicker::TYPE_BUTTON,
                            'pluginEvents' => [
                                "hide" => "function(e) { $('#termin-range span.date').removeClass('active'); }",
                                "changeDate" => "function(e){ for_slider() }",
                            ],
                            'buttonOptions' => [
                                'label'=> $od!=null ? Yii::t('common/training', 'od {date}', ['date'=>date('d.m', strtotime($od))]) : Yii::t('common/training', 'od')
                            ],
                            'pluginEvents' => [
                                'hide' => 'function(e) {$(\'#termin-range span.date\').removeClass(\'active\');}'
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => date('Y-m-d', time()),
                                'orientation'=> "bottom left",
                                'container'=>'#termin-range',
                                'calendarWeeks' => false,
                                'maxViewMode' => 0,
                                'templates'=> [
                                    'leftArrow'=>'<span class="glyphicon glyphicon-menu-left"></span>',
                                    'rightArrow'=>'<span class="glyphicon glyphicon-menu-right"></span>'
                                ]
                            ]
                        ]);

                        $do = isset($model->dateRange['end']) && validateDate($model->dateRange['end']) ? $model->dateRange['end'] : null;
                        echo DatePicker::widget([
                            'name' => $model->formName().'[dateRange][end]',
                            'value' => isset($model->dateRange['end']) ? $model->dateRange['end'] : null,
                            'separator' => '',
                            'type' => DatePicker::TYPE_BUTTON,
                            'pluginEvents' => [
                                "hide" => "function(e) { $('#termin-range span.date').removeClass('active'); }",
                            ],
                            'buttonOptions' => [
                                'label'=> $do!=null ? Yii::t('common/training', 'do {date}', ['date'=>date('d.m', strtotime($do))]) : Yii::t('common/training', 'do')
                            ],
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd',
                                'startDate' => date('Y-m-d', time()),
                                'orientation'=> "bottom right",
                                'container'=>'#termin-range',
                                'calendarWeeks' => false,
                                'maxViewMode' => 0,
                                'templates'=> [
                                    'leftArrow'=>'<span class="glyphicon glyphicon-menu-left"></span>',
                                    'rightArrow'=>'<span class="glyphicon glyphicon-menu-right"></span>'
                                ]
                            ]
                        ]);
                        ?>
                    </div>
                </div>

                <div class="col-lg-3 col-sm-6 col-md-4 col-xs-12">
                    <?= $form->field($model, 'lang')
                        ->label(Yii::t('common/training', 'Język'))
                        ->widget(Select2::classname(), [
                            'data' => array_merge([0 => Yii::t('common/training', 'wybierz')], ArrayHelper::map($model->allLanguages(['AND', ['=', 'license_id', $model->license_id],['=', 'school.country_id', $model->country_id]]), 'language', 'name')),
                            'hideSearch' => true,
                            'pluginOptions' => [
                                'allowClear' => false,
                                'dropdownParent'=> '.form-group.field-trainingsearch-lang'
                                //'selectOnClose'=>false,
                            ],
                            'showToggleAll' => false,
                        ])
                    ?>
                </div>


                <div class="form-group hidden">
                    <?= Html::submitButton(Yii::t('backend/training', 'Search'), ['class' => 'btn btn-primary', 'id' => 'training-search-btn']) ?>
                    <?=
                        Html::resetButton(Yii::t('backend/training', 'Reset'), ['class' => 'btn btn-default'])
                    ?>
                </div>
            </div>
            
            <div class="clearfix text-center visible-xs"><?= Html::a(Yii::t('backend/training', 'Pokaż filtry'), 'javascript://', ['class' => 'btn btn-default btn-block btn-lg filter-btn']) ?></div>
        </div>
    </div>
    <?php Pjax::end() ?>
<?php ActiveForm::end(); ?>