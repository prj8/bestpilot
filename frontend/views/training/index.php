<?php

/* @var $this yii\web\View */

use yii\widgets\ListView,
    yii\widgets\Pjax,
    yii\helpers\Url,
    yii\helpers\Html;

$this->title = 'Wyszukaj szkolenie';
$view = \Yii::$app->request->get('view', Yii::$app->params['defaultView']);
?>
    <?= $this->render('_search', ['model' => $searchModel, 'md5' => $md5]); ?>

    <?php $pjax = Pjax::begin(['id' => 'listData', 'formSelector'=>'#training-search-form', 'timeout' => 500000, 'scrollTo'=>false]) ?>
    <?php
    $sort = [];
    foreach ($trainingsDataProvider->sort->attributes as $k => $attribute){
        $sort[$k] = Yii::t('widok', '{title} (rosnąco)', ['title'=>$attribute['label']]);
        $sort['-'.$k] = Yii::t('widok', '{title} (malejąco)', ['title'=>$attribute['label']]);
    }
    $sorting = \Yii::$app->request->get('sort', 'price');
    ?>
    <div class="bg-grey training-list" data-md5="<?= $md5 ?>">
        <div class="container tab-pane">
            <div class="pull-left sorting visible-xs"><?= Html::dropDownList('sorting', $sorting, $sort) ?></div>
            <ul class="nav nav-tabs training">
                <li role="presentation" <?= $view=='grid'?'class="active"':'' ?> >
                    <?= Html::a('<i class="icon-siatka ico"></i>'.Yii::t('widok', 'Siatka'), Url::current(['view'=>'grid']), ['data-pjax'=>'0']) ?>
                </li>
                <li role="presentation" class="hidden-xs <?= $view=='list'?'active':'' ?>">
                    <?= Html::a('<i class="icon-lista ico"></i>'.Yii::t('widok', 'Lista'), Url::current(['view'=>'list']), ['data-pjax'=>'0']) ?>
                </li>
                <li role="presentation" <?= $view=='map'?'class="active"':'' ?>>
                    <?= Html::a('<i class="icon-mapa ico"></i>'.Yii::t('widok', 'Mapa'), Url::current(['view'=>'map']), ['data-pjax'=>'0']) ?>
                </li>
            </ul>
        </div>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php if($view=='grid'): ?>
            <div class="tab-pane active" id="grid-panel">
                <div class="container">
                    <div class="hidden hidden-sorting">
                        <?php
                        foreach ($sort as $k=>$sort_details){
                            echo Html::a($k, Url::current(['sort' => $k]), ['class'=>'hidden-sorting '.$k]);
                        }
                        ?>
                    </div>
                    <?php echo ListView::widget([
                        'dataProvider' => $trainingsDataProvider,
                        'itemView' => '_trainingGrid',
                        'itemOptions' => ['class' => 'col-lg-4 col-sm-6 col-md-4 col-xs-12 item grid'],
                        'options' => ['class' => 'col-lg-12 list-view'],
                        'emptyText' => Yii::t('widok', 'Brak wyników. Zmień ustawienia filtrowania lub skontaktuj się z nami.'),
                        'emptyTextOptions' => ['tag'=>'h1', 'class'=>'empty-text'],
                        'pager'=> [
                            'maxButtonCount' => 5
                        ],
                        /*'pager' => [
                            'class' => \twofox\infinitescroll\InfiniteScrollPager::className(),
                            'paginationSelector' => '.pagination-wrap',
                            'pjaxContainer' => $pjax->id,
                            'itemWrapper' => '.item-wraper',
                            'itemSelector' => '.item',
                            'pluginOptions' => [
                                'loadingText' => '<div class="text-center load"><div class="loader">Loading...</div></div>'
                            ]
                        ],*/
                        'layout' => '<div class="row"><div class="col-lg-12 sorting hidden-xs">' . Yii::t('widok', 'Sortowanie:') . ' {sorter}</div></div><div class="row item-wraper">{items}</div><div class="row"><div class="col-lg-12 pagination-wrap">{pager}</div></div>',
                    ]);
                    ?>
                </div>
            </div>
            <?php endif; ?>


            <?php if($view=='list'): ?>
            <div role="tabpanel" class="tab-pane active" id="list-panel">
                <div class="container">
                    <div class="hidden hidden-sorting">
                    <?php
                        foreach ($sort as $k=>$sort_details){
                            echo Html::a($k, Url::current(['sort' => $k]), ['class'=>'hidden-sorting '.$k]);
                        }
                    ?>
                    </div>
                    <?php echo ListView::widget([
                        'dataProvider' => $trainingsDataProvider,
                        'itemView' => '_trainingList',
                        'itemOptions' => ['class' => 'row item list'],
                        'options' => ['class' => 'col-lg-12 list-view'],
                        'emptyText' => Yii::t('widok', 'Brak wyników. Zmień ustawienia filtrowania lub skontaktuj się z nami.'),
                        'emptyTextOptions' => ['tag'=>'h1', 'class'=>'empty-text'],
                        'pager'=> [
                          'maxButtonCount' => 5
                        ],
                        /*'pager' => [
                            'class' => \twofox\infinitescroll\InfiniteScrollPager::className(),
                            'paginationSelector' => '.pagination-wrap',
                            'pjaxContainer' => $pjax->id,
                            'itemWrapper' => '.item-wraper',
                            'pluginOptions' => [
                                'loadingText' => '<div class="text-center load"><div class="loader">Loading...</div></div>'
                            ]
                        ],*/
                        'layout' => '<div class="row"><div class="col-lg-12 sorting hidden-xs">' . Yii::t('widok', 'Sortowanie:') . ' {sorter}</div></div><div class="row"><div class="col-lg-12 item-wraper">{items}</div></div><div class="row"><div class="col-lg-12 pagination-wrap">{pager}</div></div>',
                    ]);
                    ?>
                </div>
            </div>
            <?php endif; ?>


            <?php if($view=='map'): ?>
            <div role="tabpanel" class="tab-pane active" id="map-panel">
                <?= $this->render('_map', ['trainingsDataProvider' => $trainingsDataProvider]); ?>
                <?php
                    $this->registerJs('mapInit(15, "mapReload()");');
                ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
    <?php
    $this->registerJs('  
       setTimeout(function(){       
			$(".wrap").find(".load").remove();
       }, 500);
    ');
    ?>
    <?php Pjax::end() ?>

<?php


$this->registerJs('        
        sendDataGrid = "";     
        pjAborted = 0;
        
        $("body").on("change", "select#trainingsearch-country_id, select#trainingsearch-license_id", function(event){         
            event.preventDefault();        
            pjAborted = 1;
            $.pjax.reload({container: "#filterdata", url: "'.Url::to(['/training/index']).'?view="+$("input[name=\'view\']").val()+"&ww="+$(window).width()+"&"+$("#trainingsearch-country_id").attr("name")+"="+$("#trainingsearch-country_id").val()+"&"+$("#trainingsearch-license_id").attr("name")+"="+$("#trainingsearch-license_id").val()+"&ww="+$(window).width(), async:false});
        });          
                
        $("#filterdata").on(\'pjax:start\', function() {
            pjAborted = 1;
            $("form#training-search-form").before("<div style=\"height:"+$("#filterdata").height()+"px;position: absolute;width: 100%;z-index: 100;background: #fff;\" class=\"load\"><div class=\"loader\">Loading...</div></div>");
        }).on("pjax:end", function(event) {           
            event.preventDefault();	            
            for_slider();		
			$(".wrap .load").remove();		
			pjAborted = 0;
			$.pjax.reload({container: "#listData", async:false});
        });
        
        $("#listData").on(\'pjax:start\', function(event) {       
            $("#training-search-form").after("<div style=\"height:"+$("#listData").height()+"px;position: absolute;width: 100%;z-index: 100;background: #f1f1f1;\" class=\"load\"><div class=\"loader\">Loading...</div></div>");
            
            if(pjAborted > 0){                
                event.preventDefault();	
                return false;
            }
                    
        }).on("pjax:end", function(event) {           
            event.preventDefault();            
            for_slider();			
			mapInit(15, "mapReload()");
			wiIndwxResize(true);
			$(".wrap .load").remove();
        }).on("click", ".pagination li:not(.active) a", function(){
            $(\'html, body\').animate({
              scrollTop: $("#listData").offset().top - $(".navbar").outerHeight()
            }, 700);
        });       

		$(\'body\').on("click", ".form-group .result", function(){
			var result = $(this);
			var vslideup = result.closest(".form-group").find(".slideup");
			$(".slideup.open").not(vslideup).find(".arrow").trigger("click");
			vslideup.slideToggle(1, function(){
			    $(this).toggleClass("open");
			});	
			/*checkvalueslider(this);*/
		});
		
		for_slider();		
		
		$(\'form#training-search-form\').on("change", "input, select", function(event){	
		    event.preventDefault();		    
		    if($(this).attr("name")=="TrainingSearch[license_id2]"){
		        var value = $(this).val();
		        $("[name=\"TrainingSearch[license_id]\"]").val(value).change();
		        return false;
		    }		    	
			checkChangeForm();
		});
		
		$(\'body\').click(function(event){
		    if ($(event.target).closest("span.arrow").length != 0 || ($(event.target).closest(".slideup").length == 0 && $(event.target).closest(".result").length == 0)) {
                 $(".slideup").slideUp(1, function(){                 
                    $(this).removeClass("open");
                 });
            }
		}).on("click", "a.filter-btn", function(){
		    var $btn = $(this);
		    $(".form-training-search .row").slideToggle(1, function(){		    
                if($(".form-training-search .row:visible").length > 0)		    
                    $btn.text("'.Yii::t('backend\training', 'Ukryj filtry').'");
                else
                    $btn.text("'.Yii::t('backend\training', 'Pokaż filtry').'");
		    });
		});
		
		');

$this->registerJs('
		function checkChangeForm(){							
            if(typeof search_form_submit != "undefined")
                clearTimeout(search_form_submit);
		    
		    search_form_submit = setTimeout(function(){		    
                if(pjAborted <= 0 && $(".form-training-search .slideup.open").length == 0 && $(".form-training-search .result.open").length == 0 && $(".select2-container--open").length == 0 && $(".datepicker.datepicker-dropdown").length == 0){
                    $.post("'.Url::to(['/training/index']).'?"+$("#training-search-form").serialize(), {md5:1}, function(obj){
                        if(sendDataGrid!=obj.md5 && obj.md5!=$(".training-list").attr("data-md5")){
                            sendDataGrid = obj.md5;
                            $(\'form#training-search-form\').trigger("submit");
                        }                
                    });
                }else
                   checkChangeForm();
            }, 500);
		}
		
		function for_slider(){		
            $(\'body\').find(".slider").each(function(){                                   
                checkvalueslider(this);
            });            
                        
            $("#termin-range span.date").each(function(){
                var input = $("input", this);
                var str = input.val();
                
                if(str!=""){
                    str = str.replace(/([0-9]{4})-([0-9]{2})-([0-9]{2})/, "$3.$2");                    
                    if(input.attr("name")=="TrainingSearch[dateRange][start]")
                        $(".btn", this).text("'.Yii::t('search', 'od').' "+str);
                    else
                        $(".btn", this).text("'.Yii::t('search', 'do').' "+str);
                }
            });            
            wiIndwxResize();            
		}
		
		function checkvalueslider(itm, new_val){
			var input = $(itm).closest(".form-group").find("input");
			var value = typeof new_val != "undefined" && new_val != null ? new_val : input.val();					
			var name = input.attr("name");
			
			if(name=="TrainingSearch[aircraftAge]")
				aft = "' . Yii::t('search', 'lat') . '";
			else
				aft = "' . htmlspecialchars(\Yii::$app->params['currency_symbol']) . '";
					
			$(itm).closest(".form-group").find(".slideup > span > h3 > span:eq(0)").html(formatMoney(value, 0, 3, \'<span class="numspan"></span>\'));
			$(itm).closest(".form-group").find(".result").html(\'' . Yii::t('search', 'do ') . '\'+formatMoney(value, 0, 3, \'<span class="numspan"></span>\')+\' \'+aft);
		}
		
	', \yii\web\View::POS_HEAD);
?>

<?php
$this->registerJs('	 
	markers = [];	
	function mapReload(){
	
		if($("#locationsHF").length == 0)
		    return;
		
		var bounds = new google.maps.LatLngBounds();		
		var locations = $("#locationsHF").html(); //retrieve array	
		locations = JSON.parse(locations);
		var content_map_info = new Array();
		
		for (var i in locations) {			
			if (locations[i][1] =="undefined"){ description ="";} else { description = locations[i][1];}
			if (locations[i][2] =="undefined"){ telephone ="";} else { telephone = locations[i][2];}
			if (locations[i][3] =="undefined"){ email ="";} else { email = locations[i][3];}
			if (locations[i][4] =="undefined"){ web ="";} else { web = locations[i][4];}
			if (locations[i][7] =="undefined"){ markericon ="";} else { markericon = locations[i][7];}
			
			marker = new google.maps.Marker({				
				position: new google.maps.LatLng(locations[i][7], locations[i][8]),
				map: googlemap,
				title: locations[i][0],
				desc: i,
				web: locations[i][10]
			});
			
			content_map_info[i] = "#info_window_"+locations[i][11];			
			bounds.extend(marker.position);	
					
			google.maps.event.addListener(marker, \'click\', (function (marker, i){
				return function () {
	                jQuery("#map-panel .to_map").addClass("hidden");
	                jQuery(content_map_info[i]).removeClass("hidden").css({display:"inline-block", "position":"absolute", "margin-top": 50, "z-index": 98});	                
	                if(jQuery(content_map_info[i]).find(".thumbnail > .close").length == 0)
	                    jQuery(content_map_info[i]).find(".thumbnail").prepend("<a class=\"close\"><span class=\"glyphicon glyphicon-remove\"></span></a>")
	            }
			})(marker, i));
			
			markers.push(marker);
		}
		
		googlemap.fitBounds(bounds);
        
        google.maps.event.addDomListener(window, \'resize\', function() {
            googlemap.fitBounds(bounds);
        });
         
		/*
        setTimeout(function(){
            google.maps.event.removeListener(zoomChangeBoundsListener)
        }, 1500);*/
	}
', \yii\web\View::POS_HEAD);
?>