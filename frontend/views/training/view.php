<?php

use yii\helpers\Html,
	yii\bootstrap\Tabs;
use yii\helpers\HtmlPurifier;
use yii\bootstrap\ActiveForm,
	imanilchaudhari\CurrencyConverter\CurrencyConverter;

	$converter = new CurrencyConverter();

$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
$swissNumberProto = $phoneUtil->parse($model->school->phone, $model->school->country_id);
$model->school->phone = $phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);

/* @var $this yii\web\View */
/* @var $model backend\models\Training */

$this->title = Yii::t('training/view', 'Szczegóły szkolenia: {name}', ['name'=>$model->name]);

Yii::$app->params['country_id'] = $model->school->country_id;
Yii::$app->params['license_id']	= $model->license_id;
?>
<div class="container-fluid">
	<div class="row bg-white">
		<div class="container training-details">
			<div class="clearfix visible-xs">
				<h2 class="big text-blue pull-left"><?= $model->licenseType->license ?></h2>
				<h2 class="text-blue big pull-right"><?= number_format(($model->school->country->currency_id!=Yii::$app->params['currency'] ? $converter->convert($model->school->country->currency_id, Yii::$app->params['currency'], $model->price) : $model->price), 0, '.', '<span class="numspan"></span>') ?><span><?= Yii::$app->params['currency'] ?></span></h2>
			</div>

			<div class="row">
				<?php if(!empty($model->school->logo)): ?>
				<div class="col-lg-2 col-sm-2">
					<img src="<?= $model->school->LogoResize(200) ?>" />
				</div>
				<?php endif; ?>
				<div class="<?= empty($model->school->logo)?'col-lg-5 col-sm-8':'col-lg-3 col-sm-6' ?> col-xs-12">
					<h2 class="big text-blue hidden-xs"><?= $model->licenseType->license ?></h2>
					<h1 class="small text-blue"><?= Html::a($model->name, ['school/view', 'id'=>$model->school->id]) ?></h1>
					<div class="school-ratings">
						<span class="star-ratings-sprite">
							<span style="width:<?= $model->school->school_rate*100/5 ?>%" class="star-ratings-sprite-rating"></span>
						</span>
						<?= number_format(floatval($model->school->school_rate), 1, '.', ''); ?>
					</div>
					<hr />
					<table class="details">
						<tr>
							<th>
								<?= Yii::t('training/view', 'Termin:'); ?>
							</th>
							<td>
                                <?php
                                if(is_array($model->startDates)){
                                    $date = null;
                                    foreach ($model->startDates as $termin){
                                        if($termin->start_date>=date('Y-m-d')){
                                            $date = $termin;
                                            break;
                                        }
                                    }

                                    echo is_object($date) ? $date->start_text : '<a href="javascript:;" class="to-contact">'.Yii::t('training/view', 'Contact us').'</a>';
                                }else{
                                    echo Yii::t('training/view', 'od zaraz');
                                }
                                ?>
							</td>
						</tr>
						<tr>
							<th>
								<?= Yii::t('training/view', 'Miasto:'); ?>
							</th>
							<td>
								<?php
								/*$city = $model->school->city;
                                if(isset($model->addressPractice) && isset($model->addressTheory) && isset($model->addressPractice->city) && isset($model->addressTheory->city) && $model->addressTheory->city!=$model->addressPractice->city)
                                    $city = $model->addressPractice->city.', '.$model->addressTheory->city;
                                elseif(isset($model->addressPractice) && isset($model->addressTheory->city) && isset($model->addressTheory->city) && $model->addressTheory->city==$model->addressPractice->city)
                                    $city = $model->addressTheory->city;
                                */
								?>
								<span><?= HtmlPurifier::process($model->address_practice_id != 0 ? $model->addressPractice->city : ($model->address_theory_id != 0 ? $model->addressTheory->city : '-')) ?></span>&nbsp;
							</td>
						</tr>
						<tr>
							<th>
								<?= Yii::t('training/view', 'Samolot:'); ?>
							</th>
							<td>
								<span>
									<?php 	
										if (count($model->aircraft) > 0) {
											echo HtmlPurifier::process($model->aircraft[0]->aircraftType->name);
										} else 
											echo "-"
									?>
								</span>&nbsp;
							</td>
						</tr>
						<?php if(($model->elearning_hours + $model->theory_hours) > 0): ?>
							<tr>
								<th>
									<?= Yii::t('training/view', 'Teoria:'); ?>
								</th>
								<td>
									<?= ($model->elearning_hours + $model->theory_hours) ?>h
								</td>
							</tr>
						<?php endif; ?>

						<?php if(($model->aircraft_hours + $model->simulator_hours) > 0): ?>
							<tr>
								<th>
									<?= Yii::t('training/view', 'Praktyka:'); ?>
								</th>
								<td>
									<?= ($model->aircraft_hours + $model->simulator_hours) ?>h
								</td>
							</tr>
						<?php endif; ?>
					</table>
					<div class="licence-descriptions">
						<?= $model->licenseType->tlicencedescription ?>
					</div>
				</div>
				<div class="col-lg-3 col-lg-offset-2 col-sm-4 phone-details col-xs-12">
					<h2 class="text-blue big hidden-xs"><?= number_format($converter->convert($model->school->country->currency_id, Yii::$app->params['currency'], $model->price), 0, '.', '<span class="numspan"></span>') ?><span><?= Yii::$app->params['currency'] ?></span></h2>
					<a href="javascript:;" class="btn btn-primary btn-lg btn-block to-contact"><?= Yii::t('training/view', 'ZAPISZ SIĘ'); ?></a>
					<span class="hidden-xs"><?= Yii::t('training/view', 'Lub zadzwoń'); ?></span>
					<a href="tel:<?= trim($model->school->phone) ?>" class="btn btn-default btn-lg btn-block hidden-xs phone"><?= trim($model->school->phone) ?></a>
					<a href="tel:<?= trim($model->school->phone) ?>" class="btn btn-default btn-lg btn-block visible-xs phone pull-right"><?= Yii::t('training/view', '<span class="glyphicon glyphicon-earphone"></span> ZADZWOŃ'); ?></a>
					<hr />

					<div>
						<?php
						$city = '-';
						$markers = [];

						if($model->address_theory_id > 0){
							$markers[$model->address_theory_id] = ['coordinates'=>$model->addressTheory->coordinates, 'type'=>Yii::t('training/view', 'Teoria'), 'title'=>implode(', ', array_filter([$model->addressTheory->address, $model->addressTheory->{'zip-code'}.' '.$model->addressTheory->city, $model->school->country->ttitle]))];
						}

						if($model->address_practice_id > 0){
							$markers[$model->address_practice_id] = ['coordinates'=>$model->addressPractice->coordinates, 'type'=>Yii::t('training/view', 'Praktyka'), 'title'=>implode(', ', array_filter([$model->addressPractice->address, $model->addressPractice->{'zip-code'}.' '.$model->addressPractice->city, $model->school->country->ttitle]))];
						}

						if(count($markers) > 1){
							foreach ($markers as $marker){
								echo '<p><strong>'.$marker['type'].':</strong><br />'.$marker['title'].'</p>';
							}
						}else{
							foreach ($markers as $marker){
								echo $marker['title'].'<br />';
							}
						}

						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="training-view clearfix">
	<?php
		$items = [];
		$items[] = [
			'label' => Yii::t('training/view', 'Szczegóły szkolenia'),
			'active' => true,
			'content' => $this->render('view/Details_of_training', ['model'=>$model]),
			'contentOptions' => ['class'=>'in']
		];

		if(isset($model->aircraft) && count($model->aircraft) > 0){
			$items[] = [
				'label' => Yii::t('training/view', 'Dostępne samoloty'),
				'content' => $this->render('view/Available_aircraft', ['aircrafts'=>$model->aircraft]),
			];
		}

		if(isset($model->simulator) && count($model->simulator) > 0){
			$items[] = [
				'label' => Yii::t('training/view', 'Dostępne symulatory'),
				'content' => $this->render('view/Available_simulator', ['simulators'=>$model->simulator]),
			];
		}

		if(count($markers)>0)
		$items['map'] = [
			'label' => Yii::t('training/view', 'Mapa'),
			'content' => $this->render('view/Map', ['model'=>$model, 'markers'=>$markers]),
			'options' => ['id'=>'view_map']
		];

		$items['schoolRates'] = [
			'label' => isset($model->school->schoolRates) && count($model->school->schoolRates) > 0 ? Yii::t('school-rates', 'Opinie ({num})', ['num'=>count($model->school->schoolRates)]) : Yii::t('training/view', 'Opinie'),
			'content' => $this->render('view/Rates', ['schoolRates'=>$model->school->schoolRates, 'schoolRatesForm'=>$schoolRatesForm]),
		];

		echo Tabs::widget([
			'options'=>['class'=>'container hidden-xs'],
			'items' => $items,
			'encodeLabels' => false
		]);

		if(count($markers)>0)
			$items['map'] = [
				'label' => Yii::t('training/view', 'Mapa'),
				'content' => $this->render('view/Map2', ['model'=>$model, 'markers'=>$markers]),
				'options' => ['id'=>'view_map2']
			];


		unset($items['schoolRates']);

		echo \yii\bootstrap\Collapse::widget([
			'options'=>['class'=>'visible-xs'],
			'items' => $items,
			'encodeLabels' => false,
			'id' => 'myCollapsible'
		]);
	?>
</div>

<div class="contact_form clearfix">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-sm-6 col-xs-12">
				<h2 class="big text-blue">
					<?= $model->licenseType->license ?>
					<div class="pull-right"><?= number_format(($model->school->country->currency_id!=Yii::$app->params['currency'] ? $converter->convert($model->school->country->currency_id, Yii::$app->params['currency'], $model->price) : $model->price), 0, '.', '<span class="numspan"></span>') ?><span><?= Yii::$app->params['currency'] ?></span></div>
				</h2>
				<h1 class="small text-blue school-name"><?= Html::a($model->name, ['school/view', 'id'=>$model->school->id]) ?></h1>
				<table class="details2">
                    <tr>
						<th>
							<?= Yii::t('training/view', 'Termin:'); ?>
						</th>
						<td>
                            <?php
                            if(is_array($model->startDates)){
                                $date = null;
                                foreach ($model->startDates as $termin){
                                    if($termin->start_date>=date('Y-m-d')){
                                        $date = $termin;
                                        break;
                                    }
                                }

                                echo is_object($date) ? $date->start_text : '<a href="javascript:;" class="to-contact">'.Yii::t('training/view', 'Contact us').'</a>';
                            }else{
                                echo Yii::t('training/view', 'od zaraz');
                            }
                            ?>
						</td>
					</tr>
					<tr>
						<th>
							<?= Yii::t('training/view', 'Miasto:'); ?>
						</th>
						<td>
							<span><?= HtmlPurifier::process($model->address_practice_id != 0 ? $model->addressPractice->city : ($model->address_theory_id != 0 ? $model->addressTheory->city : '-')) ?></span>&nbsp;
						</td>
					</tr>
					<tr>
						<th>
							<?= Yii::t('training/view', 'Samolot:'); ?>
						</th>
						<td>
								<span>
									<?php
									if (count($model->aircraft) > 0) {
										echo HtmlPurifier::process($model->aircraft[0]->aircraftType->name);
									} else
										echo "-"
									?>
								</span>&nbsp;
						</td>
					</tr>

					<?php if(($model->elearning_hours + $model->theory_hours) > 0): ?>
						<tr>
							<th>
								<?= Yii::t('training/view', 'Teoria:'); ?>
							</th>
							<td>
								<?= ($model->elearning_hours + $model->theory_hours) ?>h
							</td>
						</tr>
					<?php endif; ?>

					<?php if(($model->aircraft_hours + $model->simulator_hours) > 0): ?>
						<tr>
							<th>
								<?= Yii::t('training/view', 'Praktyka:'); ?>
							</th>
							<td>
								<?= ($model->aircraft_hours + $model->simulator_hours) ?>h
							</td>
						</tr>
					<?php endif; ?>

				</table>
			</div>
			<div class="col-lg-5 col-lg-offset-1 col-sm-6 col-xs-12">
				<?php $form = ActiveForm::begin(['id' => 'contact-form', 'validateOnBlur'=>false, 'validateOnChange'=>false]); ?>

				<?= Html::tag('h2', Yii::t('training/view', 'Zarezerwuj miejsce'), ['class'=>'text-blue']) ?>

				<?= $form->field($ContactForm, 'name', ['inputOptions' => ['placeholder' => $ContactForm->getAttributeLabel('name'), 'class'=>'input-lg form-control']])->label(false) ?>

				<?= $form->field($ContactForm, 'email', ['inputOptions' => ['placeholder' => $ContactForm->getAttributeLabel('email'), 'class'=>'input-lg form-control']])->label(false) ?>

				<?= $form->field($ContactForm, 'phone', ['inputOptions' => ['placeholder' => $ContactForm->getAttributeLabel('phone'), 'class'=>'input-lg form-control']])->label(false) ?>

				<?= $form->field($ContactForm, 'regulamin')->checkbox(['template' => '<div class="checkbox"><label>{input}<p style="display: inline-block;">{label}</p></label>{error}</div>']) ?>

				<?php /*$form->field($ContactForm, 'personal_data')->checkbox(['template' => '<div class="checkbox"><label>{input}<p style="display: inline-block;">{label}</p></label>{error}</div>']) ?>

				<?= $form->field($ContactForm, 'offers')->checkbox(['template' => '<div class="checkbox"><label>{input}<p style="display: inline-block;">{label}</p></label>{error}</div>'])*/ ?>

				<div class="form-group">
					<?= Html::submitButton(Yii::t('contact_form', 'Zapisz się'), ['class' => 'btn btn-primary btn-lg', 'name' => 'contact-button']) ?>
				</div>

				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
<?php

$this->registerJs('

$(\'#myCollapsible\').on(\'shown.bs.collapse\', function () {
  $(".panel .panel-heading.active").removeClass("active");
  $(".panel-collapse.in").closest(".panel").find(".panel-heading").addClass("active");
  mapInit(10, "mapReload2()");
  
  $(\'html, body\').animate({
     scrollTop: $(\'#myCollapsible .panel-heading.active\').offset().top - $(".navbar").outerHeight() - $(".container-fluid.navbar-fixed-top").outerHeight()
  }, 700);
  
}).on(\'hidden.bs.collapse\', function () {
  $(".panel .panel-heading.active").removeClass("active");
});

$(".to-contact").click(function() {
    $(\'html, body\').animate({
        scrollTop: $(".wrap > .contact_form").offset().top - $(".navbar").outerHeight() - $(".container-fluid.navbar-fixed-top").outerHeight()
    }, 700);
});
');