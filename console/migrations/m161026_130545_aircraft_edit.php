<?php

use yii\db\Migration;

class m161026_130545_aircraft_edit extends Migration
{
    public function up()
    {
        $this->execute("ALTER TABLE `aircraft` ADD `distance` INT NOT NULL AFTER `ifr`, ADD `max_speed` INT NOT NULL AFTER `distance`");
    }

    public function down()
    {
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
