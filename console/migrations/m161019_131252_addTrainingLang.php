<?php

use yii\db\Migration;

class m161019_131252_addTrainingLang extends Migration
{
    public function down()
    {
        echo "m161019_131252_addTrainingLang cannot be reverted.\n";

        return false;
    }


    // Use safeUp/safeDown to run migration code within a transaction
    public function up()
    {
        $this->execute("ALTER TABLE `training` ADD `lang` TEXT NOT NULL AFTER `start_date`");
        $this->execute("UPDATE `training` SET `lang` = 'pl-PL'");
    }

    public function safeDown()
    {
    }
}
