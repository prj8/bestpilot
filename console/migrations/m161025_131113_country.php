<?php

use yii\db\Migration;

class m161025_131113_country extends Migration
{
    public function up(){
        $this->execute("ALTER TABLE school DROP FOREIGN KEY FK_school_countries");
        $this->execute("UPDATE `countries` SET `id` = '1' WHERE `countries`.`id` = 0");
        $this->execute("ALTER TABLE `countries` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT");
        $this->execute("UPDATE `school` SET `county_id` = '1'");
        $this->execute("ALTER TABLE `school` ADD FOREIGN KEY (`county_id`) REFERENCES `countries`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT");
        $this->execute("ALTER TABLE `school` CHANGE `county_id` `country_id` INT(11) NULL DEFAULT '1'");
    }

    public function down()
    {
        echo "m161025_131113_country cannot be reverted.\n";

        return false;
    }
}
